{ *************************************************************************** } 
{ Komunikacja z serwerem                                                      }
{ *************************************************************************** }

procedure print(tekst)
{ Przekazanie fragmentu wyj�cia do serwera/aplikacji }
begin end

function external(tekst)
{ Przekazanie do wykonania instrukcji serwerowi oraz zwr�cenie jej wyniku }
begin end

function cfg(parametr)
{ Zwraca parametr katalogu }
begin end

{ *************************************************************************** } 
{ Przetwarzanie tekstu - ci�cie                                               }
{ *************************************************************************** }

function head(wejscie, separator)
{ Zwraca fragment �a�cucha `wej�cie` poprzedzaj�cy `separator`.
  Je�li wej�cie nie zawiera separatora, zwracany jest pusty �a�cuch. }
begin end

function tail(wejscie, separator)
{ Zwraca fragment �a�cucha `wej�cie` nast�puj�cy po `separator`.
  Je�li wej�cie nie zawiera separatora, zwracany jest ca�y �a�cuch. }
begin end

function tail(wejscie, separator, N)
{ Zwraca fragment �a�cucha `wej�cie` nast�puj�cy po `separator`.
  Je�li wej�cie nie zawiera separatora, zwracany jest ca�y �a�cuch.
  Wykonuje operacj� `N` razy. }
begin end

function left(str, N)
{ Zwraca pierwsze N znak�w �a�cucha. }
begin end

function right(str, N)
{ Zwraca ostatnie N znak�w �a�cucha. }
begin end

function substr(str, S, N)
{ Zwraca N znak�w �a�cucha zaczynaj�c od S-tego. (Jak w Delphi i PSQL). }
begin end

function drop(str, N)
{ Zwraca �a�cuch bez pierwszych N znak�w. }
begin end

{ *************************************************************************** } 
{ Przetwarzanie tekstu - usuwanie przedrostk�w                                }
{ *************************************************************************** }

{ Od lewej ------------------------------------------------------------------ }

function ltrim(str, znaki)
{ Wycina podane znaki z lewej strony �a�cucha. }
begin end

function ltrimw(str)
{ Wycina bia�e znaki z lewej strony �a�cucha. }
begin end

function ltrimw(str, znaki)
{ Wycina bia�e oraz podane znaki z lewej strony �a�cucha. }
begin end

function ltrimwc(str)
{ Wycina bia�e i kontrolne znaki z lewej strony �a�cucha. }
begin end

function ltrimwc(str, znaki)
{ Wycina bia�e, kontrolne oraz podane znaki z lewej strony �a�cucha. }
begin end

{ ----------------------------------------------------------------- Od prawej }

function rtrim(str, znaki)
{ Wycina podane znaki z prawej strony �a�cucha. }
begin end

function rtrimw(str)
{ Wycina bia�e znaki z prawej strony �a�cucha. }
begin end

function rtrimw(str, znaki)
{ Wycina bia�e oraz podane znaki z prawej strony �a�cucha. }
begin end

function rtrimwc(str)
{ Wycina bia�e i kontrolne znaki z prawej strony �a�cucha. }
begin end

function rtrimwc(str, znaki)
{ Wycina bia�e, kontrolne oraz podane znaki z prawej strony �a�cucha. }
begin end

{ Z obu --------------------------------------------------------------- stron }

function lrtrim(str, znaki)
{ Wycina podane znaki z obu stron �a�cucha. }
begin end

function lrtrimw(str)
{ Wycina bia�e znaki z obu stron �a�cucha. }
begin end

function lrtrimw(str, znaki)
{ Wycina bia�e oraz podane znaki z obu stron �a�cucha. }
begin end

function lrtrimwc(str)
{ Wycina bia�e i kontrolne znaki z obu stron �a�cucha. }
begin end

function lrtrimwc(str, znaki)
{ Wycina bia�e, kontrolne oraz podane znaki z obu stron �a�cucha. }
begin end

function trim(str)
{ Wycina spacje z obu stron �a�cucha. }
begin end

{ *************************************************************************** } 
{ Przetwarzanie tekstu - d�ugo��                                              }
{ *************************************************************************** }

function len(str)
{ Zwraca liczb� logicznych znak�w. }
begin end

function len8(str)
{ Zwraca liczb� bajt�w w kodowaniu UTF-8. }
begin end

function len16(str)
{ Zwraca liczb� jednostek w kodowaniu UTF-16. }
begin end

function len32(str)
{ Zwraca liczb� jednostek w kodowaniu UTF-32. }
begin end

{ *************************************************************************** } 
{ Przetwarzanie tekstu - wielko�� liter                                       }
{ *************************************************************************** }

function up(str)
{ Zamienia wszystkie litery str na wielkie. }
begin end

function lo(str)
{ Zamienia wszystkie litery str na ma�e. }
begin end

{ *************************************************************************** } 
{ Przetwarzanie tekstu - konkatenacja                                         }
{ *************************************************************************** }

function sep(L, separator, R)
{ ��czy �a�cuchy L i R separatorem sep, o ile L i R s� niepustymi �a�cuchami.
  Je�li jeden z �a�cuch�w L lub R jest pusty, zwracany jest tylko drugi, niepusty �a�cuch. }
begin end

function pr(prefiks, str)
{ Je�li �a�cuch str jest niepusty, zwraca prefiks+str. }
begin end

function ta(str, sufiks)
{ Je�li �a�cuch str jest niepusty, zwraca str+sufiks. }
begin end

{ *************************************************************************** } 
{ Przetwarzanie tekstu - szukanie i pasowanie                                 }
{ *************************************************************************** }

function position(igla, siano)
{ Sprawdza czy �a�cuch `siano` zawiera �a�cuch `ig�a`.
  Je�li tak, zwracana jest jego pozycja (indeksy znak�w liczone od 1).
  Je�li nie, zwracany jest pusty �a�cuch. }
begin end

function matches(str, wzorzec)
{ Sprawdza czy �a�cuch pasuje do wzorca i je�li tak, zwracany jest pusty �a�cuch.
  Je�li nie, zwraca warto�� 'N'. W sk�adni wzorca wyst�puj� dwa znaki specjalne:
    ? - zast�puje dowolny pojedy�czy znak
    * - zast�puje dowolny ci�g znak�w }
begin end

function form(str, wzorzec)
{ Formatuje �a�cuch str wg wzorca.
  Przetwarzane s� kolejne znaki �a�cucha str wg odpowiadaj�cym ich pozycji znakom we wzorcu.
  Wzorzec mo�e zawiera� znaki 'A' - przepisanie znaku z �a�cucha str,
  oraz '\' (backslash) - pomini�cie znaku z �a�cucha str. }
begin end

function rform(str, wzorzec)
{ Formatuje �a�cuch str wg wzorca, przy czym w odr�nieniu od funkcji form -
  �a�cuch str przed rozpocz�ciem formatowania jest odwracany (przetwarzanie str odbywa sie od ko�ca). }
begin end

{ *************************************************************************** } 
{ Przetwarzanie tekstu - kodowanie                                            }
{ *************************************************************************** }

function ascii(str)
{ Zwraca kod Windows-1250 pierwszego znaku �a�cucha str. }
begin end

function chr(num)
{ Zwraca znak o kodzie Windows-1250 wyra�onym w �a�cuchu num. }
begin end

function unichr(num)
{ Zwraca codepoint unikodowy o kodzie num. }
begin end

function base64(str)
{ Kodowanie BASE64. }
begin end

function mak(str)
{ Zamienia symbole liter obcych alfabet�w MAK na zwyk�e. }
begin end

function uri(str)
{ Koduje znaki specjalne w �a�cuchu str, do postaci zgodnej z formatem URI (adresy zasob�w w HTTP). }
begin end

function utf8(str)
{ Koduje �a�cuch str do postaci w UTF-8. W strixie nic nie robi. }
begin end

function escapehtml(str)
{ Koduje pewne znaki jako `&foo;` }
begin end

function normalize(str)
{ Usuwa znaki diakrytyczne }
begin end

{ *************************************************************************** } 
{ Przetwarzanie tekstu - inne                                                 }
{ *************************************************************************** }

function replace(str, co, naco)
{ Podmienia wszystkie wyst�pienia �a�cucha `co` w �a�cuchu `str`. }
begin end

function reverse(str)
{ Przestawia pierwszy wyraz na koniec.
  Jako miejsce podzia�u w pierwszej kolejno�ci przyjmowany jest pierwszy napotkany przecinek.
  Je�li nie ma przecinka, miejscem podzia�u jest pierwsza spacja.
  Warunek specjalny: je�li str ko�czy si� sekwencj� [warto��] - w�wczas jest ona zachowywana na miejscu,
  przestawienie wyrazu dotyczy cz�ci �a�cucha poprzedzaj�cej t� sekwencj�. }
begin end

function sort(list, sep)
{ Sortuje alfabetycznie list� wyra�on� w �a�cuchu list, przy za�o�eniu �e elementy listy wyznaczone s� separatorem sep.
  Stosowany jest alfabet polski (z uwzgl�dnieniem znak�w diakrytycznych) }
begin end

function agregate(list, sep)
{ Agreguje list� kolejnych identyfikator�w w linii do postaci \"od - do\" (uprzednio je sortuj�c).
  Identyfikatory rozdzielane s� separatorem sep. }
begin end

{ *************************************************************************** } 
{ Arytmetyka                                                                  }
{ *************************************************************************** }

function isint(str)
{ Czy podany �a�cuch parsuje si� jako liczba? }
begin end

function int(str)
{ Je�li podany �a�cuch jest liczb� ca�kowit�, to jest zwracany, w przeciwnym wypadku zwracany jest �a�cuch '0'. }
begin end

function int(str, albo)
{ Je�li podany �a�cuch jest liczb� ca�kowit�, to jest zwracany, w przeciwnym wypadku zwracany jest drugi �a�cuch. }
begin end

function succ(num)
{ Dodaje 1 do warto�ci liczbowej wyra�onej w �a�cuchu num (zwraca num+1).
  Je�li num nie jest liczb�, zwracane jest '0'. }
begin end

function add(num, num)
{ Dodaje dwie warto�ci liczbowe.
  Je�li parametr nie jest liczb�, traktowany jest jako '0'. }
begin end

function mul(num, num)
{ Mno�y dwie warto�ci liczbowe.
  Je�li dowolny parametr nie jest liczb�, zwracana jest warto�� '0'. }
begin end

function mod(num, den)
{ Dzieli dwie warto�ci liczbowe i zwraca reszt�.
  Je�li dowolna z warto�ci nie jest liczb�, albo den jest zerem, zwracana jest warto�� '-' (minus). }
begin end

function div(num, den, digits)
{ Dzieli dwie warto�ci liczbowe wyra�one w �a�cuchach num i den (operacja num/den).
  W warto�ci liczbowej wyra�onej w �a�cuchu digits okre�li� nale�y liczb� zwracanych miejsc po przecinku.
  Je�li dowolna z warto�ci nie jest liczb�, albo den jest zerem, zwracana jest warto�� '-' (minus).
  Je�li digits jest liczb� wi�ksz� od zera, separatorem cz�ci u�amkowej jest przecinek. }
begin end

function gt(L, R)
{ Por�wnuje dwie warto�ci liczbowe wyra�one w �a�cuchach L i R.
  Je�li parametr nie jest liczb�, traktowany jest jako '0'.
  Je�li L jest wi�kszy od R, zwracany jest pusty �a�cuch.
  W przeciwnym razie zwracana jest 'N'. }
begin end

function ge(L, R)
{ Por�wnuje dwie warto�ci liczbowe wyra�one w �a�cuchach L i R.
  Je�li parametr nie jest liczb�, traktowany jest jako '0'.
  Je�li L jest wi�kszy lub r�wny R, zwracany jest pusty �a�cuch.
  W przeciwnym razie zwracana jest 'N'. }
begin end

function random(num)
{ Zwraca liczb� losow� w przedziale od 0 do warto�ci liczbowej wyra�onej w �a�cuchu num. }
begin end

{ *************************************************************************** } 
{ Data i czas                                                                 }
{ *************************************************************************** }

function date(dni)
{ Zwraca bie��c� dat� powi�kszon� o liczb� dni, w formacie RRRRMMDD.
  Je�li argument jest pustym �a�cuchem zwracana jest bie��ca data. }
begin end

function time(sekundy)
{ Zwraca bie��cy czas powi�szony o liczb� sekund, w formacie GGMMSS.
  Je�li argument jest pustym �a�cuchem zwracany jest bie��cy czas. }
begin end

function days(wczesniejsza, pozniejsza)
{ Zwraca liczb� dni mi�dzy dwiema datami.
  Daty powinny by� wyra�one w postaci RRRRMMDD i podane w rosn�cej kolejno�ci. }
begin end

{ *************************************************************************** } 
{ MARC21                                                                      }
{ *************************************************************************** }

function omit(marc21)
{ Zwraca �a�cuch z pomini�ciem znacznik�w podp�l MARC-21.
  Oznaczenie znacznika podpola zale�y od konfiguracji, zwykle jest to <tt>^c</tt>,
  gdzie <tt>c</tt> to kod podpola.
  Dla podp�l o kodzie numerycznym pomija r�wnie� tre�� podpola. }
begin end

function womit(marc21)
{ Zwraca �a�cuch z pomini�ciem znacznik�w podp�l MARC-21.
  Oznaczenie znacznika podpola zale�y od konfiguracji, zwykle jest to <tt>^c</tt>,
  gdzie <tt>c</tt> to kod podpola.
  Dzia�a jak dawny omit. }
begin end

function pomit(marc21, kody)
{ Pozytywny omit - pozostawia tylko warto�ci podp�l o podanych kodach.
  Usuwa wszystkie znaczniki. }
begin end

function nomit(marc21, kody)
{ Negatywny omit - usuwa tylko warto�ci podp�l o podanych kodach.
  Usuwa wszystkie znaczniki. }
begin end

function subfield(marc21, kod)
{ Zwraca warto�� podpola o podanym kodzie w wierszu marc21,
  zawieraj�cym pole MARC-21 z oznaczeniami podp�l.
  Je�li marc21 nie zawiera podpola o takim kodzie, zwracany jest pusty �a�cuch. }
begin end

function marker(marc21)
{ Zwraca oznaczenie pierwszego podpola w marc21,
  zawieraj�cym pole MARC-21 z oznaczeniami podp�l. }
begin end

{ *************************************************************************** } 
{ Rekord logiczny                                                             }
{ *************************************************************************** }

procedure open(id)
{ Wrzucenie na stos nowego rekordu logicznego }
begin end

procedure grupa(prefiks)
{ Nawigacja do podanego miejsca w rekordzie logicznym }
begin end

function grupa(prefiks)
{ Nawigacja do podanego miejsca w rekordzie logicznym }
begin end

procedure skip()
{ Pomini�cie jednej pozycji w rekordzie logicznym }
begin end

function skip()
{ Pomini�cie jednej pozycji w rekordzie logicznym }
begin end

procedure back()
{ Powr�t do poprzedniego miejsca w rekordzie logicznym albo poprzedniego rekordu logicznego }
begin end

begin
  ok = 'czy jeste�my w pozycji wskazuj�cej na istniej�ce pole'
  pozycja = 'aktualna pozycja'
  edytowalne = '?'
  pole = 'warto�� pola w aktualnej pozycji'
  pierwotne = 'pierwotna warto�� pola w aktualnej pozycji'
  __slimit__ = 'limit wielko�ci zmiennej w MiB (dzia�a jak zmienna globalna)' 
  __ilimit__ = 'limit liczby iteracji nast�pnej p�tli while (dzia�a jak zmienna globalna), lub otaczaj�cej p�tli while (dzia�a jak zmienna lokalna)' 
  __i__ = 'nr aktualnej iteracji p�tli (dzia�a jak zmienna lokalna)'
end

{ *************************************************************************** } 
{ Indeksy bazodanowe                                                          }
{ *************************************************************************** }

function first(idf, prefix)
{ Warto�� pierwszego wpisu w indeksie idf+prefix. }
begin end

function next(idf, prefix, key)
{ Warto�� nast�pnego wpisu w indeksie idf+prefix,
  za key (key musi istnie� i jest kontrolowane z uwzgl�dnieniem ma�ych i wielkich liter). }
begin end

function find(idf, key)
{ Znajduje warto�� najbli�szego wpisu w indeksie idf,
  pocz�wszy od key (ignorowana jest wielko�� liter). }
begin end

function last(idf, prefix)
{ Warto�� ostatniego wpisu w indeksie idf+prefix. }
begin end

function select(record, hard, pattern)
{ Zwraca list� numer�w rekord�w, separowanych '|',
  dla kt�rych klucz indeksu jest zgodny z hard i spe�nia pattern. }
begin end

function references(id)
{ Zwraca list� numer�w rekord�w, separowanych '|', kt�re zawieraj� referencj� do id. }
begin end

function refs(id, list)
{ Lista rekord�w okre�lonego typu wskazuj�cych na podany. }
begin end

function limited(record, idf, prefix, N)
{ Lista N pierwszych rekord�w wybranych wg indeksu. }
begin end

{ *************************************************************************** } 
{ Debugowanie                                                                 }
{ *************************************************************************** }

procedure trace(wiadomosc)
{ Zalogowanie wiadomo�ci programistycznej }
begin end
