package pl.sokrates.intellisowa;

import com.intellij.openapi.fileTypes.ExtensionFileNameMatcher;
import com.intellij.openapi.fileTypes.FileNameMatcher;
import com.intellij.openapi.fileTypes.FileTypeConsumer;
import com.intellij.openapi.fileTypes.FileTypeFactory;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;
import pl.sokrates.intellisowa.epr.EprFileType;
import pl.sokrates.intellisowa.leshlang.LeshFileType;

public class SokratesFileTypeFactory extends FileTypeFactory
{
    @Override
    public void createFileTypes (@NotNull FileTypeConsumer fileTypeConsumer)
    {
        fileTypeConsumer.consume (LeshFileType.INSTANCE,
                                  new ExtensionFileNameMatcher ("vpr"),
                                  new ExtensionFileNameMatcher ("spr"),
                                  new ExtensionFileNameMatcher ("zpr"),
                                  new ExtensionFileNameMatcher ("apr"),
                                  new ExtensionFileNameMatcher ("act"),
                                  new ExtensionFileNameMatcher ("ppr"),
                                  new ExtensionFileNameMatcher ("tpr"),
                                  new ExtensionFileNameMatcher ("cnv"),
                                  new FileNameMatcher ()
                                  {
                                      /* akceptowanie *.inc pod warunkiem, że nie zawiera "epr" w środku */
                                      private final java.util.regex.Pattern pattern = java.util.regex.Pattern.compile (
                                          "\\bepr\\b");
                                      
                                      @Override
                                      public boolean acceptsCharSequence (@NotNull CharSequence s)
                                      {
                                          return s.toString().endsWith (".inc") && !pattern.matcher (s).find ();
                                      }
                                      
                                      @NotNull
                                      @Override
                                      public String getPresentableString ()
                                      {
                                          return "*.inc && ! *epr*";
                                      }
                                  });

        fileTypeConsumer.consume (EprFileType.INSTANCE,
                                  new ExtensionFileNameMatcher ("epr"),
                                  new FileNameMatcher ()
                                  {
                                      /* akceptowanie *.inc pod warunkiem, że zawiera "epr" w środku */
                                      private final java.util.regex.Pattern pattern = java.util.regex.Pattern.compile (
                                          "\\bepr\\b");

                                      @Override
                                      public boolean acceptsCharSequence (@NotNull CharSequence s)
                                      {
                                          return s.toString().endsWith (".inc") && pattern.matcher (s).find ();
                                      }

                                      @NotNull
                                      @Override
                                      public String getPresentableString ()
                                      {
                                          return "*.inc && *epr*";
                                      }
                                  });
    }
}
