package pl.sokrates.intellisowa.leshlang.psi;

import com.intellij.psi.tree.IElementType;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;
import pl.sokrates.intellisowa.leshlang.LeshLanguage;

public class LeshTokenType extends IElementType
{
    public LeshTokenType (@NotNull @NonNls String debugName)
    {
        super (debugName, LeshLanguage.INSTANCE);
    }

    @Override
    public String toString ()
    {
        return "LeshTokenType." + super.toString ();
    }
}
