package pl.sokrates.intellisowa.leshlang.psi.impl;

import com.intellij.navigation.ItemPresentation;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.util.IncorrectOperationException;
import org.jetbrains.annotations.Nullable;
import pl.sokrates.intellisowa.SokratesIcons;
import pl.sokrates.intellisowa.epr.psi.EprElementFactory;
import pl.sokrates.intellisowa.leshlang.psi.*;
import pl.sokrates.intellisowa.leshlang.psi.refs.LeshVariableRef;

import javax.swing.*;
import java.io.File;
import java.util.stream.Collectors;

public class LeshPsiImplUtil
{
    public static LeshSubroutineName getSubroutineName(LeshProcedure proc) {
        return proc.getProcedureName();
    }
    
    public static LeshSubroutineName getSubroutineName(LeshFunction func) {
        return func.getFunctionName();
    }
    
    public static PsiElement setName (LeshProcedureName element, String newName)
    {
        var paramNames = ((LeshProcedure) element.getParent())
                             .getFormalArgumentList()
                             .stream()
                             .map(param -> param.getName())
                             .collect(Collectors.toList());
        var procedure = LeshElementFactory.createProcedure (element.getProject (), newName, paramNames);
        if (procedure == null)
            throw new IncorrectOperationException ("Invalid name for procedure");
        LeshProcedureName newElement = procedure.getProcedureName ();
        element.replace (newElement);
        return newElement;
    }
    
    public static PsiElement setName (LeshFunctionName element, String newName)
    {
        var paramNames = ((LeshFunction) element.getParent())
                             .getFormalArgumentList()
                             .stream()
                             .map(param -> param.getName())
                             .collect(Collectors.toList());
        var function = LeshElementFactory.createFunction (element.getProject (), newName, paramNames);
        if (function == null)
            throw new IncorrectOperationException ("Invalid name for function");
        var newElement = function.getFunctionName();
        element.replace (newElement);
        return newElement;
    }
    
    public static PsiElement setName (LeshFormalArgument element, String newName)
    {
        var oldProcedure = ((LeshProcedure) element.getParent());
        var oldParams = oldProcedure.getFormalArgumentList();
        var index = oldParams.indexOf(element);
        var paramNames = oldParams
                             .stream()
                             .map(param -> param.getName())
                             .collect(Collectors.toList());
        paramNames.set(index, newName);
        var procedure = LeshElementFactory.createProcedure (element.getProject (), oldProcedure.getProcedureName().getName(), paramNames);
        if (procedure == null)
            throw new IncorrectOperationException ("Invalid name for parameter");
        var newElement = procedure.getFormalArgumentList().get(index);
        element.replace (newElement);
        return newElement;
    }
    
    public static PsiElement setName (LeshLetName element, String newName)
    {
        var binding = LeshElementFactory.createLetBinding(element.getProject (), newName);
        if (binding == null)
            throw new IncorrectOperationException ("Invalid name for local variable");
        var newElement = binding.getLetName();
        element.replace (newElement);
        return newElement;
    }

    public static LeshVariableRef getNewReference (LeshVariable element)
    {
        return new LeshVariableRef (element);
    }

    public static LeshIncludePath handleRename (LeshIncludePath element, String newPath)
    {
        newPath = newPath.replace (File.separator, "\\");
        PsiElement newElement = EprElementFactory.createInclude (element.getProject (), newPath);
        if (newElement == null)
            throw new IncorrectOperationException ("Invalid name for file");
        for (PsiElement el : newElement.getChildren ()) {
            if (el instanceof LeshIncludePath) {
                element.replace (el);
                return (LeshIncludePath) el;
            }
        }
        throw new IncorrectOperationException ("Oops?");
    }

    public static ItemPresentation getPresentation (final LeshProcedureName element)
    {
        return new ItemPresentation ()
        {
            @Nullable
            @Override
            public String getPresentableText ()
            {
                var proc = (LeshProcedure) element.getParent();
                return element.getText () + "(" + proc.getFormalArgumentList().stream().map(param -> param.getName()).collect(
                    Collectors.joining(", ")) + ")";
            }

            @Nullable
            @Override
            public String getLocationString ()
            {
                PsiFile containingFile = element.getContainingFile ();
                return containingFile == null ? null : containingFile.getName ();
            }

            @Nullable
            @Override
            public Icon getIcon (boolean unused)
            {
                return SokratesIcons.LESH_LANG;
            }
        };
    }
}
