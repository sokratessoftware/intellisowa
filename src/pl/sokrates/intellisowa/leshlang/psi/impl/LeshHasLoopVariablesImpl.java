package pl.sokrates.intellisowa.leshlang.psi.impl;

import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.intellij.lang.ASTNode;
import org.jetbrains.annotations.NotNull;
import pl.sokrates.intellisowa.leshlang.psi.LeshHasLoopVariables;
import pl.sokrates.intellisowa.leshlang.psi.LeshLoopVariable;

public class LeshHasLoopVariablesImpl extends ASTWrapperPsiElement implements LeshHasLoopVariables
{
    final LeshLoopVariable ___i__ = new LeshLoopVariable(this, "__i__");
    final LeshLoopVariable ___ilimit__ = new LeshLoopVariable(this, "__ilimit__");
    
    public LeshHasLoopVariablesImpl (@NotNull ASTNode node)
    {
        super(node);
    }
    
    
    @Override
    public LeshLoopVariable __i__ ()
    {
        return ___i__;
    }
    
    @Override
    public LeshLoopVariable __ilimit__ ()
    {
        return ___ilimit__;
    }
}
