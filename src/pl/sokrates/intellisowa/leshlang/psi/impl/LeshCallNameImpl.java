package pl.sokrates.intellisowa.leshlang.psi.impl;

import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiReference;
import org.jetbrains.annotations.NotNull;
import pl.sokrates.intellisowa.leshlang.psi.LeshCall;
import pl.sokrates.intellisowa.leshlang.psi.LeshCallName;
import pl.sokrates.intellisowa.leshlang.psi.refs.LeshSubroutineRef;

public abstract class LeshCallNameImpl extends ASTWrapperPsiElement implements LeshCallName
{
    public LeshCallNameImpl (@NotNull ASTNode node)
    {
        super(node);
    }
    
    @Override
    public LeshCall getCall () {
        var parent = getParent();
        if (parent instanceof LeshCall)
            return (LeshCall) parent;
        return null;
    }
    
    @Override
    public PsiReference getReference ()
    {
        return new LeshSubroutineRef(this);
    }
    
    //    
//    @Override
//    public final @Nullable PsiReference getReference ()
//    {
//        return this;
//    }
//    
//    @Override
//    public final PsiReference @NotNull [] getReferences ()
//    {
//        return new PsiReference[] { this };
//    }
//    
//    @Override
//    public boolean isSoft ()
//    {
//        return false;
//    }
//    
//    @Override
//    public @NotNull PsiElement getElement ()
//    {
//        return this;
//    }
//    
//    @Override
//    public @NotNull TextRange getRangeInElement ()
//    {
//        return new TextRange(0, getTextLength());
//    }
//    
//    @Override
//    public @NotNull
//    @NlsSafe String getCanonicalText ()
//    {
//        return getName();
//    }
//    
//    @Override
//    public LeshCallName handleElementRename (@NotNull String newElementName) throws IncorrectOperationException
//    {
//        PsiElement newElement = LeshElementFactory.createProcedureCall(getProject (), newName);
//        if (newElement == null)
//            throw new IncorrectOperationException ("Niedozwolona nazwa dla procedury.");
//        myElement.replace (newElement);
//        return newElement;
//
//    }
}
