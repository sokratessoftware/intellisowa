package pl.sokrates.intellisowa.leshlang.psi.impl;

import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import pl.sokrates.intellisowa.leshlang.psi.LeshNamedElement;

public abstract class LeshNamedElementImpl extends ASTWrapperPsiElement implements LeshNamedElement
{
    public LeshNamedElementImpl (@NotNull ASTNode node)
    {
        super (node);
    }
    
    @Override
    public @Nullable PsiElement getNameIdentifier ()
    {
        return this;
    }
    
    @Override
    public String getName ()
    {
        return getText();
    }
}
