package pl.sokrates.intellisowa.leshlang.psi.impl;

import com.intellij.lang.ASTNode;
import com.intellij.navigation.ItemPresentation;
import com.intellij.psi.PsiFile;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import pl.sokrates.intellisowa.SokratesIcons;
import pl.sokrates.intellisowa.leshlang.psi.LeshSubroutine;
import pl.sokrates.intellisowa.leshlang.psi.LeshSubroutineName;

import javax.swing.*;
import java.util.stream.Collectors;

public abstract class LeshSubroutineNameImpl extends LeshNamedElementImpl implements LeshSubroutineName
{
    public LeshSubroutineNameImpl (@NotNull ASTNode node)
    {
        super(node);
    }
    
    @Override
    public LeshSubroutine getSubroutine() {
        return (LeshSubroutine) getParent();
    }
    
    @Override
    public ItemPresentation getPresentation ()
    {
        return new ItemPresentation ()
        {
            @Nullable
            @Override
            public String getPresentableText ()
            {
                var proc = getSubroutine();
                return proc.getNode().getFirstChildNode().getText() + " " + getText () + "(" + proc.getFormalArgumentList().stream().map(param -> param.getName()).collect(
                    Collectors.joining(", ")) + ")";
            }
        
            @Nullable
            @Override
            public String getLocationString ()
            {
                PsiFile containingFile = getContainingFile ();
                return containingFile == null ? null : containingFile.getName ();
            }
            
            @Nullable
            @Override
            public Icon getIcon (boolean unused)
            {
                return SokratesIcons.LESH_LANG;
            }
        };
    
    }
}
