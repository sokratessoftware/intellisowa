package pl.sokrates.intellisowa.leshlang.psi.impl;

import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import org.jetbrains.annotations.NotNull;
import pl.sokrates.intellisowa.leshlang.psi.LeshCall;
import pl.sokrates.intellisowa.leshlang.psi.LeshCallName;
import pl.sokrates.intellisowa.leshlang.psi.LeshExpression;

public abstract class LeshCallImpl extends ASTWrapperPsiElement implements LeshCall
{
    public LeshCallImpl (@NotNull ASTNode node)
    {
        super(node);
    }
    
    @Override
    public LeshCallName getCallName ()
    {
        return findChildByClass(LeshCallName.class);
    }
    
    @Override
    public int getNumberOfArguments ()
    {
        var numArguments = 0;
        for (PsiElement child = getFirstChild(); child != null; child = child.getNextSibling()) {
            if (child instanceof LeshExpression)
                numArguments++;
        }
        return numArguments;
    }
    
    @Override
    public String getSignature() {
        var numArguments = 0;
        LeshCallName name = null;
        for (PsiElement child = getFirstChild(); child != null; child = child.getNextSibling()) {
            if (child instanceof LeshExpression)
                numArguments++;
            else if (child instanceof LeshCallName)
                name = (LeshCallName) child;
        }
        return name.getText().toLowerCase() + "/" + numArguments;
    }
}
