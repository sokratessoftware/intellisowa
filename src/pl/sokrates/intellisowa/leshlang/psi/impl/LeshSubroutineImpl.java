package pl.sokrates.intellisowa.leshlang.psi.impl;

import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.intellij.lang.ASTNode;
import com.intellij.navigation.ItemPresentation;
import org.jetbrains.annotations.NotNull;
import pl.sokrates.intellisowa.leshlang.psi.LeshSubroutine;

public abstract class LeshSubroutineImpl extends ASTWrapperPsiElement implements LeshSubroutine
{
    public LeshSubroutineImpl (@NotNull ASTNode node)
    {
        super(node);
    }
    
    @Override
    public ItemPresentation getPresentation ()
    {
        return getSubroutineName().getPresentation();
    }
    
    @Override
    public String getName ()
    {
        return getSubroutineName().getName();
    }
}
