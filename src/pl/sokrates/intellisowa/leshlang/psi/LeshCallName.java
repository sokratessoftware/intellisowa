package pl.sokrates.intellisowa.leshlang.psi;

import com.intellij.psi.PsiElement;

public interface LeshCallName extends PsiElement
{
    LeshCall getCall ();
}
