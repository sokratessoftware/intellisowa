package pl.sokrates.intellisowa.leshlang.psi;

import com.intellij.icons.AllIcons;
import com.intellij.lang.Language;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiManager;
import com.intellij.psi.impl.FakePsiElement;
import com.intellij.psi.tree.IElementType;
import com.intellij.util.IncorrectOperationException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import pl.sokrates.intellisowa.leshlang.LeshLanguage;

import javax.swing.*;

// No więc ta klasa reprezentuje fikcyjną deklarację zmiennej globalnej.
// Jest fikcyjna, bo w LL nie ma deklaracji, zmienne tworzone są przy pierwszym przypisaniu, a takich może być wiele.
// Zmienne lokalne tego nie używają.
public class LeshVariableAnchor extends FakePsiElement
{
    public  final String     name;
    public  final String     key;
    private final PsiManager manager;
    
    public LeshVariableAnchor (PsiManager manager, String name) {
        this.name = name;
        this.key  = name.toLowerCase ();
        this.manager = manager;
    }
    
    @Override
    public PsiManager getManager ()
    {
        return manager;
    }
    
    @Override
    public PsiElement getParent ()
    {
        return null;
    }
    
    @NotNull
    @Override
    public Language getLanguage ()
    {
        return LeshLanguage.INSTANCE;
    }
    
    @Override
    public String getName ()
    {
        return name;
    }
    
    @Nullable
    @Override
    public String getText ()
    {
        return name;
    }
    
    public PsiElement getNameIdentifier ()
    {
        return this;
    }
    
    @Override
    public LeshVariableAnchor setName (@NotNull String name)
    {
        PsiElement newElement = LeshElementFactory.createAssignment (getProject (), name);
        if (newElement == null)
            throw new IncorrectOperationException ("Invalid name for variable");
        
        return new LeshVariableAnchor (manager, name);
    }
    
    @Nullable
    @Override
    public Icon getIcon (boolean open)
    {
        return AllIcons.Nodes.Variable;
    }
    
    @Override
    public boolean isValid ()
    {
        return true;
    }
    
    @Override
    public PsiFile getContainingFile ()
    {
        return null;
    }
    
    @Override
    public PsiElement replace (@NotNull PsiElement newElement) throws IncorrectOperationException
    {
        return newElement;
    }
    
    @Override
    public boolean isWritable ()
    {
        return true;
    }
    
}
