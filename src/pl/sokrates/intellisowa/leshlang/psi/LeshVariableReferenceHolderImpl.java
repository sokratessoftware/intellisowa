package pl.sokrates.intellisowa.leshlang.psi;

import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import org.jetbrains.annotations.NotNull;
import pl.sokrates.intellisowa.common.psi.PsiReferenceHolderImpl;
import pl.sokrates.intellisowa.leshlang.psi.impl.LeshStatementWhileImpl;

public abstract class LeshVariableReferenceHolderImpl extends PsiReferenceHolderImpl implements LeshVariableReferenceHolder
{
//    private PsiElement localTarget = null;
    
    @Override
    public PsiElement getLocalTarget() {
//        return localTarget;
        var name = getText().toLowerCase();
        PsiElement psi = this;

        // szukamy pasującej deklaracji zmiennej lokalnej wyżej w drzewie
        boolean isLoopVar = name.equals("__i__") || name.equals("__ilimit__");
        while (psi != null) {
            if (psi instanceof LeshStatementLevel) {
                if (isLoopVar && (psi instanceof LeshHasLoopVariables)) {
                    var whl = (LeshHasLoopVariables) psi;
                    if (name.equals("__i__"))
                        return whl.__i__();
                    else
                        return whl.__ilimit__();
                }
                
                var sibling = psi;
                while (sibling != null) {
                    sibling = sibling.getPrevSibling();
                    if (sibling instanceof LeshLetBinding) {
                        var let = ((LeshLetBinding) sibling).getLetName();
                        var n = let.getName().toLowerCase();
                        if (n.equals(name)) {
                            return let;
                        }
                    }
                }
            }
            else if (psi instanceof LeshSubroutine) {
                var procedure = (LeshSubroutine) psi;
                for (var param : procedure.getFormalArgumentList()) {
                    if (param.getName().toLowerCase().equals(name)) {
                        return param;
                    }
                }
            }

            psi = psi.getParent();
        }
        
        return null;
    }
    
    public LeshVariableReferenceHolderImpl (@NotNull ASTNode node)
    {
        super(node);
//        
//        var name = getText().toLowerCase();
//        var psi = node.getTreeParent().getPsi();
//
//        // szukamy pasującej deklaracji zmiennej lokalnej wyżej w drzewie
//        while (psi != null) {
//            var statement = psi;
//            while (!(psi instanceof LeshScopeElement)) {
//                statement = psi;
//                psi = psi.getParent();
//                if (psi == null)
//                    return;
//            }
//
//            var sibling = statement;
//
//            while (sibling != null) {
//                sibling = sibling.getPrevSibling();
//                if (sibling instanceof LeshLetBinding) {
//                    var let = ((LeshLetBinding) sibling).getLetName();
//                    var n = let.getName().toLowerCase();
//                    if (n.equals(name)) {
//                        localTarget = let;
//                        return;
//                    }
//                }
//            }
//
//            psi = psi.getParent();
//        }
    }
}
