package pl.sokrates.intellisowa.leshlang.psi;

import com.intellij.navigation.NavigationItem;
import com.intellij.psi.PsiElement;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface LeshSubroutine extends PsiElement, NavigationItem, LeshScopeElement
{
    @NotNull
    List<LeshFormalArgument> getFormalArgumentList();
    
    LeshSubroutineName getSubroutineName();
    
    default int getNumberOfArguments() {
        var numArguments = 0;
        for (PsiElement child = getFirstChild(); child != null; child = child.getNextSibling()) {
            if (child instanceof LeshFormalArgument)
                numArguments++;
        }
        return numArguments;
    }
    
    default String getSignature() {
        var name = getSubroutineName();
        if (name == null)
            return null;
        return name.getName() + "/" + getNumberOfArguments();
    }
}
