package pl.sokrates.intellisowa.leshlang.psi;

import com.intellij.navigation.NavigationItem;

public interface LeshSubroutineName extends LeshNamedElement, NavigationItem
{
    LeshSubroutine getSubroutine();
}
