package pl.sokrates.intellisowa.leshlang.psi;

import com.intellij.psi.tree.IElementType;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;
import pl.sokrates.intellisowa.leshlang.LeshLanguage;

public class LeshElementType extends IElementType
{
    public LeshElementType (@NotNull @NonNls String debugName)
    {
        super (debugName, LeshLanguage.INSTANCE);
    }
}
