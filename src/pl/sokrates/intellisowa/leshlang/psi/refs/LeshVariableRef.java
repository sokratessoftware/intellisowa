package pl.sokrates.intellisowa.leshlang.psi.refs;

import com.intellij.codeInsight.lookup.LookupElement;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.project.impl.ProjectImpl;
import com.intellij.openapi.util.Key;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.*;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.util.IncorrectOperationException;
import com.intellij.util.indexing.StorageException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import pl.sokrates.intellisowa.common.IncludeUtil;
import pl.sokrates.intellisowa.common.psi.refs.BaseRef;
import pl.sokrates.intellisowa.leshlang.index.IndexUtil;
import pl.sokrates.intellisowa.leshlang.psi.*;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class LeshVariableRef extends BaseRef<LeshVariable> implements PsiPolyVariantReference
{
    public LeshVariableRef (@NotNull LeshVariable element)
    {
        super (element);
    }
    
    @Override
    public PsiElement handleElementRename (String newName)
    {
        PsiElement newElement = LeshElementFactory.createAssignment (myElement.getProject (), newName);
        if (newElement == null)
            throw new IncorrectOperationException ("Niedozwolona nazwa dla zmiennej.");
        newElement = newElement.getFirstChild ();
        myElement.replace (newElement);
        return newElement;
    }
    
    @NotNull
    @Override
    public Object[] getVariants ()
    {
        return LookupElement.EMPTY_ARRAY;
    }
    
    private static class AnchorIndex extends ConcurrentHashMap<String, LeshVariableAnchor>
    {
    }
    
    private static final Key<AnchorIndex> indexKey = new Key<> ("pl.sokrates.intellisowa.leshlang.psi.refs.LeshVariableRef.INDEX");
    
    private static LeshVariableAnchor getVariableAnchor (Project project, String ident)
    {
        ProjectImpl proj = (ProjectImpl) project;
        AnchorIndex index = proj.getUserData (indexKey);
        if (index == null) {
            index = new AnchorIndex ();
            index = proj.putUserDataIfAbsent (indexKey, index);
        }
        
        ident = ident.toLowerCase ();
        LeshVariableAnchor var = index.get (ident);
        if (var == null) {
            PsiManager manager = PsiManager.getInstance (project);
            var = index.putIfAbsent (ident, new LeshVariableAnchor (manager, ident));
        }
        
        return var;
    }
    
    @NotNull
    @Override
    public ResolveResult[] multiResolve (boolean incompleteCode)
    {
        PsiElement result = resolve ();
        if (result != null)
            return new ResolveResult[] { new PsiElementResolveResult (result) };
        else
            return ResolveResult.EMPTY_ARRAY;
    }
    
    @Nullable
    @Override
    public PsiElement resolve ()
    {
        var local = myElement.getLocalTarget();
        if (local != null)
            return local;
        
        return getVariableAnchor (myElement.getProject (),
                                  myElement.getText ());
    }
    
    public boolean wasAssigned ()
    {
        //if (myElement.getLocalTarget() != null)
        //    return true;
        
        if (myElement.getFirstChild ()
                     .getNode ()
                     .getElementType () != LeshTypes.IDENTIFIER)
            return true;
        
        final LeshFile file = (LeshFile) myElement.getContainingFile ();
        final Set<VirtualFile> visitedFiles = new HashSet<> ();
        
        IncludeUtil.visitIncludes (file, visitedFiles);
        
        try {
            return IndexUtil.isVariableInScope (GlobalSearchScope.filesScope (myElement.getProject (), visitedFiles),
                                                myElement.getText ().toLowerCase ());
        } catch (StorageException ex) {
            System.err.println ("isVariableInScope() crash");
            return true;
        }
    }
    
    @Override
    public boolean isSoft ()
    {
        return true;
    }
}
