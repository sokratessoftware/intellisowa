package pl.sokrates.intellisowa.leshlang.psi.refs;

import com.intellij.psi.*;
import com.intellij.util.IncorrectOperationException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import pl.sokrates.intellisowa.common.psi.refs.BaseRef;
import pl.sokrates.intellisowa.common.psi.refs.GenericIncludeRef;
import pl.sokrates.intellisowa.leshlang.LeshBuiltins;
import pl.sokrates.intellisowa.leshlang.LeshUtil;
import pl.sokrates.intellisowa.leshlang.psi.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class LeshSubroutineRef extends BaseRef<LeshCallName> implements PsiPolyVariantReference
{
    public LeshSubroutineRef (@NotNull LeshCallName element)
    {
        super (element);
    }
    
    public interface CollectCallback
    {
        void collect (LeshSubroutineName element);
    }
    
    public static void collectFromFile (LeshFile file,
                                        Class<? extends LeshSubroutine> cls,
                                        String name,
                                        int numParameters,
                                        PsiElement at,
                                        Set<PsiFile> visitedFiles,
                                        CollectCallback callback)
    {
        if (name != null) name = name.toLowerCase ();
        
        var it = LeshUtil.findChildrenInCurrentSection(file, at).filter(x -> cls.isInstance(x)).iterator();
        while (it.hasNext()) {
            var procedure = (LeshSubroutine) it.next();
            LeshSubroutineName procname = procedure.getSubroutineName();
            if (procname != null && (name == null || (procname.getName().toLowerCase().equals(name) && (numParameters < 0 || procedure.getNumberOfArguments() == numParameters)))) {
                callback.collect (procname);
            }
        }
    
        it = LeshUtil.findChildrenInCurrentSection(file, at).filter(x -> x instanceof LeshInclude).iterator();
        while (it.hasNext()) {
            var include = (LeshInclude) it.next();
            LeshIncludePath includePath = include.getIncludePath ();
            if (includePath == null) continue;
            
            PsiFile psiFile = ((GenericIncludeRef) includePath.getReference ()).resolve ();
            if (psiFile instanceof LeshFile) {
                if (visitedFiles == null) {
                    visitedFiles = new HashSet<>();
                    visitedFiles.add (file);
                }
                
                if (!visitedFiles.contains (psiFile)) {
                    visitedFiles.add (psiFile);
                    collectFromFile ((LeshFile) psiFile, cls, name, numParameters, null, visitedFiles, callback);
                }
            }
        }
    }
    
    @NotNull
    @Override
    public ResolveResult[] multiResolve (boolean incompleteCode)
    {
        var call = myElement.getCall();
        if (call == null)
            return ResolveResult.EMPTY_ARRAY;
        
        final ArrayList<ResolveResult> results = new ArrayList<> ();
        
        var project = myElement.getProject();
        var adj = call.getParent() instanceof LeshPassOp ? 1 : 0;
        var numArguments = call.getNumberOfArguments() + adj;
        var signature = myElement.getText().toLowerCase() + "/" + numArguments;
        var isProcedure = myElement instanceof LeshProcedureCall;
        
        if (isProcedure) {
            var builtin = LeshBuiltins.getBuiltinProcedures(project).get(signature);
            if (builtin != null)
                results.add (new PsiElementResolveResult(builtin.getSubroutineName()));
    
            collectFromFile ((LeshFile) myElement.getContainingFile (),
                             LeshProcedure.class,
                             myElement.getText (),
                             numArguments,
                             myElement,
                             null,
                             element -> results.add (new PsiElementResolveResult (element)));
    
        }
        else {
            var builtin = LeshBuiltins.getBuiltinFunctions(project).get(signature);
            if (builtin != null)
                results.add (new PsiElementResolveResult(builtin.getSubroutineName()));
    
            collectFromFile ((LeshFile) myElement.getContainingFile (),
                             LeshFunction.class,
                             myElement.getText (),
                             numArguments,
                             myElement,
                             null,
                             element -> results.add (new PsiElementResolveResult (element)));
    
        }
        
        if (results.size () > 0)
            return results.toArray (new ResolveResult[results.size ()]);
    
        return ResolveResult.EMPTY_ARRAY;
    }
    
    @Nullable
    @Override
    public PsiElement resolve ()
    {
        ResolveResult[] resolveResults = multiResolve (false);
        return resolveResults.length == 1 ? resolveResults[0].getElement () : null;
    }
    
    @Override
    public LeshCallName handleElementRename (String newName)
    {
        var project = myElement.getProject();
        LeshCallName newElement = myElement instanceof LeshProcedureCall
                                    ? LeshElementFactory.createProcedureCall(project, newName)
                                    : LeshElementFactory.createFunctionCall(project, newName);
        if (newElement == null)
            throw new IncorrectOperationException("Niedozwolona nazwa dla funkcji lub procedury.");
        myElement.replace (newElement);
        return newElement;
    }
    
    @Override
    public boolean isSoft ()
    {
        return false;
    }
}
