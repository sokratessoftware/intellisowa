package pl.sokrates.intellisowa.leshlang.psi;

import com.intellij.psi.PsiNameIdentifierOwner;

public interface LeshNamedElement extends PsiNameIdentifierOwner
{
}
