package pl.sokrates.intellisowa.leshlang.psi;

import com.intellij.icons.AllIcons;
import com.intellij.lang.Language;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.impl.FakePsiElement;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import pl.sokrates.intellisowa.leshlang.LeshLanguage;

import javax.swing.*;

// No więc ta klasa reprezentuje fikcyjną deklarację zmiennej globalnej.
// Jest fikcyjna, bo w LL nie ma deklaracji, zmienne tworzone są przy pierwszym przypisaniu, a takich może być wiele.
// Zmienne lokalne tego nie używają.
public class LeshLoopVariable extends FakePsiElement
{
    public  final String     name;
    public  final String     key;
    private final PsiElement parent;
    
    public LeshLoopVariable (PsiElement parent, String name) {
        this.parent = parent;
        this.name = name;
        this.key  = name.toLowerCase ();
    }
    
    @Override
    public PsiElement getParent ()
    {
        return parent;
    }
    
    @NotNull
    @Override
    public Language getLanguage ()
    {
        return LeshLanguage.INSTANCE;
    }
    
    @Override
    public String getName ()
    {
        return name;
    }
    
    @Nullable
    @Override
    public String getText ()
    {
        return name;
    }
    
    public PsiElement getNameIdentifier ()
    {
        return this;
    }
    
    @Nullable
    @Override
    public Icon getIcon (boolean open)
    {
        return AllIcons.Nodes.Variable;
    }
    
    @Override
    public boolean isValid ()
    {
        return true;
    }
    
    @Override
    public PsiFile getContainingFile ()
    {
        return null;
    }
    
    @Override
    public boolean isWritable ()
    {
        return false;
    }
    
}
