package pl.sokrates.intellisowa.leshlang.psi;

import com.intellij.extapi.psi.PsiFileBase;
import com.intellij.openapi.fileTypes.FileType;
import com.intellij.psi.FileViewProvider;
import org.jetbrains.annotations.NotNull;
import pl.sokrates.intellisowa.leshlang.LeshFileType;
import pl.sokrates.intellisowa.leshlang.LeshLanguage;

import javax.swing.*;

public class LeshFile extends PsiFileBase
{
    public LeshFile (@NotNull FileViewProvider viewProvider)
    {
        super (viewProvider, LeshLanguage.INSTANCE);
    }

    @NotNull
    @Override
    public FileType getFileType ()
    {
        return LeshFileType.INSTANCE;
    }

    @Override
    public String toString ()
    {
        return "LeshLang(" + getName () + ")";
    }

    @Override
    public Icon getIcon (int flags)
    {
        return super.getIcon (flags);
    }
}