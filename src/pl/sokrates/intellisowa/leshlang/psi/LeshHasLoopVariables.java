package pl.sokrates.intellisowa.leshlang.psi;

import com.intellij.psi.PsiElement;

public interface LeshHasLoopVariables extends PsiElement
{
    public LeshLoopVariable __i__();
    public LeshLoopVariable __ilimit__();
}
