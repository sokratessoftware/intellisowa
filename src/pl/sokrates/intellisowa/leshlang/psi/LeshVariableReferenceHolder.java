package pl.sokrates.intellisowa.leshlang.psi;

import com.intellij.psi.PsiElement;
import pl.sokrates.intellisowa.common.psi.PsiReferenceHolder;

public interface LeshVariableReferenceHolder extends PsiReferenceHolder
{
    PsiElement getLocalTarget();
}
