package pl.sokrates.intellisowa.leshlang.psi;

import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiFileFactory;
import com.intellij.psi.util.PsiTreeUtil;
import pl.sokrates.intellisowa.leshlang.LeshFileType;

import java.util.List;
import java.util.stream.Collectors;

public class LeshElementFactory
{
    public static LeshProcedure createProcedure (Project project, String name, List<String> arguments)
    {
        return createFile (project, "procedure " + name + " (" + arguments.stream().collect(Collectors.joining(", ")) + ") begin end").findChildByClass (LeshProcedure.class);
    }
    
    public static LeshFunction createFunction (Project project, String name, List<String> arguments)
    {
        return createFile (project, "function " + name + " (" + arguments.stream().collect(Collectors.joining(", ")) + ") = ''").findChildByClass (LeshFunction.class);
    }
    
    public static LeshLetBinding createLetBinding (Project project, String name)
    {
        return createFile (project, "let " + name + " = ''").findChildByClass (LeshLetBinding.class);
    }
    
    public static LeshCallName createProcedureCall (Project project, String name)
    {
        return PsiTreeUtil.findChildOfType(createFile(project, "begin " + name + " end"), LeshCallName.class);
    }
    
    public static LeshCallName createFunctionCall (Project project, String name)
    {
        return PsiTreeUtil.findChildOfType(createFile(project, "begin void " + name + "() end"), LeshCallName.class);
    }

    public static LeshAssignment createAssignment (Project project, String name)
    {
        return createFile (project, name + "= ''").findChildByClass (LeshAssignment.class);
    }

    public static LeshInclude createInclude (Project project, String name)
    {
        return createFile (project, "\n[PLIK " + name + "]\n").findChildByClass (LeshInclude.class);
    }

    public static LeshFile createFile (Project project, String text)
    {
        String name = "dummy.ppr";
        return (LeshFile) PsiFileFactory.getInstance (project).
                                                                  createFileFromText (name,
                                                                                      LeshFileType.INSTANCE,
                                                                                      text);
    }
}