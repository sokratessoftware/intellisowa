package pl.sokrates.intellisowa.leshlang.psi;

import com.intellij.psi.PsiElement;

import java.util.List;

public interface LeshCall extends PsiElement
{
    LeshCallName getCallName();
    
    List<LeshExpression> getExpressionList();
    
    String getSignature();
    
    int getNumberOfArguments();
}
