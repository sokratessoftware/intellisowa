package pl.sokrates.intellisowa.leshlang;

import com.intellij.codeInsight.lookup.LookupElement;
import com.intellij.lang.ASTNode;
import com.intellij.lang.parameterInfo.CreateParameterInfoContext;
import com.intellij.lang.parameterInfo.ParameterInfoContext;
import com.intellij.lang.parameterInfo.ParameterInfoHandler;
import com.intellij.lang.parameterInfo.UpdateParameterInfoContext;
import com.intellij.psi.PsiElement;
import com.intellij.psi.util.PsiTreeUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import pl.sokrates.intellisowa.leshlang.psi.LeshExpression;

public abstract class LeshParameterInfoHandler<ParameterOwner extends Object & PsiElement, ParameterType> implements ParameterInfoHandler<ParameterOwner, ParameterType>
{
    protected abstract Class<ParameterOwner> getPsiClass();
    
    static final Object[] EMPTY_ARRAY = new Object [0];
    
    @Nullable
    @Override
    public ParameterOwner findElementForParameterInfo (@NotNull CreateParameterInfoContext context)
    {
        return PsiTreeUtil.getNonStrictParentOfType (context.getFile ().findElementAt (context.getEditor ().getCaretModel ().getOffset ()), getPsiClass());
    }
    
    @Nullable
    @Override
    public ParameterOwner findElementForUpdatingParameterInfo (@NotNull UpdateParameterInfoContext context)
    {
        return PsiTreeUtil.getNonStrictParentOfType (context.getFile ().findElementAt (context.getEditor ().getCaretModel ().getOffset ()), getPsiClass());
    }
    
    @Override
    public void updateParameterInfo (@NotNull ParameterOwner element,
                                     @NotNull UpdateParameterInfoContext context)
    {
        ParameterOwner actualElement = (ParameterOwner) context.getParameterOwner ();
        
        int at = context.getEditor ().getCaretModel ().getOffset () - actualElement.getTextOffset ();
        
        StringBuilder sb = new StringBuilder (actualElement.getTextLength ());
        for (ASTNode child : actualElement.getNode ().getChildren (null)) {
            if (child.getPsi () instanceof LeshExpression)
                for (int n = 0; n < child.getTextLength (); n++) sb.append (' ');
            else
                sb.append (child.getText ());
        }
        String text = sb.toString ();
        
        if (at < 0 || at >= text.length ()) {
            context.setUIComponentEnabled (0, false);
            context.setCurrentParameter (0);
            return;
        }
        
        context.setUIComponentEnabled (0, true);
        
        int lparen = text.indexOf ('(');
        if (at > lparen) {
            int i = 1;
            for (int c = lparen; c < at; c++) {
                if (text.charAt (c) == ',') i++;
            }
            context.setCurrentParameter (i);
        }
        else
            context.setCurrentParameter (0);
    }
}
