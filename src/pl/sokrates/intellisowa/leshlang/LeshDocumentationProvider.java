package pl.sokrates.intellisowa.leshlang;

import com.intellij.lang.ASTNode;
import com.intellij.lang.documentation.DocumentationProviderEx;
import com.intellij.openapi.editor.Editor;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiManager;
import com.intellij.psi.util.PsiTreeUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import pl.sokrates.intellisowa.leshlang.psi.*;

import java.awt.*;
import java.util.List;
import java.util.stream.Collectors;

public class LeshDocumentationProvider extends DocumentationProviderEx
{
    @Nullable
    @Override
    public String getQuickNavigateInfo (PsiElement element, PsiElement originalElement)
    {
        return generateDoc(element, originalElement);
    }

    @Nullable
    @Override
    public List<String> getUrlFor (PsiElement psiElement, PsiElement psiElement1)
    {
        return null;
    }

    @Nullable
    @Override
    public PsiElement getDocumentationElementForLink (PsiManager psiManager, String s, PsiElement psiElement)
    {
        return null;
    }

    @Nullable
    @Override
    public PsiElement getDocumentationElementForLookupItem (PsiManager psiManager, Object o, PsiElement psiElement)
    {
        return null;
    }

    @Nullable
    @Override
    public String generateDoc (PsiElement element, @Nullable PsiElement originalElement)
    {
        if (element instanceof LeshCallName || element instanceof LeshSubroutineName) {
            var name = element instanceof LeshCallName ? (LeshSubroutineName) element.getReference().resolve() : (LeshSubroutineName) element;
            if (name == null)
                return null;
            
            var procedure = name.getSubroutine();
            if (procedure == null)
                return null;
            
            ASTNode       node = procedure.getNode ();
            StringBuilder sb   = new StringBuilder ();
            
            if (procedure instanceof LeshProcedure)
                sb.append("procedure ");
            else
                sb.append("function ");
            
            sb.append ("<b>");
            sb.append (element.getText());
            sb.append ("</b> (");
            
            var params = procedure.getFormalArgumentList();
            for (var param : params) {
                sb.append ("<b>");
                sb.append(param.getName());
                sb.append("</b>, ");
            }
            if (!params.isEmpty())
                sb.setLength(sb.length() - 2);
            
            sb.append (") @ ");
            sb.append (element.getContainingFile ().getName ());
            sb.append ("<br/><br/>");

            for (ASTNode comment : node.getChildren (LeshParserDefinition.COMMENTS)) {
                if (comment.getTextLength () > 1) {
                    sb.append (comment.getText ().replace ("\n", "<br/>"));
                    sb.append ("<br/>");
                }
            }

            return sb.toString ();
        }

        if (element instanceof LeshVariableAnchor)
            return element.getText ();
        
        return null;
    }

    @Nullable
    @Override
    public Image getLocalImageForElement (@NotNull PsiElement element, @NotNull String imageSpec)
    {
        return super.getLocalImageForElement (element, imageSpec);
    }

//    @Nullable
//    @Override
//    public PsiElement getCustomDocumentationElement (@NotNull Editor editor,
//                                                     @NotNull PsiFile file,
//                                                     @Nullable PsiElement contextElement,
//                                                     int targetOffset)
//    {
//        PsiElement result;
//
//        if ((result = PsiTreeUtil.getNonStrictParentOfType (contextElement, LeshCall.class)) != null)
//            return ((LeshCall) result).getCallName();
//
//        return super.getCustomDocumentationElement (editor, file, contextElement, targetOffset);
//    }
}
