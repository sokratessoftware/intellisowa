package pl.sokrates.intellisowa.leshlang;

import com.intellij.lexer.FlexLexer;
import com.intellij.psi.tree.IElementType;
import pl.sokrates.intellisowa.leshlang.psi.LeshTypes;
import com.intellij.psi.TokenType;

%%

%class LeshLexer
%implements FlexLexer
%unicode
%ignorecase
%function advance
%type IElementType
%eof{  return;
%eof}

CRLF= \n|\r|\r\n
WHITE_SPACE=[\ \t\f]
IDENTIFIER=[A-Za-z_][A-Za-z0-9_]*
LINE_COMMENT=[/][/][^\n\r]*
PATH_COMPONENT=[A-Za-z0-9_\.+-]+

%state INCOMMENT
%state INDIRECTIVE
%state ININCLUDE
%state INGLOBALS
%state INSTRING 

%%

<YYINITIAL> {
    "!"          { return LeshTypes.OP_BANG; }
    "+"          { return LeshTypes.OP_PLUS; }
    "="          { return LeshTypes.OP_EQUALS; }
    "#"          { return LeshTypes.OP_HASH; }
    "or"         { return LeshTypes.OP_OR; }
    ">>"         { return LeshTypes.OP_PASS; }
    ">"          { return LeshTypes.OP_GT; }
    "("          { return LeshTypes.OP_LEFTPAREN; }
    ")"          { return LeshTypes.OP_RIGHTPAREN; }
    ","          { return LeshTypes.OP_COMMA; }
    
    "if"         { return LeshTypes.KW_IF; }
    "else"       { return LeshTypes.KW_ELSE; }
    "elif"       { return LeshTypes.KW_ELIF; }
    "endif"      { return LeshTypes.KW_ENDIF; }
    
    "while"      { return LeshTypes.KW_WHILE; }
    "enddo"      { return LeshTypes.KW_ENDDO; }
    
    "procedure"  { return LeshTypes.KW_PROCEDURE; }
    "begin"      { return LeshTypes.KW_BEGIN; }
    "end"        { return LeshTypes.KW_END; }
    
    "open"       { return LeshTypes.IDENTIFIER_RESERVED; }
    "grupa"      { return LeshTypes.IDENTIFIER_RESERVED; }
    "back"       { return LeshTypes.IDENTIFIER_RESERVED; }
    "skip"       { return LeshTypes.IDENTIFIER_RESERVED; }
    "print"      { return LeshTypes.IDENTIFIER_RESERVED; }
    "trace"      { return LeshTypes.IDENTIFIER_RESERVED; }
    
    "let"        { return LeshTypes.KW_LET; }
    "function"   { return LeshTypes.KW_FUNCTION; }
    "return"     { return LeshTypes.KW_RETURN; }
    "void"       { return LeshTypes.KW_VOID; }
    
    // specjalnie traktowane w lexerze sowy
    "ok"         |
    "pierwotne"  { return LeshTypes.IDENTIFIER_RESERVED; }

//    // normalnie traktowane w lexerze sowy, ale predefiniowane
//    "rekord"     |
//    "lp"         |
//    "header"     |
    
    // specjalnie traktowane w lexerze sowy
    "pole"       |
    "pozycja"    |
    "edytowalne" { return LeshTypes.IDENTIFIER_RESERVED; }

//    "head" |
//    "tail" |
//    "left" |
//    "right" |
//    "drop" |
//    "substr" |
//    "ltrim" | "ltrimw" | "ltrimwc" |
//    "rtrim" | "rtrimw" | "rtrimwc" |
//    "lrtrim" | "lrtrimw" | "lrtrimwc" |
//    "len" | "len8" | "len16" | "len32" |
//    "sep" |
//    "reverse" |
//    "up" |
//    "lo" |
//    "matches" |
//    "position" |
//    "isint" | "int" |
//    "succ" |
//    "add" |
//    "mul" |
//    "mod" |
//    "div" |
//    "gt" |
//    "ge" |
//    "replace" |
//    "trim" |
//    "sort" |
//    "agregate" |
//    "ascii" |
//    "chr" |
//    "random" |
//    "omit" |
//    "subfield" |
//    "marker" |
//    "mak" |
//    "uri" |
//    "utf8" |
//    "pr" |
//    "ta" |
//    "form" |
//    "rform" |
//    "date" |
//    "time" |
//    "days" |
//    "cfg" |
//    "external" |
//    "first" |
//    "next" |
//    "find" |
//    "last" |
//    "select" |
//    "limited" |
//    "refs" |
//    "references" { return LeshTypes.KW_BUILTIN_FUNCTION; }

    "{:" | ":}"        { return LeshTypes.COMMENT; }

    "{"                { yybegin (INCOMMENT); return LeshTypes.COMMENT_START; }
    "["                { yybegin (INDIRECTIVE); return LeshTypes.DIRECTIVE_START; }
      
    {LINE_COMMENT}     { return LeshTypes.COMMENT; }

    "'"                { yybegin(INSTRING); return LeshTypes.STRING; }
    \d+                { return LeshTypes.STRING; }
    \\\d+              { return LeshTypes.CHARACTER; }
    {IDENTIFIER}       { return LeshTypes.IDENTIFIER; }

    ({CRLF}|{WHITE_SPACE})+  { return TokenType.WHITE_SPACE; }
    
    .                  { return TokenType.BAD_CHARACTER; }
}

<INCOMMENT> {
    "}"             { yybegin (YYINITIAL); return LeshTypes.COMMENT_END; }
    ({CRLF}|[^\}])+ { return LeshTypes.COMMENT; }
}

<INDIRECTIVE> {
    "]"            { yybegin (YYINITIAL); return LeshTypes.DIRECTIVE_END; }
    "plik"         { yybegin (ININCLUDE); return LeshTypes.DIRECTIVE_PLIK; }
    "globals"      { yybegin (INGLOBALS); return LeshTypes.DIRECTIVE_GLOBALS; }
    {WHITE_SPACE}+ { return LeshTypes.DIRECTIVE_WS; }
    [^\ \t\f\]\n]+ { return LeshTypes.DIRECTIVE_VALUE; }
    {CRLF}         { yypushback (1); yybegin (YYINITIAL); }
    .              { return LeshTypes.DIRECTIVE_VALUE; }
}

<ININCLUDE> {
    {PATH_COMPONENT} |
    {PATH_COMPONENT}?([/\\]{PATH_COMPONENT}?)+
                   { return LeshTypes.DIRECTIVE_PATH; }

    "]"            { yybegin (YYINITIAL); return LeshTypes.DIRECTIVE_END; }
    {WHITE_SPACE}+ { return LeshTypes.DIRECTIVE_WS; }
    {CRLF}         { yypushback (1); yybegin (YYINITIAL); }
    .              { return TokenType.BAD_CHARACTER; }
}

<INGLOBALS> {
    {IDENTIFIER}   { return LeshTypes.IDENTIFIER; }
      
    "ok"           |
    "pierwotne"    { return LeshTypes.IDENTIFIER_RESERVED; }
    "pole"         |
    "pozycja"      |
    "edytowalne"   { return LeshTypes.IDENTIFIER_RESERVED; }
      
    "]"            { yybegin (YYINITIAL); return LeshTypes.DIRECTIVE_END; }
    {WHITE_SPACE}+ { return LeshTypes.DIRECTIVE_WS; }
    {CRLF}         { yypushback (1); yybegin (YYINITIAL); }
    .              { return TokenType.BAD_CHARACTER; }
}

<INSTRING> {
    \n             { yybegin(YYINITIAL); return TokenType.BAD_CHARACTER; }
    "'"            { yybegin(YYINITIAL); return LeshTypes.STRING; }
    .              { return LeshTypes.STRING; }
}
