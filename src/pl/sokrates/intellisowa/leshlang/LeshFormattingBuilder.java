package pl.sokrates.intellisowa.leshlang;

import com.intellij.formatting.*;
import com.intellij.lang.ASTNode;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.util.TextRange;
import com.intellij.psi.PsiFile;
import com.intellij.psi.TokenType;
import com.intellij.psi.codeStyle.CodeStyleSettings;
import com.intellij.psi.formatter.common.AbstractBlock;
import com.intellij.psi.tree.IElementType;
import com.intellij.psi.tree.TokenSet;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import pl.sokrates.intellisowa.leshlang.psi.LeshTypes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LeshFormattingBuilder implements FormattingModelBuilder
{
    private static final Logger LOG = Logger.getInstance ("#pl.sokrates.intellisowa.leshlang.LeshFormattingBuilder");
    
    @Override
    public @NotNull FormattingModel createModel (@NotNull FormattingContext formattingContext)
    {
        var element = formattingContext.getPsiElement();
        PsiFile containingFile = element.getContainingFile ().getViewProvider ().getPsi (LeshLanguage.INSTANCE);
    
        return FormattingModelProvider
                   .createFormattingModelForPsiFile (
                       containingFile,
                       new LeshBlock (element.getNode (), formattingContext.getCodeStyleSettings()),
                       formattingContext.getCodeStyleSettings());
    }
    
    public TextRange getRangeAffectingIndent (PsiFile file, int offset, ASTNode elementAtOffset)
    {
        return null;
    }
    
    protected static class LeshBlock extends AbstractBlock
    {
        protected final CodeStyleSettings settings;
        
        public LeshBlock (ASTNode node, CodeStyleSettings settings)
        {
            super (node, null, null);
            this.settings = settings;
        }
        
        protected static final TokenSet WHITE_SPACES = TokenSet.create (TokenType.WHITE_SPACE,
                                                                        LeshTypes.DIRECTIVE_WS);
        
        @Override
        protected List<Block> buildChildren ()
        {
            if (isLeaf ()) return Collections.emptyList ();
            
            ArrayList<Block> children = new ArrayList<> ();
            for (ASTNode childNode : myNode.getChildren (null)) {
                if (WHITE_SPACES.contains (childNode.getElementType ()) || childNode.getTextLength () == 0) continue;
                
                children.add (new LeshBlock (childNode, settings));
            }
            
            return children;
        }
        
        @Override
        public boolean isLeaf ()
        {
            return myNode.getFirstChildNode () == null;
        }

        static final TokenSet INDENT_PARENTS = TokenSet.create (LeshTypes.INSIDE_BLOCK,
                                                                LeshTypes.THEN_BLOCK,
                                                                LeshTypes.ELSE_BLOCK,
                                                                LeshTypes.ELIF_BLOCK,
                                                                LeshTypes.WHILE_BLOCK);
        
        static final TokenSet EXTRA_INDENT_PARENTS = TokenSet.create (LeshTypes.TOP_BLOCK,
                                                                      LeshTypes.PROCEDURE_BLOCK,
                                                                      LeshTypes.FUNCTION_BLOCK,
                                                                      LeshTypes.STATEMENT_IF,
                                                                      LeshTypes.STATEMENT_WHILE);
        
        static final TokenSet NEVER_INDENT = TokenSet.create (LeshTypes.KW_END,
                                                              LeshTypes.KW_ENDIF,
                                                              LeshTypes.KW_ENDDO,
                                                              LeshTypes.COMMENT,
                                                              LeshTypes.COMMENT_END);
        
        static final TokenSet CONTINUATION_PARENTS = TokenSet.create (LeshTypes.ASSIGNMENT,
                                                                      LeshTypes.EXPRESSION);
        
        @Override
        public Indent getIndent ()
        {
            ASTNode parent = myNode.getTreeParent ();
            if (parent == null)
                return Indent.getAbsoluteNoneIndent ();

            IElementType type = myNode.getElementType ();
            if (NEVER_INDENT.contains (type))
                return Indent.getNoneIndent ();
            
            IElementType ptype = parent.getElementType ();
            if (INDENT_PARENTS.contains (ptype))
                return Indent.getNormalIndent ();
            
            if (type == LeshTypes.COMMENT_START && EXTRA_INDENT_PARENTS.contains (ptype))
                return Indent.getNormalIndent ();
            
            if (CONTINUATION_PARENTS.contains (ptype))
                return Indent.getContinuationWithoutFirstIndent ();
            
            return Indent.getNoneIndent ();
        }
        
        @Nullable
        @Override
        protected Indent getChildIndent ()
        {
//            System.out.println ("getChildIndent of: " + myNode);
            
            if (myNode.getTreeParent () == null)
                return Indent.getAbsoluteNoneIndent ();
            
            IElementType type = myNode.getElementType ();
            if (INDENT_PARENTS.contains (type) || EXTRA_INDENT_PARENTS.contains (type))
                return Indent.getNormalIndent ();
            
            if (CONTINUATION_PARENTS.contains (type))
                return Indent.getContinuationWithoutFirstIndent ();
            
            ASTNode prev = myNode.getTreePrev ();
            if (prev != null) {
//                System.out.println ("getChildIndent of: " + myNode + ", prev=" + prev);
                IElementType ptype = prev.getElementType ();
                if (ptype == LeshTypes.KW_BEGIN)
                    return Indent.getNormalIndent ();
            }
            
            return Indent.getNoneIndent ();
        }
        
        @Nullable
        @Override
        public Spacing getSpacing (@Nullable Block block, @NotNull Block block1)
        {
            return null;
        }
    }
}
