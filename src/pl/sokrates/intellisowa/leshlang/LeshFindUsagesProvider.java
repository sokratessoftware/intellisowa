package pl.sokrates.intellisowa.leshlang;

import com.intellij.lang.cacheBuilder.DefaultWordsScanner;
import com.intellij.lang.cacheBuilder.WordsScanner;
import com.intellij.lang.findUsages.FindUsagesProvider;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiNamedElement;
import com.intellij.psi.tree.TokenSet;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import pl.sokrates.intellisowa.leshlang.psi.*;

import java.util.stream.Collectors;

public class LeshFindUsagesProvider implements FindUsagesProvider
{
    @Nullable
    @Override
    public WordsScanner getWordsScanner ()
    {
        return new DefaultWordsScanner (new LeshLexerAdapter (),
                                        TokenSet.create (LeshTypes.IDENTIFIER,
                                                         LeshTypes.IDENTIFIER_RESERVED),
                                        TokenSet.create (LeshTypes.COMMENT),
                                        TokenSet.create (LeshTypes.STRING));
    }

    @Override
    public boolean canFindUsagesFor (@NotNull PsiElement psiElement)
    {
        return psiElement instanceof PsiNamedElement;
    }

    @Nullable
    @Override
    public String getHelpId (@NotNull PsiElement psiElement)
    {
        return null;
    }

    @NotNull
    @Override
    public String getType (@NotNull PsiElement element)
    {
        if (element instanceof LeshProcedureName) {
            return "procedure";
        }
        else if (element instanceof LeshFunctionName) {
            return "function";
        }
        else if (element instanceof LeshVariableAnchor) {
            return "global variable";
        }
        else if (element instanceof LeshLetName) {
            return "local variable";
        }
        else if (element instanceof LeshLoopVariable) {
            return "loop-local variable";
        }
        else {
            return "";
        }
    }

    @NotNull
    @Override
    public String getDescriptiveName (@NotNull PsiElement element)
    {
        return element.getText ();
    }

    @NotNull
    @Override
    public String getNodeText (@NotNull PsiElement element, boolean useFullName)
    {
        if (element instanceof LeshSubroutineName) {
            var name = (LeshSubroutineName) element;
            var procedure = name.getSubroutine();
            var kind = procedure instanceof LeshProcedure ? "procedure " : "function ";
            return kind + element.getText() + "-(" +
               procedure.getFormalArgumentList().stream().map(param -> param.getText()).collect(Collectors.joining(", ")) + ") ";
        }
        return element.getText ();
    }
}