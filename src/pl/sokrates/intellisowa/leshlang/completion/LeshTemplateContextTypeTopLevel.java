package pl.sokrates.intellisowa.leshlang.completion;

import com.intellij.codeInsight.template.TemplateActionContext;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiErrorElement;
import com.intellij.psi.TokenType;
import com.intellij.psi.tree.IElementType;
import org.jetbrains.annotations.NotNull;
import pl.sokrates.intellisowa.leshlang.psi.*;

import static com.intellij.lang.parser.GeneratedParserUtilBase.DUMMY_BLOCK;

public class LeshTemplateContextTypeTopLevel extends LeshTemplateContextType
{
    public LeshTemplateContextTypeTopLevel () { super (LeshTemplateContextType.class, "TopLevel", "Poziom pliku"); }
    
    @Override
    public boolean isInContext (@NotNull TemplateActionContext templateActionContext)
    {
        if (super.isInContext(templateActionContext)) {
            var psiFile = templateActionContext.getFile();
            var i = templateActionContext.getStartOffset();

            PsiElement at = psiFile.getOriginalFile ().findElementAt (i);
            if (at == null) return true;
    
            PsiElement par = at.getParent ();
            if (par == null) return true;
    
            if (par.getNode ().getElementType () == DUMMY_BLOCK) {
                at = psiFile.findElementAt (i);
                if (at == null) return true;
                par = at.getParent ();
                if (par == null) return true;
            }
            
            ASTNode node = at.getNode ();
            IElementType type = node.getElementType ();
            
            if (type == LeshTypes.KW_PROCEDURE) {
                // sprawdzamy czy ta procedura nie ma już nazwy, wtedy nie dopełniamy
                node = node.getTreeNext ();
                while (node != null) {
                    if (node.getElementType () != TokenType.WHITE_SPACE
                            && node.getElementType () != TokenType.ERROR_ELEMENT)
                        return false;
        
                    if (node.getText ().contains ("\n")) break;
        
                    node = node.getTreeNext ();
                }
                
                // sprawdzamy czy ta procedura nie ma już ciała, wtedy nie dopełniamy
                while (node != null) {
                    if (node.getElementType () == LeshTypes.PROCEDURE_BLOCK) return false;
        
                    node = node.getTreeNext ();
                }
            }
            else if (type == LeshTypes.KW_BEGIN) {
                // sprawdzamy czy jest END
                while (node != null) {
                    if (node.getElementType () == LeshTypes.KW_END) return false;
        
                    node = node.getTreeNext ();
                }
                
                if (par instanceof LeshTopBlock)
                    return true;
            }

//            System.out.println ("TL " + at + " /" + par);
//            PsiElement p = at.getParent ();
//            String pfx = "  ";
//            while (p != null) {
//                System.out.println (pfx + p);
//                pfx += "  ";
//                p = p.getParent ();
//            }
        
            // w środku procedure begin .. end - nie
            if (par instanceof LeshProcedureBlock) return false;
            
            if (par instanceof LeshFile || par instanceof LeshProcedure) return true;
            
            // gdziekolwiek gdzie parser myśli, że możemy wpisać "procedure"
            if (par instanceof PsiErrorElement) {
                String msg = ((PsiErrorElement) par).getErrorDescription ();
                if (msg.contains ("KW_PROCEDURE"))
                    return true;
            }
            
        }
        return false;
    }
}
