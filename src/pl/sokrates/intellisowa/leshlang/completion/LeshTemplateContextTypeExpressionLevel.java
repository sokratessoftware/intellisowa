package pl.sokrates.intellisowa.leshlang.completion;

import com.intellij.codeInsight.template.TemplateActionContext;
import com.intellij.psi.PsiElement;
import com.intellij.psi.util.PsiTreeUtil;
import org.jetbrains.annotations.NotNull;
import pl.sokrates.intellisowa.leshlang.psi.LeshExpression;

public class LeshTemplateContextTypeExpressionLevel extends LeshTemplateContextType
{
    public LeshTemplateContextTypeExpressionLevel ()
    {
        super (LeshTemplateContextType.class,
               "ExpressionLevel",
               "Wewnątrz wyrażenia");
    }
    
    @Override
    public boolean isInContext (@NotNull TemplateActionContext templateActionContext)
    {
        if (super.isInContext(templateActionContext)) {
            var psiFile = templateActionContext.getFile();
            var i = templateActionContext.getStartOffset();
            
            PsiElement at = psiFile.findElementAt (i);
            if (at == null) return false;
            if (PsiTreeUtil.getParentOfType (at, LeshExpression.class) != null)
                return true;
        }
        return false;
    }
}
