package pl.sokrates.intellisowa.leshlang.completion;

import com.intellij.codeInsight.completion.CompletionParameters;
import com.intellij.codeInsight.completion.CompletionProvider;
import com.intellij.codeInsight.completion.CompletionResultSet;
import com.intellij.codeInsight.lookup.LookupElementBuilder;
import com.intellij.icons.AllIcons;
import com.intellij.psi.PsiElement;
import com.intellij.util.ProcessingContext;
import com.intellij.util.indexing.IdFilter;
import com.intellij.util.indexing.StorageException;
import org.jetbrains.annotations.NotNull;
import pl.sokrates.intellisowa.common.IncludeUtil;
import pl.sokrates.intellisowa.leshlang.LeshUtil;
import pl.sokrates.intellisowa.leshlang.index.IndexUtil;
import pl.sokrates.intellisowa.leshlang.psi.LeshFile;

import java.util.BitSet;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class VariableCompletionProvider extends CompletionProvider<CompletionParameters>
{
    final boolean myOnlyWritable;
    final boolean myMakeAssignment;
    
    static final Set<String> builtins = new HashSet<String> ();
    static final Set<String> rwBuiltins = new HashSet<String> ();
    
    static {
        builtins.add ("ok");
        builtins.add ("pierwotne");
        
        builtins.add ("rekord");
        builtins.add ("pole");
        builtins.add ("pozycja");
        builtins.add ("edytowalne");
        builtins.add ("header");
        builtins.add ("lp");
        builtins.add ("__ilimit__");
        builtins.add ("__slimit__");
        
        rwBuiltins.add ("rekord");
        rwBuiltins.add ("pole");
        rwBuiltins.add ("pozycja");
        rwBuiltins.add ("edytowalne");
        rwBuiltins.add ("header");
        rwBuiltins.add ("lp");
        rwBuiltins.add ("__ilimit__");
        rwBuiltins.add ("__slimit__");
    }
    
    public VariableCompletionProvider (boolean onlyWritable, boolean makeAssignment)
    {
        super ();
        
        myOnlyWritable = onlyWritable;
        myMakeAssignment = makeAssignment;
    }
    
    @Override
    protected void addCompletions (@NotNull CompletionParameters completionParameters,
                                   ProcessingContext processingContext,
                                   @NotNull CompletionResultSet completionResultSet)
    {
        final PsiElement myElement = completionParameters.getPosition ();
        final LeshFile file = (LeshFile) myElement.getContainingFile ().getOriginalFile ();
        
        final BitSet visitedFiles = new BitSet ();
        
        final Collection<String> results;
    
        var locals = LeshUtil.findLocalVariables(myElement);
        for (String result : locals) {
            completionResultSet.consume (LookupElementBuilder.create (myMakeAssignment ? result + " = " : result)
                                                             .withIcon (AllIcons.Nodes.Variable)
                                                             .withItemTextUnderlined(true));
        }
        
        IncludeUtil.visitIncludesById (file, visitedFiles);
        
        try {
            results = IndexUtil.allVariableNamesInFiles (myElement.getProject (), new IdFilter ()
            {
                @Override
                public boolean containsFileId (int i)
                {
                    return i >= 0 && visitedFiles.get (i);
                }
            });
        } catch (StorageException ex) {
            System.err.println ("allVariableNamesInFiles() crash");
            return;
        }
        
        for (String result : results) {
            if (!builtins.contains (result))
                completionResultSet.consume (LookupElementBuilder.create (myMakeAssignment ? result + " = " : result)
                                                                 .withIcon (AllIcons.Nodes.Gvariable));
        }
        
        if (!myOnlyWritable) {
            completionResultSet.consume (LookupElementBuilder.create ("ok")
                                                             .withBoldness (true)
                                                             .withIcon (AllIcons.Nodes.Constant));
            completionResultSet.consume (LookupElementBuilder.create ("pierwotne").withBoldness (true).withIcon (
                AllIcons.Nodes.Constant));
        }
        
        if (myMakeAssignment) {
            for (String name : rwBuiltins) {
                completionResultSet.consume (LookupElementBuilder.create (name + " = ")
                                                                 .withPresentableText (name)
                                                                 .withBoldness (true)
                                                                 .withTailText (" =")
                                                                 .withIcon (AllIcons.Nodes.Gvariable));
            }
        } else {
            for (String name : rwBuiltins) {
                completionResultSet.consume (LookupElementBuilder.create (name)
                                                                 .withBoldness (true)
                                                                 .withIcon (AllIcons.Nodes.Gvariable));
            }
        }
    }
}
