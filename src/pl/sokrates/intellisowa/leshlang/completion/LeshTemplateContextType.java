package pl.sokrates.intellisowa.leshlang.completion;

import com.intellij.codeInsight.template.TemplateActionContext;
import com.intellij.codeInsight.template.TemplateContextType;
import org.jetbrains.annotations.NotNull;
import pl.sokrates.intellisowa.leshlang.psi.LeshFile;

/**
 * Kontekst pozwalający skojarzyć szablony z LeshLangiem
 */
public class LeshTemplateContextType extends TemplateContextType
{
    public LeshTemplateContextType ()
    {
        super ("pl.sokrates.intellisowa.leshlang.completion.LeshTemplateContextType",
               "LeshLang");
    }
    
    protected LeshTemplateContextType (Class<? extends LeshTemplateContextType> parent,
                                       String subName,
                                       String subDesc)
    {
        super ("pl.sokrates.intellisowa.leshlang.completion.LeshTemplateContextType." + subName, subDesc, parent);
    }
    
    @Override
    public boolean isInContext (@NotNull TemplateActionContext templateActionContext)
    {
        return templateActionContext.getFile() instanceof LeshFile;
    }
}
