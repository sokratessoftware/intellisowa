package pl.sokrates.intellisowa.leshlang.completion;

import com.intellij.codeInsight.completion.CompletionParameters;
import com.intellij.codeInsight.completion.CompletionProvider;
import com.intellij.codeInsight.completion.CompletionResultSet;
import com.intellij.codeInsight.completion.util.ParenthesesInsertHandler;
import com.intellij.codeInsight.lookup.LookupElementBuilder;
import com.intellij.icons.AllIcons;
import com.intellij.psi.PsiElement;
import com.intellij.util.ProcessingContext;
import org.jetbrains.annotations.NotNull;
import pl.sokrates.intellisowa.leshlang.LeshBuiltins;
import pl.sokrates.intellisowa.leshlang.psi.LeshFile;
import pl.sokrates.intellisowa.leshlang.psi.LeshFunction;
import pl.sokrates.intellisowa.leshlang.psi.LeshProcedure;
import pl.sokrates.intellisowa.leshlang.psi.LeshSubroutine;
import pl.sokrates.intellisowa.leshlang.psi.refs.LeshSubroutineRef;

import java.util.HashSet;
import java.util.stream.Collectors;

public class SubroutineCompletionProvider extends CompletionProvider<CompletionParameters>
{
    private final boolean wantProcedures;
    private final boolean wantFunctions;
    
    public SubroutineCompletionProvider (boolean wantProcedures, boolean wantFunctions)
    {
        super ();
        this.wantProcedures = wantProcedures;
        this.wantFunctions = wantFunctions;
    }

    @Override
    protected void addCompletions (@NotNull CompletionParameters completionParameters,
                                   ProcessingContext processingContext,
                                   @NotNull final CompletionResultSet completionResultSet)
    {
        final PsiElement myElement = completionParameters.getPosition ();
        final LeshFile file = (LeshFile) myElement.getContainingFile().getOriginalFile();
        
        var dedup = new HashSet<String>();
        
        if (wantProcedures) {
            for (var element : LeshBuiltins.getBuiltinProcedures(myElement.getProject()).values()) {
                dedup.add(element.getSignature());
                provideProcedure(completionResultSet, element);
            }
    
            LeshSubroutineRef.collectFromFile(file, LeshProcedure.class, null, -1, myElement, null, name -> {
                var subroutine = name.getSubroutine();
                dedup.add(subroutine.getSignature());
                provideProcedure(completionResultSet, subroutine);
            });
        }
        
        if (wantFunctions) {
            for (var element : LeshBuiltins.getBuiltinFunctions(myElement.getProject()).values()) {
                var signature = element.getSignature();
                if (wantProcedures && dedup.contains(signature))
                    continue;
                provideFunction(completionResultSet, element);
            }
    
            LeshSubroutineRef.collectFromFile(file, LeshFunction.class, null, -1, myElement, null, name -> {
                var subroutine = name.getSubroutine();
                var signature = subroutine.getSignature();
                if (wantProcedures && dedup.contains(signature)) {/* continue */}
                else
                    provideFunction(completionResultSet, subroutine);
            });
        }
    }
    
    private void provideProcedure(@NotNull final CompletionResultSet completionResultSet, LeshSubroutine subroutine)
    {
        var formalArgs = subroutine.getFormalArgumentList();
        completionResultSet.consume(LookupElementBuilder.create(subroutine.getSubroutineName())
                                                        .withIcon(AllIcons.Nodes.Method)
                                                        .withTypeText(subroutine.getContainingFile().getName())
                                                        .withTailText(formalArgs.stream()
                                                                                .map(param -> param.getText())
                                                                                .collect(
                                                                                    Collectors.joining(", ",
                                                                                                       "(",
                                                                                                       ") proc")), true)
                                                        .withInsertHandler(formalArgs.isEmpty() ? null : ParenthesesInsertHandler.WITH_PARAMETERS));
    }
    
    private void provideFunction(@NotNull final CompletionResultSet completionResultSet, LeshSubroutine subroutine)
    {
        var formalArgs = subroutine.getFormalArgumentList();
        completionResultSet.consume(LookupElementBuilder.create(subroutine.getSubroutineName())
                                                        .withIcon(AllIcons.Nodes.Function)
                                                        .withTypeText(subroutine.getContainingFile().getName())
                                                        .withTailText(formalArgs.stream()
                                                                                .map(param -> param.getText())
                                                                                .collect(
                                                                                    Collectors.joining(", ",
                                                                                                       "(",
                                                                                                       ") func")), true)
                                                        .withInsertHandler(ParenthesesInsertHandler.WITH_PARAMETERS));
    }
}
