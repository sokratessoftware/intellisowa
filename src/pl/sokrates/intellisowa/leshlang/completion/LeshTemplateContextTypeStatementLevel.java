package pl.sokrates.intellisowa.leshlang.completion;

import com.intellij.codeInsight.template.TemplateActionContext;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.TokenType;
import com.intellij.psi.tree.IElementType;
import com.intellij.psi.util.PsiTreeUtil;
import org.jetbrains.annotations.NotNull;
import pl.sokrates.intellisowa.leshlang.psi.LeshExpression;
import pl.sokrates.intellisowa.leshlang.psi.LeshInsideBlock;
import pl.sokrates.intellisowa.leshlang.psi.LeshTypes;

public class LeshTemplateContextTypeStatementLevel extends LeshTemplateContextType
{
    public LeshTemplateContextTypeStatementLevel ()
    {
        super (LeshTemplateContextType.class,
               "StatementLevel",
               "Wewnątrz skryptu lub procedury");
    }
    
    @Override
    public boolean isInContext (@NotNull TemplateActionContext templateActionContext)
    {
        if (super.isInContext (templateActionContext)) {
            var psiFile = templateActionContext.getFile();
            var i = templateActionContext.getStartOffset();
            
            PsiElement at = psiFile.getOriginalFile ().findElementAt (i);
            if (at == null) return false;
    
            ASTNode node = at.getNode ();
            IElementType type = node.getElementType ();
            if (type == LeshTypes.KW_IF || type == LeshTypes.KW_WHILE) {
                // sprawdzamy czy ten if/while nie ma już warunku, wtedy nie dopełniamy
                node = node.getTreeNext ();
                while (node != null) {
                    if (node.getElementType () != TokenType.WHITE_SPACE
                            && node.getElementType () != TokenType.ERROR_ELEMENT)
                        return false;
                    
                    if (node.getText ().contains ("\n")) break;
                    
                    node = node.getTreeNext ();
                }
                
                // sprawdzamy czy ten if/while nie ma już endifa, wtedy nie dopełniamy
                IElementType end = (type == LeshTypes.KW_WHILE) ? LeshTypes.KW_ENDDO : LeshTypes.KW_ENDIF;
                while (node != null) {
                    if (node.getElementType () == end) return false;
                    
                    node = node.getTreeNext ();
                }
            }
            
            if ((PsiTreeUtil.getNonStrictParentOfType (at, LeshInsideBlock.class) != null) &&
                    PsiTreeUtil.getNonStrictParentOfType (at, LeshExpression.class) == null)
                return true;
        }
        return false;
    }
}
