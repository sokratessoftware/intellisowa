package pl.sokrates.intellisowa.leshlang.completion;

import com.intellij.codeInsight.completion.CompletionContributor;
import com.intellij.codeInsight.completion.CompletionType;
import com.intellij.psi.TokenType;
import pl.sokrates.intellisowa.common.completion.ErrorCompletionProvider;
import pl.sokrates.intellisowa.common.completion.IncludeCompletionProvider;
import pl.sokrates.intellisowa.common.completion.KeywordCompletionProvider;
import pl.sokrates.intellisowa.leshlang.LeshFileType;
import pl.sokrates.intellisowa.leshlang.psi.*;

import static com.intellij.patterns.PlatformPatterns.psiElement;


public class LeshCompletionContributor extends CompletionContributor
{
    public LeshCompletionContributor ()
    {
        // nazwy procedur, nazwy zmiennych, nazwy zapisywalnych zmiennych specjalnych (dodajemy =), słowa kluczowe
        extend (CompletionType.BASIC,
                psiElement ().withParent (LeshProcedureCall.class),
                new SubroutineCompletionProvider(true, false));

        extend (CompletionType.BASIC,
                psiElement ().withParent (LeshProcedureCall.class),
                new VariableCompletionProvider (true, true));

        // szablony
//        extend (CompletionType.BASIC,
//                psiElement ().withParent (psiElement (LeshProcedureCall.class).withParent (LeshFile.class)),
//                new KeywordCompletionProvider (true, "procedure"));
//
//        extend (CompletionType.BASIC,
//                psiElement ().withParent (psiElement (LeshProcedureCall.class).withParent (LeshFile.class)),
//                new KeywordCompletionProvider (false, "begin"));

        extend (CompletionType.BASIC,
                psiElement ().withParent (psiElement (LeshProcedureCall.class).withParent (LeshTopBlock.class)),
                new KeywordCompletionProvider (false, "end"));
    
        extend (CompletionType.BASIC,
                psiElement ().withParent (psiElement (LeshProcedureCall.class).withParent (LeshInsideBlock.class)),
                new KeywordCompletionProvider (false, "end"));
        
        extend (CompletionType.BASIC,
                psiElement ().withParent (psiElement (LeshProcedureCall.class).withParent (LeshProcedureBlock.class)),
                new KeywordCompletionProvider (false, "end"));
        
        extend (CompletionType.BASIC,
                psiElement ().withParent (psiElement (LeshProcedureCall.class).withParent (LeshThenBlock.class)),
                new KeywordCompletionProvider (false, "endif", "else"));

        extend (CompletionType.BASIC,
                psiElement ().withParent (psiElement (LeshProcedureCall.class).withParent (LeshElseBlock.class)),
                new KeywordCompletionProvider (false, "endif"));

        extend (CompletionType.BASIC,
                psiElement ().withParent (psiElement (LeshProcedureCall.class).withParent (LeshWhileBlock.class)),
                new KeywordCompletionProvider (false, "enddo"));

        // nazwy zmiennych i nazwy zapisywalnych zmiennych specjalnych
        extend (CompletionType.BASIC,
                psiElement ().withParent (psiElement (LeshVariable.class).withParent (LeshAssignment.class)),
                new VariableCompletionProvider (true, false));

        // nazwy zmiennych, zmiennych specjalnych i funkcji (dodajemy nawiasy)
        extend (CompletionType.BASIC,
                psiElement ().withParent (psiElement (LeshVariable.class).withParent (LeshTerm.class)),
                new VariableCompletionProvider (false, false));

        extend (CompletionType.BASIC,
                psiElement ().withParent (psiElement (LeshVariable.class).withParent (LeshTerm.class)),
                new SubroutineCompletionProvider(false, true));

        // nazwy funkcji
        extend (CompletionType.BASIC,
                psiElement ().withParent (LeshFunctionCall.class),
                new SubroutineCompletionProvider(false, true));
        
        extend (CompletionType.BASIC,
                psiElement().afterLeaf(psiElement(LeshTypes.OP_PASS)),
                new SubroutineCompletionProvider(false, true));
        
        // słowa kluczowe
        extend (CompletionType.BASIC,
                psiElement (TokenType.WHITE_SPACE).afterLeaf (psiElement (LeshTypes.IDENTIFIER).withParent (
                    LeshProcedureName.class)),
                new KeywordCompletionProvider (false, "begin"));

        extend (CompletionType.BASIC,
                psiElement (),
                // nie dopełniamy słów, które mamy w szablonach
                // bo nie znaleźliśmy sposoby na przesortowanie szablonów powyżej samych słów
                new ErrorCompletionProvider (LeshTokenType.class.getSimpleName (), "procedure", "function", "if", "while", "begin"));

        // ścieżki
        extend (CompletionType.BASIC,
                psiElement (LeshTypes.DIRECTIVE_PATH).withParent (LeshIncludePath.class),
                new IncludeCompletionProvider (LeshFileType.INSTANCE));

        //extend (CompletionType.BASIC, psiElement (), new PrintCompletionProvider ());
    }
}