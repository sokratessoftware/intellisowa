package pl.sokrates.intellisowa.leshlang;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.Key;
import com.intellij.psi.PsiFileFactory;
import pl.sokrates.intellisowa.common.SowaFileType;
import pl.sokrates.intellisowa.leshlang.psi.LeshFile;
import pl.sokrates.intellisowa.leshlang.psi.LeshFunction;
import pl.sokrates.intellisowa.leshlang.psi.LeshProcedure;
import pl.sokrates.intellisowa.leshlang.psi.LeshSubroutine;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class LeshBuiltins
{
    private static final Key<LeshFile> fileKey = new Key<>("pl.sokrates.intellisowa.leshlang.BUILTINS_FILE");
    private static final Key<Map<String, LeshProcedure>> proceduresKey = new Key<>("pl.sokrates.intellisowa.leshlang.BUILTIN_PROCEDURES");
    private static final Key<Map<String, LeshFunction>> functionsKey = new Key<>("pl.sokrates.intellisowa.leshlang.BUILTIN_FUNCTIONS");
    
    public static LeshFile getBuiltinsFile (Project project) {
        var file = project.getUserData(fileKey);
        if (file == null) {
            var rdr = new InputStreamReader(LeshLanguage.INSTANCE.getClass().getClassLoader().getResourceAsStream("builtins.ll"),
                                            SowaFileType.GLORIOUS_CHARSET);
            var buf = new BufferedReader(rdr);
            var txt = buf.lines().collect(Collectors.joining("\n"));
            file = (LeshFile) PsiFileFactory.getInstance(project).createFileFromText("builtins.ll", LeshLanguage.INSTANCE, txt);
            final var vFile = file.getVirtualFile();
            try {
                vFile.setCharset(SowaFileType.GLORIOUS_CHARSET);
                vFile.setWritable(false);
            }
            catch (IOException e) {}
            project.putUserData(fileKey, file);
            
//            java.awt.EventQueue.invokeLater(() -> {
//                FileEditorManager.getInstance(project).openFile(vFile, false);
//            });
        }
        return file;
    }
    
    public static Map<String, LeshProcedure> getBuiltinProcedures(Project project) {
        var coll = project.getUserData(proceduresKey);
        if (coll == null) {
            var arr = getBuiltinsFile(project).findChildrenByClass(LeshProcedure.class);
            var map = new TreeMap<String, LeshProcedure>();
            for (var x : arr) if (x.getSignature() != null) map.put(x.getSignature(), x);
            coll = Collections.unmodifiableSortedMap(map);
            project.putUserData(proceduresKey, coll);
        }
        return coll;
    }
    
    public static Map<String, LeshFunction> getBuiltinFunctions(Project project) {
        var coll = project.getUserData(functionsKey);
        if (coll == null) {
            var arr = getBuiltinsFile(project).findChildrenByClass(LeshFunction.class);
            var map = new TreeMap<String, LeshFunction>();
            for (var x : arr) if (x.getSignature() != null) map.put(x.getSignature(), x);
            coll = Collections.unmodifiableSortedMap(map);
            project.putUserData(functionsKey, coll);
        }
        return coll;
    }
    
    public static void collectBuiltinSubroutines(Project project, Class<? extends LeshSubroutine> ofClass, String name, Collection<? super LeshSubroutine> into) {
        var procedures = getBuiltinProcedures(project);
        var functions = getBuiltinFunctions(project);
        var wantProcedures = ofClass.isAssignableFrom(LeshProcedure.class);
        var wantFunctions = ofClass.isAssignableFrom(LeshFunction.class);
        for (int i = 0; i < 6; i++) {
            if (wantFunctions) {
                var x = functions.get(name + "/" + i);
                if (x != null)
                    into.add(x);
            }
            if (wantProcedures) {
                var x = procedures.get(name + "/" + i);
                if (x != null)
                    into.add(x);
            }
        }
    }
}
