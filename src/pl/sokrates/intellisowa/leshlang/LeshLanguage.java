package pl.sokrates.intellisowa.leshlang;

import com.intellij.lang.Language;
import org.jetbrains.annotations.NotNull;

public class LeshLanguage extends Language
{
    public static final LeshLanguage INSTANCE = new LeshLanguage ();

    private LeshLanguage ()
    {
        super ("LeshLang");
    }

    @NotNull
    @Override
    public String getDisplayName ()
    {
        return "SOWA: LeshLang";
    }

    @Override
    public boolean isCaseSensitive ()
    {
        return false;
    }
}
