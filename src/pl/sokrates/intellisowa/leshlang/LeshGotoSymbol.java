package pl.sokrates.intellisowa.leshlang;

import com.intellij.navigation.ChooseByNameContributor;
import com.intellij.navigation.NavigationItem;
import com.intellij.openapi.project.Project;
import org.jetbrains.annotations.NotNull;
import pl.sokrates.intellisowa.leshlang.index.IndexUtil;

import java.util.Collection;

public class LeshGotoSymbol implements ChooseByNameContributor
{
    @NotNull
    @Override
    public String[] getNames (Project project, boolean b)
    {
        Collection<String> all = IndexUtil.allSubroutineNames(project);
        return all.toArray (new String[all.size ()]);
    }

    @NotNull
    @Override
    public NavigationItem[] getItemsByName (String name, String pattern, Project project, boolean b)
    {
        var list = IndexUtil.findSubroutinesInProject(project, name, true, true);
        return list.toArray (new NavigationItem[list.size ()]);
    }
}