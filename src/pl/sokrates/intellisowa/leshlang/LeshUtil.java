package pl.sokrates.intellisowa.leshlang;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiManager;
import com.intellij.psi.search.FileTypeIndex;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.psi.util.PsiTreeUtil;
import com.intellij.util.indexing.FileBasedIndex;
import org.jetbrains.annotations.NotNull;
import pl.sokrates.intellisowa.leshlang.psi.*;

import java.util.*;
import java.util.stream.Stream;

public class LeshUtil
{
    public static List<LeshProcedureName> findProcedureDeclarations (Project project, String key)
    {
        List<LeshProcedureName> result = null;
        Collection<VirtualFile> virtualFiles =
            FileBasedIndex.getInstance ().getContainingFiles (FileTypeIndex.NAME, LeshFileType.INSTANCE,
                                                              GlobalSearchScope.allScope (project));
        for (VirtualFile virtualFile : virtualFiles) {
            LeshFile leshFile = (LeshFile) PsiManager.getInstance (project).findFile (virtualFile);
            if (leshFile != null) {
                LeshProcedure[] procedures = PsiTreeUtil.getChildrenOfType (leshFile, LeshProcedure.class);
                if (procedures != null) {
                    for (LeshProcedure procedure : procedures) {
                        LeshProcedureName name = procedure.getProcedureName ();
                        if (key.equals (name.getText ())) {
                            if (result == null) {
                                result = new ArrayList<> ();
                            }
                            result.add (name);
                        }
                    }
                }
            }
        }
        return result != null ? result : Collections.<LeshProcedureName>emptyList ();
    }

    public static List<LeshProcedureName> findProcedureDeclarations (Project project)
    {
        List<LeshProcedureName> result = new ArrayList<> ();
        Collection<VirtualFile> virtualFiles =
            FileBasedIndex.getInstance ().getContainingFiles (FileTypeIndex.NAME, LeshFileType.INSTANCE,
                                                              GlobalSearchScope.allScope (project));
        for (VirtualFile virtualFile : virtualFiles) {
            LeshFile leshFile = (LeshFile) PsiManager.getInstance (project).findFile (virtualFile);
            if (leshFile != null) {
                LeshProcedure[] procedures = PsiTreeUtil.getChildrenOfType (leshFile, LeshProcedure.class);
                if (procedures != null) {
                    for (LeshProcedure procedure : procedures)
                        result.add (procedure.getProcedureName ());
                }
            }
        }
        return result;
    }

    public static List<LeshAssignment> findAssignments (Project project, String key)
    {
        List<LeshAssignment> result = null;
        Collection<VirtualFile> virtualFiles =
            FileBasedIndex.getInstance ().getContainingFiles (FileTypeIndex.NAME, LeshFileType.INSTANCE,
                                                              GlobalSearchScope.allScope (project));
        for (VirtualFile virtualFile : virtualFiles) {
            LeshFile leshFile = (LeshFile) PsiManager.getInstance (project).findFile (virtualFile);
            if (leshFile != null) {
                Collection<LeshAssignment> assignments = PsiTreeUtil.collectElementsOfType (leshFile,
                                                                                            LeshAssignment.class);
                for (LeshAssignment assignment : assignments) {
                    LeshVariable name = assignment.getVariable ();
                    if (key.equals (name.getText ())) {
                        if (result == null) {
                            result = new ArrayList<> ();
                        }
                        result.add (assignment);
                        break;
                    }
                }
            }
        }
        return result != null ? result : Collections.<LeshAssignment>emptyList ();
    }

    public static List<LeshAssignment> findAssignments (Project project)
    {
        List<LeshAssignment> result = new ArrayList<> ();
        Collection<VirtualFile> virtualFiles =
            FileBasedIndex.getInstance ().getContainingFiles (FileTypeIndex.NAME, LeshFileType.INSTANCE,
                                                              GlobalSearchScope.allScope (project));
        for (VirtualFile virtualFile : virtualFiles) {
            LeshFile leshFile = (LeshFile) PsiManager.getInstance (project).findFile (virtualFile);
            if (leshFile != null) {
                Collection<LeshAssignment> assignments = PsiTreeUtil.collectElementsOfType (leshFile,
                                                                                            LeshAssignment.class);
                for (LeshAssignment assignment : assignments)
                    result.add (assignment);
                break;
            }
        }
        return result;
    }
    
    public static Set<String> findLocalVariables(PsiElement psi) {
        // szukamy pasującej deklaracji zmiennej lokalnej wyżej w drzewie
        var results = new HashSet<String>();
        while (psi != null) {
            if (psi instanceof LeshStatementLevel) {
                if (psi instanceof LeshStatementWhile) {
                    results.add("__ilimit__");
                    results.add("__i__");
                }
                
                var sibling = psi;
                while (sibling != null) {
                    sibling = sibling.getPrevSibling();
                    if (sibling instanceof LeshLetBinding) {
                        var let = ((LeshLetBinding) sibling).getLetName();
                        results.add(let.getName());
                    }
                }
            }
            else if (psi instanceof LeshSubroutine) {
                var procedure = (LeshSubroutine) psi;
                for (var param : procedure.getFormalArgumentList()) {
                    results.add(param.getName());
                }
            }
            
            psi = psi.getParent();
        }
        return results;
    }
    
    public static Stream<PsiElement> findChildrenInCurrentSection(@NotNull PsiElement in, PsiElement at) {
        if (at != null) {
            while (at != null) {
                var prev = at;
                at = at.getParent();
                if (at instanceof LeshFile) {
                    at = prev;
                    break;
                }
            }
        }
        
        var result = Arrays.stream(in.getChildren());
        
        if (at == null)
            return result;
        
        var sectionStart = at;
        while (sectionStart != null & !(sectionStart instanceof LeshSection))
            sectionStart = sectionStart.getPrevSibling();
        
        if (sectionStart != null) {
            final var from = sectionStart;
            result = result.dropWhile(x -> x != from);
        }
    
        var sectionEnd = at;
        do {
            sectionEnd = sectionEnd.getNextSibling();
        } while (sectionEnd != null && !(sectionEnd instanceof LeshSection));
        
        if (sectionEnd != null) {
            final var to = sectionEnd;
            result = result.takeWhile(x -> x != to);
        }
        
        return result;
    }
}