package pl.sokrates.intellisowa.leshlang;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import pl.sokrates.intellisowa.SokratesIcons;
import pl.sokrates.intellisowa.common.SowaFileType;

import javax.swing.*;

public class LeshFileType extends SowaFileType
{
    public static final LeshFileType INSTANCE = new LeshFileType ();

    protected LeshFileType ()
    {
        super (LeshLanguage.INSTANCE);
    }

    @NotNull
    @Override
    public String getName ()
    {
        return "Skrypt w LeshLangu";
    }

    @NotNull
    @Override
    public String getDescription ()
    {
        return "Skrypt w LeshLangu";
    }

    @NotNull
    @Override
    public String getDefaultExtension ()
    {
        return "ppr";
    }

    @Nullable
    @Override
    public Icon getIcon ()
    {
        return SokratesIcons.LESH_LANG;
    }
}

