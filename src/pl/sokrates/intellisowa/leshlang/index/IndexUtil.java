package pl.sokrates.intellisowa.leshlang.index;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiManager;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.util.Processor;
import com.intellij.util.indexing.FileBasedIndex;
import com.intellij.util.indexing.IdFilter;
import com.intellij.util.indexing.StorageException;
import pl.sokrates.intellisowa.leshlang.psi.LeshFile;
import pl.sokrates.intellisowa.leshlang.psi.LeshFunction;
import pl.sokrates.intellisowa.leshlang.psi.LeshProcedure;
import pl.sokrates.intellisowa.leshlang.psi.LeshSubroutine;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class IndexUtil
{
    private static final FileBasedIndex index = FileBasedIndex.getInstance ();

    public static List<LeshSubroutine> findSubroutinesInProject(Project project, String key, boolean wantProcedures, boolean wantFunctions)
    {
        List<LeshSubroutine> result = new ArrayList<>();
        var scope = GlobalSearchScope.projectScope(project);
        var manager = PsiManager.getInstance(project);
        key = key.toLowerCase();
    
        if (wantProcedures) {
            for (VirtualFile file : index.getContainingFiles(LeshSubroutineIndexExtension.KEY, "proc:" + key, scope)) {
                LeshFile leshFile = (LeshFile) manager.findFile(file);
                if (leshFile == null) continue;
        
                for (var procedure : leshFile.findChildrenByClass(LeshProcedure.class)) {
                    var name = procedure.getSubroutineName().getName().toLowerCase();
                    if (key.equals(name)) {
                        result.add(procedure);
                    }
                }
            }
        }
        
        if (wantFunctions) {
            for (VirtualFile file : index.getContainingFiles(LeshSubroutineIndexExtension.KEY, "func:" + key, scope)) {
                LeshFile leshFile = (LeshFile) manager.findFile(file);
                if (leshFile == null) continue;
                
                for (var procedure : leshFile.findChildrenByClass(LeshFunction.class)) {
                    var name = procedure.getSubroutineName().getName().toLowerCase();
                    if (key.equals(name)) {
                        result.add(procedure);
                    }
                }
            }
        }
        
        return result;
    }
    
    public static Collection<String> allSubroutineNames (Project project)
    {
        return index.getAllKeys(LeshSubroutineIndexExtension.KEY, project)
                    .stream()
                    .map(name -> name.substring(5)).collect(Collectors.toSet());
    }

    public static Collection<String> allVariableNamesInFiles (Project project, IdFilter files) throws StorageException
    {
        final ArrayList<String> results = new ArrayList<> ();

        index.processAllKeys (LeshVariableIndexExtension.KEY, new Processor<String> ()
        {
            @Override
            public boolean process (String s)
            {
                results.add (s);
                return true;
            }
        }, GlobalSearchScope.projectScope (project), files);

        return results;
    }
    
    public static boolean isVariableInScope (GlobalSearchScope scope, String key) throws StorageException
    {
        return index.getContainingFiles (LeshVariableIndexExtension.KEY, key, scope).size () > 0;
    }
    
    public static Collection<VirtualFile> findFilesWithVariable (Project project, String key)
    {
        return index.getContainingFiles (LeshVariableIndexExtension.KEY, key, GlobalSearchScope.projectScope (project));
    }

    public static Collection<VirtualFile> findFilesWithVariable (GlobalSearchScope scope, String key)
    {
        return index.getContainingFiles (LeshVariableIndexExtension.KEY, key, scope);
    }
}
