package pl.sokrates.intellisowa.leshlang.index;

import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiFile;
import com.intellij.util.indexing.*;
import com.intellij.util.io.DataExternalizer;
import com.intellij.util.io.EnumeratorStringDescriptor;
import com.intellij.util.io.KeyDescriptor;
import gnu.trove.THashMap;
import org.jetbrains.annotations.NotNull;
import pl.sokrates.intellisowa.leshlang.LeshFileType;
import pl.sokrates.intellisowa.leshlang.psi.LeshFile;
import pl.sokrates.intellisowa.leshlang.psi.LeshProcedure;
import pl.sokrates.intellisowa.leshlang.psi.LeshSubroutine;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Map;

/**
 * Na podstawie https://github.com/goaop/idea-plugin/blob/58f4a849e6f3ec88cda6b0f7bfc2c21a488f72ad/src/com/aopphp/go/index/AnnotatedPhpNamedElementIndex.java
 */
public class LeshSubroutineIndexExtension extends FileBasedIndexExtension<String, Boolean>
{

    public static final ID<String, Boolean>   KEY           = ID.create (
        "pl.sokrates.intellisowa.index.LeshSubroutineIndexExtension.v3");
    private final       KeyDescriptor<String> keyDescriptor = new EnumeratorStringDescriptor ();

    @NotNull
    @Override
    public ID<String, Boolean> getName ()
    {
        return KEY;
    }

    @NotNull
    @Override
    public DataIndexer<String, Boolean, FileContent> getIndexer ()
    {
        return new DataIndexer<String, Boolean, FileContent> ()
        {
            @NotNull
            @Override
            public Map<String, Boolean> map (@NotNull FileContent inputData)
            {
                final Map<String, Boolean> map = new THashMap<> ();

                PsiFile psiFile = inputData.getPsiFile ();
                if (!(psiFile instanceof LeshFile)) {
                    return map;
                }

                for (var procedure : ((LeshFile) psiFile).findChildrenByClass(LeshSubroutine.class)) {
                    var name = procedure.getSubroutineName();
                    if (name != null) {
                        var pfx = procedure instanceof LeshProcedure ? "proc:" : "func:";
                        map.put(pfx + name.getName().toLowerCase(), true);
                    }
                }

                return map;
            }
        };
    }

    @NotNull
    @Override
    public KeyDescriptor<String> getKeyDescriptor ()
    {
        return this.keyDescriptor;
    }

    @NotNull
    @Override
    public DataExternalizer<Boolean> getValueExternalizer ()
    {
        return new TrueExternalizer ();
    }

    @NotNull
    @Override
    public FileBasedIndex.InputFilter getInputFilter ()
    {
        return new FileBasedIndex.InputFilter ()
        {
            public boolean acceptInput (@NotNull VirtualFile file)
            {
                return file.getFileType() == LeshFileType.INSTANCE && !file.getName().equals("builtins.ll");
            }
        };
    }

    @Override
    public boolean dependsOnFileContent ()
    {
        return true;
    }

    @Override
    public int getVersion ()
    {
        return 1;
    }

    private static class TrueExternalizer implements DataExternalizer<Boolean>
    {

        @Override
        public void save (@NotNull DataOutput dataOutput, Boolean value) throws IOException
        {
        }

        @Override
        public Boolean read (@NotNull DataInput dataInput) throws IOException
        {
            return true;
        }
    }
}

