package pl.sokrates.intellisowa.leshlang.syntax;

import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.openapi.fileTypes.SyntaxHighlighter;
import com.intellij.openapi.options.colors.AttributesDescriptor;
import com.intellij.openapi.options.colors.ColorDescriptor;
import com.intellij.openapi.options.colors.ColorSettingsPage;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import pl.sokrates.intellisowa.SokratesIcons;

import javax.swing.*;
import java.util.Collections;
import java.util.Map;

public class LeshColorSettingsPage implements ColorSettingsPage
{
    private static final AttributesDescriptor[] DESCRIPTORS = new AttributesDescriptor[] {
        new AttributesDescriptor ("Keyword", LeshSyntaxHighlighter.KEYWORD),
        new AttributesDescriptor ("Identifier", LeshSyntaxHighlighter.IDENTIFIER),
//        new AttributesDescriptor ("Variable: built-in", LeshSyntaxHighlighter.SPECIAL),
        new AttributesDescriptor ("Variable: local", LeshSyntaxHighlighter.LOCAL_VARIABLE),
        new AttributesDescriptor ("Variable: global", LeshSyntaxHighlighter.GLOBAL_VARIABLE),
        new AttributesDescriptor ("Subroutine name", LeshSyntaxHighlighter.DECLARATION),
        new AttributesDescriptor ("Subroutine call: user-defined", LeshSyntaxHighlighter.PROCEDURE),
        new AttributesDescriptor ("Subroutine call: built-in", LeshSyntaxHighlighter.BUILTIN),
        new AttributesDescriptor ("Literal: string", LeshSyntaxHighlighter.STRING),
        new AttributesDescriptor ("Literal: character", LeshSyntaxHighlighter.CHARACTER),
        new AttributesDescriptor ("Comment", LeshSyntaxHighlighter.COMMENT),
        new AttributesDescriptor ("Directive", LeshSyntaxHighlighter.DIRECTIVE),
        new AttributesDescriptor ("Symbol: operator", LeshSyntaxHighlighter.OPERATOR),
        new AttributesDescriptor ("Symbol: separator", LeshSyntaxHighlighter.SEPARATOR),
        };

    @Nullable
    @Override
    public Icon getIcon ()
    {
        return SokratesIcons.LESH_LANG;
    }

    @NotNull
    @Override
    public SyntaxHighlighter getHighlighter ()
    {
        return new LeshSyntaxHighlighter ();
    }

    @NotNull
    @Override
    public String getDemoText ()
    {
        return DEMO_TEXT;
    }

    public static final String DEMO_TEXT = "[PLIK foo.inc]\n" +
                                               "\n" +
                                               "procedure <declaration>foo</declaration>(<local>param</local>)\n" +
                                               "{procedura foo}\n" +
                                               "begin\n" +
                                               "    <global>hmm</global> = 'global'\n" +
                                               "    if <builtin>rform</builtin>(<global>cytata</global>, ok) # 'DD'\n" +
                                               "        edytowalne = 'N'\n" +
                                               "    else\n" +
                                               "        let <local>hmm</local> = 'local: ' + <global>hmm</global>\n" +
                                               "        print(<local>hmm</local> + \\10)\n" +
                                               "    endif\n" +
                                               "    <procedure>foo</procedure>\n" +
                                               "end{foo}\n";

    @Nullable
    @Override
    public Map<String, TextAttributesKey> getAdditionalHighlightingTagToDescriptorMap ()
    {
        return tags;
    }
    
    private static Map<String, TextAttributesKey> tags = Map.of("local", LeshSyntaxHighlighter.LOCAL_VARIABLE,
                                                                "global", LeshSyntaxHighlighter.GLOBAL_VARIABLE,
                                                                "builtin", LeshSyntaxHighlighter.BUILTIN,
                                                                "procedure", LeshSyntaxHighlighter.PROCEDURE,
                                                                "declaration", LeshSyntaxHighlighter.DECLARATION);

    @NotNull
    @Override
    public AttributesDescriptor[] getAttributeDescriptors ()
    {
        return DESCRIPTORS;
    }

    @NotNull
    @Override
    public ColorDescriptor[] getColorDescriptors ()
    {
        return ColorDescriptor.EMPTY_ARRAY;
    }

    @NotNull
    @Override
    public String getDisplayName ()
    {
        return "SOWA: LeshLang";
    }
}
