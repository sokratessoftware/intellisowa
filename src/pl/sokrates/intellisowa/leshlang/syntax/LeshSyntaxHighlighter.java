package pl.sokrates.intellisowa.leshlang.syntax;

import com.intellij.lexer.Lexer;
import com.intellij.openapi.editor.DefaultLanguageHighlighterColors;
import com.intellij.openapi.editor.HighlighterColors;
import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.openapi.fileTypes.SyntaxHighlighterBase;
import com.intellij.psi.TokenType;
import com.intellij.psi.tree.IElementType;
import com.intellij.psi.tree.TokenSet;
import org.jetbrains.annotations.NotNull;
import pl.sokrates.intellisowa.leshlang.LeshLexerAdapter;
import pl.sokrates.intellisowa.leshlang.psi.LeshTypes;

import static com.intellij.openapi.editor.colors.TextAttributesKey.createTextAttributesKey;

public class LeshSyntaxHighlighter extends SyntaxHighlighterBase
{
    public static final TextAttributesKey OPERATOR      =
        createTextAttributesKey ("LESH_OPERATOR", DefaultLanguageHighlighterColors.OPERATION_SIGN);
    public static final TextAttributesKey SEPARATOR     =
        createTextAttributesKey ("LESH_SEPARATOR", DefaultLanguageHighlighterColors.PARENTHESES);
    public static final TextAttributesKey KEYWORD       =
        createTextAttributesKey ("LESH_KEYWORD", DefaultLanguageHighlighterColors.KEYWORD);
    public static final TextAttributesKey BUILTIN       =
        createTextAttributesKey ("LESH_BUILTIN", DefaultLanguageHighlighterColors.PREDEFINED_SYMBOL);
//    public static final TextAttributesKey SPECIAL       =
//        createTextAttributesKey ("LESH_SPECIAL", DefaultLanguageHighlighterColors.KEYWORD);
    public static final TextAttributesKey DIRECTIVE     =
        createTextAttributesKey ("LESH_DIRECTIVE", DefaultLanguageHighlighterColors.METADATA);
    public static final TextAttributesKey IDENTIFIER    =
        createTextAttributesKey ("LESH_IDENTIFIER", DefaultLanguageHighlighterColors.IDENTIFIER);
    public static final TextAttributesKey PROCEDURE      =
        createTextAttributesKey ("LESH_PROCEDURE", DefaultLanguageHighlighterColors.FUNCTION_CALL);
    public static final TextAttributesKey STRING        =
        createTextAttributesKey ("LESH_STRING", DefaultLanguageHighlighterColors.STRING);
    public static final TextAttributesKey CHARACTER     =
        createTextAttributesKey ("LESH_CHARACTER", DefaultLanguageHighlighterColors.STRING);
    public static final TextAttributesKey COMMENT       =
        createTextAttributesKey ("LESH_COMMENT", DefaultLanguageHighlighterColors.BLOCK_COMMENT);
    public static final TextAttributesKey BAD_CHARACTER =
        createTextAttributesKey ("LESH_BAD_CHARACTER", HighlighterColors.BAD_CHARACTER);
    public static final TextAttributesKey LOCAL_VARIABLE =
        createTextAttributesKey ("LESH_LOCAL", DefaultLanguageHighlighterColors.LOCAL_VARIABLE);
    public static final TextAttributesKey GLOBAL_VARIABLE =
        createTextAttributesKey ("LESH_GLOBAL", DefaultLanguageHighlighterColors.GLOBAL_VARIABLE);
    public static final TextAttributesKey DECLARATION =
        createTextAttributesKey ("LESH_DECLARATION", DefaultLanguageHighlighterColors.FUNCTION_DECLARATION);
    
    public static final TokenSet OPERATOR_SET = TokenSet.create (
        LeshTypes.OP_BANG,
        LeshTypes.OP_EQUALS,
        LeshTypes.OP_GT,
        LeshTypes.OP_HASH,
        LeshTypes.OP_OR,
        LeshTypes.OP_PLUS);

    public static final TokenSet SEPARATOR_SET = TokenSet.create (
        LeshTypes.OP_LEFTPAREN,
        LeshTypes.OP_RIGHTPAREN,
        LeshTypes.OP_COMMA,
        LeshTypes.OP_PASS);

    public static final TokenSet KEYWORD_SET = TokenSet.create (
        LeshTypes.KW_BEGIN,
        LeshTypes.KW_ELIF,
        LeshTypes.KW_ELSE,
        LeshTypes.KW_END,
        LeshTypes.KW_ENDDO,
        LeshTypes.KW_ENDIF,
        LeshTypes.KW_IF,
        LeshTypes.KW_LET,
        LeshTypes.KW_PROCEDURE,
        LeshTypes.KW_FUNCTION,
        LeshTypes.KW_RETURN,
        LeshTypes.KW_VOID,
        LeshTypes.KW_WHILE,
        LeshTypes.IDENTIFIER_RESERVED);

    public static final TokenSet COMMENT_SET = TokenSet.create (
        LeshTypes.COMMENT,
        LeshTypes.COMMENT_START,
        LeshTypes.COMMENT_END);

    public static final TokenSet DIRECTIVE_SET = TokenSet.create (
        LeshTypes.DIRECTIVE_START,
        LeshTypes.DIRECTIVE_PLIK,
        LeshTypes.DIRECTIVE_GLOBALS,
        LeshTypes.DIRECTIVE_PATH,
        LeshTypes.DIRECTIVE_VALUE,
        LeshTypes.DIRECTIVE_WS,
        LeshTypes.DIRECTIVE_END);

    private static final TextAttributesKey[] BAD_CHAR_KEYS   = new TextAttributesKey[] { BAD_CHARACTER };
    private static final TextAttributesKey[] OPERATOR_KEYS   = new TextAttributesKey[] { OPERATOR };
    private static final TextAttributesKey[] SEPARATOR_KEYS  = new TextAttributesKey[] { SEPARATOR };
    private static final TextAttributesKey[] KEYWORD_KEYS    = new TextAttributesKey[] { KEYWORD };
//    private static final TextAttributesKey[] BUILTIN_KEYS    = new TextAttributesKey[] { BUILTIN };
//    private static final TextAttributesKey[] SPECIAL_KEYS    = new TextAttributesKey[] { SPECIAL };
    private static final TextAttributesKey[] DIRECTIVE_KEYS  = new TextAttributesKey[] { DIRECTIVE };
    private static final TextAttributesKey[] IDENTIFIER_KEYS = new TextAttributesKey[] { IDENTIFIER };
    private static final TextAttributesKey[] STRING_KEYS     = new TextAttributesKey[] { STRING };
    private static final TextAttributesKey[] CHARACTER_KEYS  = new TextAttributesKey[] { CHARACTER };
    private static final TextAttributesKey[] COMMENT_KEYS    = new TextAttributesKey[] { COMMENT };
    private static final TextAttributesKey[] EMPTY_KEYS      = new TextAttributesKey[0];

    @NotNull
    @Override
    public Lexer getHighlightingLexer ()
    {
        return new LeshLexerAdapter ();
    }
    
    @NotNull
    @Override
    public TextAttributesKey[] getTokenHighlights (IElementType tokenType)
    {
        if (tokenType.equals (LeshTypes.STRING)) return STRING_KEYS;
        if (tokenType.equals (LeshTypes.CHARACTER)) return CHARACTER_KEYS;
        if (tokenType.equals (LeshTypes.IDENTIFIER)) return IDENTIFIER_KEYS;
        //if (tokenType.equals (LeshTypes.IDENTIFIER_RESERVED)) return SPECIAL_KEYS;
        if (tokenType.equals (TokenType.BAD_CHARACTER)) return BAD_CHAR_KEYS;
        if (KEYWORD_SET.contains (tokenType)) return KEYWORD_KEYS;
        if (SEPARATOR_SET.contains (tokenType)) return SEPARATOR_KEYS;
        if (OPERATOR_SET.contains (tokenType)) return OPERATOR_KEYS;
        if (COMMENT_SET.contains (tokenType)) return COMMENT_KEYS;
        if (DIRECTIVE_SET.contains (tokenType)) return DIRECTIVE_KEYS;
        return EMPTY_KEYS;
    }
}
