package pl.sokrates.intellisowa.leshlang.codeStyle;

import com.intellij.application.options.CodeStyleAbstractConfigurable;
import com.intellij.application.options.CodeStyleAbstractPanel;
import com.intellij.application.options.TabbedLanguageCodeStylePanel;
import com.intellij.psi.codeStyle.CodeStyleConfigurable;
import com.intellij.psi.codeStyle.CodeStyleSettings;
import com.intellij.psi.codeStyle.CodeStyleSettingsProvider;
import com.intellij.psi.codeStyle.CustomCodeStyleSettings;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import pl.sokrates.intellisowa.common.codeStyle.SowaCodeStyleSettings;
import pl.sokrates.intellisowa.leshlang.LeshLanguage;

public class LeshCodeStyleSettingsProvider extends CodeStyleSettingsProvider
{
    @Nullable
    @Override
    public CustomCodeStyleSettings createCustomSettings (CodeStyleSettings settings)
    {
        return new SowaCodeStyleSettings (settings);
    }

    @Nullable
    @Override
    public String getConfigurableDisplayName ()
    {
        return "SOWA: LeshLang";
    }
    
    @Override
    public @NotNull CodeStyleConfigurable createConfigurable (@NotNull CodeStyleSettings settings,
                                                              @NotNull CodeStyleSettings modelSettings)
    {
        return new CodeStyleAbstractConfigurable (settings, modelSettings, "SOWA")
        {
            @Override
            protected CodeStyleAbstractPanel createPanel (CodeStyleSettings codeStyleSettings)
            {
                return new LeshCodeStyleMainPanel (getCurrentSettings (), settings);
            }

            @Nullable
            @Override
            public String getHelpTopic ()
            {
                return null;
            }
        };
    }

    private static class LeshCodeStyleMainPanel extends TabbedLanguageCodeStylePanel
    {
        public LeshCodeStyleMainPanel (CodeStyleSettings currentSettings, CodeStyleSettings settings)
        {
            super (LeshLanguage.INSTANCE, currentSettings, settings);
        }
        
        @Override
        protected void initTabs (CodeStyleSettings settings)
        {
            this.addIndentOptionsTab(settings);
        }
    }
}
