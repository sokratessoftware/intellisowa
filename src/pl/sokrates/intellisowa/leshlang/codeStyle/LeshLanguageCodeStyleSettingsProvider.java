package pl.sokrates.intellisowa.leshlang.codeStyle;

import com.intellij.application.options.IndentOptionsEditor;
import com.intellij.application.options.SmartIndentOptionsEditor;
import com.intellij.lang.Language;
import com.intellij.psi.codeStyle.CodeStyleSettingsCustomizable;
import com.intellij.psi.codeStyle.LanguageCodeStyleSettingsProvider;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import pl.sokrates.intellisowa.leshlang.LeshLanguage;
import pl.sokrates.intellisowa.leshlang.syntax.LeshColorSettingsPage;

public class LeshLanguageCodeStyleSettingsProvider extends LanguageCodeStyleSettingsProvider
{
    @NotNull
    @Override
    public Language getLanguage ()
    {
        return LeshLanguage.INSTANCE;
    }

    @Nullable
    @Override
    public IndentOptionsEditor getIndentOptionsEditor ()
    {
        return new SmartIndentOptionsEditor ();
    }

    @Override
    public void customizeSettings (@NotNull CodeStyleSettingsCustomizable consumer,
                                   @NotNull SettingsType settingsType)
    {
    }

    @Override
    public String getCodeSample (@NotNull SettingsType settingsType)
    {
        return LeshColorSettingsPage.DEMO_TEXT;
    }
}
