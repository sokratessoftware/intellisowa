package pl.sokrates.intellisowa.leshlang;

import com.intellij.lang.parameterInfo.CreateParameterInfoContext;
import com.intellij.lang.parameterInfo.ParameterInfoUIContext;
import org.jetbrains.annotations.NotNull;
import pl.sokrates.intellisowa.leshlang.psi.*;
import pl.sokrates.intellisowa.leshlang.psi.refs.LeshSubroutineRef;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class LeshSubroutineParameterInfoHandler extends LeshParameterInfoHandler<LeshCall, LeshSubroutine>
{
    @Override
    protected Class<LeshCall> getPsiClass ()
    {
        return LeshCall.class;
    }
    
    @Override
    public void showParameterInfo (@NotNull LeshCall element,
                                   @NotNull CreateParameterInfoContext context)
    {
        var cls = element instanceof LeshProcedureCallNew ? LeshProcedure.class : LeshFunction.class;
        var items = new ArrayList<Object>();
        var name = element.getCallName().getText().toLowerCase();
        
        LeshBuiltins.collectBuiltinSubroutines(element.getProject(), cls, name, items);
        
        LeshSubroutineRef.collectFromFile((LeshFile) element.getContainingFile(),
                                          cls,
                                          element.getCallName().getText().toLowerCase(),
                                          -1,
                                          element,
                                          null,
                                          result -> items.add(result.getParent()));
        
        if (items.isEmpty())
            return;
        
        context.setItemsToShow(items.toArray());
        context.showHint(element, element.getCallName().getTextRange ().getEndOffset (), this);
    }
    
    @Override
    public void updateUI (LeshSubroutine procedure, @NotNull ParameterInfoUIContext context)
    {
        int adj = context.getParameterOwner().getParent() instanceof LeshPassOp ? 1 : 0;
        String name = procedure.getSubroutineName().getName();
        String kind = procedure instanceof LeshProcedure ? "procedure " + name : "function " + name;
        String join = procedure.getFormalArgumentList().stream().map(param -> param.getName()).collect(Collectors.joining(", "));
        String args = kind + "(" + join + ")";
        String argz = kind + "," + join + ",";
        int current = context.getCurrentParameterIndex();
        int from = kind.length() - name.length(), to = kind.length();
        
        if (current > 0) {
            current += adj;
            while (current-- > 0) {
                from = argz.indexOf(',', to) + 1;
                to = argz.indexOf(',', from + 1);
            }
        }
        
        context.setupUIComponentPresentation (args,
                                              from, to,
                                              !context.isUIComponentEnabled (),
                                              false, false,
                                              context.getDefaultParameterColor ());
    }
}
