package pl.sokrates.intellisowa.leshlang;

import com.intellij.psi.PsiElement;
import com.intellij.usages.impl.rules.UsageType;
import com.intellij.usages.impl.rules.UsageTypeProvider;
import pl.sokrates.intellisowa.leshlang.psi.*;

public class LeshUsageTypeProvider implements UsageTypeProvider
{
    private static final UsageType STATEMENT_INVOCATION = new UsageType ("Statement invocation");
    private static final UsageType EXPRESSION_INVOCATION = new UsageType ("Expression invocation");

    @Override
    public UsageType getUsageType (PsiElement element)
    {
        if (element instanceof LeshVariable) {
            PsiElement parent = element.getParent ();
            if (parent instanceof LeshAssignment && ((LeshAssignment) parent).getVariable () == element)
                return UsageType.WRITE;
            else
                return UsageType.READ;
        }

        if (element instanceof LeshProcedureCall)
            return STATEMENT_INVOCATION;
        
        if (element instanceof LeshFunctionCall)
            return EXPRESSION_INVOCATION;
        
        return null;
    }
}