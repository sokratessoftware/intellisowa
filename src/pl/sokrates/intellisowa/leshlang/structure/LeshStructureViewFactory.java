package pl.sokrates.intellisowa.leshlang.structure;

import com.intellij.ide.structureView.StructureViewBuilder;
import com.intellij.ide.structureView.StructureViewModel;
import com.intellij.ide.structureView.TreeBasedStructureViewBuilder;
import com.intellij.lang.PsiStructureViewFactory;
import com.intellij.openapi.editor.Editor;
import com.intellij.psi.PsiFile;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import pl.sokrates.intellisowa.leshlang.psi.LeshFile;

public class LeshStructureViewFactory implements PsiStructureViewFactory
{
    @NotNull
    public StructureViewBuilder getStructureViewBuilder (final PsiFile psiFile)
    {
        return new TreeBasedStructureViewBuilder ()
        {
            @NotNull
            @Override
            public StructureViewModel createStructureViewModel (@Nullable Editor editor)
            {
                return new LeshFileStructureViewModel ((LeshFile) psiFile);
            }
        };
    }
}
