/*
 * Copyright 2000-2014 JetBrains s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package pl.sokrates.intellisowa.leshlang.structure;

import com.intellij.icons.AllIcons;
import com.intellij.ide.structureView.StructureViewTreeElement;
import com.intellij.ide.structureView.TextEditorBasedStructureViewModel;
import com.intellij.ide.structureView.impl.common.PsiTreeElementBase;
import com.intellij.ide.util.treeView.smartTree.SortableTreeElement;
import com.intellij.ide.util.treeView.smartTree.Sorter;
import com.intellij.openapi.util.NlsSafe;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import pl.sokrates.intellisowa.leshlang.psi.*;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author max
 */
public class LeshFileStructureViewModel extends TextEditorBasedStructureViewModel
{
    private final pl.sokrates.intellisowa.leshlang.psi.LeshFile myFile;

    public LeshFileStructureViewModel (final LeshFile root)
    {
        super (root);
        myFile = root;
    }

    static class FileVTE extends PsiTreeElementBase<LeshFile>
    {
        public FileVTE (LeshFile element)
        {
            super (element);
        }

        @Nullable
        @Override
        public String getPresentableText ()
        {
            return getElement().getName();
        }

        @NotNull
        @Override
        public Collection<StructureViewTreeElement> getChildrenBase ()
        {
            List<StructureViewTreeElement> result = new ArrayList<>();
            
            var file = getElement();
            LeshSection section = null;
            
            for (var child : file.getChildren()) {
                if (child instanceof LeshSection) {
                    section = (LeshSection) child;
                    result.add(new SectionVTE(section));
                }
                else if (section == null) {
                    if (child instanceof LeshSubroutine) {
                        result.add(new SubroutineVTE((LeshSubroutine) child));
                    }
                    else if (child instanceof LeshTopBlock) {
                        result.add(new BodyVTE((LeshTopBlock) child));
                    }
                }
            }
            
            return result;
        }
    }
    
    static class SectionVTE extends PsiTreeElementBase<LeshSection>
    {
        public SectionVTE (LeshSection element)
        {
            super (element);
        }
        
        @Nullable
        @Override
        public String getPresentableText ()
        {
            return getElement().getText();
        }
        
        @NotNull
        @Override
        public Collection<StructureViewTreeElement> getChildrenBase ()
        {
            List<StructureViewTreeElement> result = new ArrayList<>();
            
            PsiElement sibling = getElement();
            while ((sibling = sibling.getNextSibling()) != null) {
                if (sibling instanceof LeshSubroutine)
                    result.add(new SubroutineVTE((LeshSubroutine) sibling));
                
                else if (sibling instanceof LeshTopBlock)
                    result.add(new BodyVTE((LeshTopBlock) sibling));
                
                else if (sibling instanceof LeshSection)
                    break;
            }
            
            return result;
        }
    
        @Override
        public Icon getIcon (boolean open)
        {
            return AllIcons.Nodes.Package;
        }
    }

    static class BodyVTE extends PsiTreeElementBase<LeshTopBlock>
    {
        protected BodyVTE (LeshTopBlock psiElement)
        {
            super(psiElement);
        }
    
        @Override
        public @NotNull Collection<StructureViewTreeElement> getChildrenBase ()
        {
            return Collections.emptyList();
        }
    
        @Override
        public @NlsSafe @Nullable String getPresentableText ()
        {
            return "begin ... end";
        }
        
        @Override
        public Icon getIcon (boolean open)
        {
            return AllIcons.Nodes.EntryPoints;
        }
    }
    
    static class SubroutineVTE extends PsiTreeElementBase<LeshSubroutine> implements SortableTreeElement
    {
        private String sortKey = "";
        private String text = "";
        private Icon icon = null;
        
        public SubroutineVTE (LeshSubroutine element)
        {
            super (element);
            
            var nameElement = element.getSubroutineName();
            var name = nameElement == null ? "<?>" : nameElement.getName();
            icon = element instanceof LeshProcedure ? AllIcons.Nodes.Method : AllIcons.Nodes.Function;
            String kind = element instanceof LeshProcedure ? "procedure " : "function ";
            String common = name + "(" + element.getFormalArgumentList().stream().map(param -> param.getText()).collect(
                Collectors.joining(", ")) + ")";
            sortKey = "000000000" + common + kind;
            text = kind + common;
        }
    
        @Override
        public @NotNull String getAlphaSortKey ()
        {
            return sortKey;
        }
    
        @Nullable
        @Override
        public String getPresentableText ()
        {
            return text;
        }

        @Nullable
        @Override
        public Icon getIcon (boolean open)
        {
            return icon;
        }

        @NotNull
        @Override
        public Collection<StructureViewTreeElement> getChildrenBase ()
        {
            return Collections.emptyList();
        }
        
    }

    @NotNull
    public StructureViewTreeElement getRoot ()
    {
        return new FileVTE (myFile);
    }

    @NotNull
    public Sorter[] getSorters ()
    {
        return new Sorter[] { Sorter.ALPHA_SORTER };
    }

    protected PsiFile getPsiFile ()
    {
        return myFile;
    }

    @NotNull
    protected Class[] getSuitableClasses ()
    {
        return new Class[] { LeshFile.class, LeshProcedureName.class };
    }
}
