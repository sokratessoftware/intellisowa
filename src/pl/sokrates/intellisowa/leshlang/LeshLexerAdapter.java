package pl.sokrates.intellisowa.leshlang;

import com.intellij.lexer.FlexAdapter;

import java.io.Reader;

public class LeshLexerAdapter extends FlexAdapter
{
    public LeshLexerAdapter ()
    {
        super (new LeshLexer (null));
    }
}
