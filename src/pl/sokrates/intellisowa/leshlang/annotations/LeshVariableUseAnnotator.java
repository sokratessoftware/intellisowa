package pl.sokrates.intellisowa.leshlang.annotations;

import com.intellij.codeInspection.ProblemHighlightType;
import com.intellij.lang.annotation.AnnotationHolder;
import com.intellij.lang.annotation.Annotator;
import com.intellij.lang.annotation.HighlightSeverity;
import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.psi.PsiElement;
import org.jetbrains.annotations.NotNull;
import pl.sokrates.intellisowa.leshlang.LeshBuiltins;
import pl.sokrates.intellisowa.leshlang.psi.*;
import pl.sokrates.intellisowa.leshlang.psi.refs.LeshVariableRef;
import pl.sokrates.intellisowa.leshlang.syntax.LeshSyntaxHighlighter;

import java.util.Set;

public class LeshVariableUseAnnotator implements Annotator
{
    public static Set<String> commonVariables = Set.of("rekord", "lp", "header", "__slimit__", "__ilimit__", "__i__");
    public static Set<String> reservedVariables = Set.of("ok", "pierwotne", "pole", "pozycja", "edytowalne");
    public static Set<String> roVariables = Set.of("ok", "pierwotne");
    
    @Override
    public void annotate (@NotNull PsiElement element, @NotNull AnnotationHolder annotationHolder)
    {
        if (element instanceof LeshVariable) {
            if (((LeshVariable) element).getLocalTarget() != null) {
                highlight(element, annotationHolder, LeshSyntaxHighlighter.LOCAL_VARIABLE);
                return;
            }
            
            var name = element.getText().toLowerCase();
            var reserved = element.getNode().getFirstChildNode().getElementType() == LeshTypes.IDENTIFIER_RESERVED;
            PsiElement parent = element.getParent ();
            
            if (parent instanceof LeshTerm && !reservedVariables.contains(name) && !commonVariables.contains(name)) {
                LeshVariableRef ref = (LeshVariableRef) element.getReference ();
    
                if (!ref.wasAssigned ()) {
                    annotationHolder.newAnnotation(HighlightSeverity.WARNING, "Zmienna `" + element.getText () + "` nie została przypisana nigdzie w pliku.")
                        .range(element)
                        .highlightType (ProblemHighlightType.LIKE_UNKNOWN_SYMBOL)
                        .create();
                }
                else if (!reserved ) {
                    highlight(element, annotationHolder, LeshSyntaxHighlighter.GLOBAL_VARIABLE);
                }
            }
            else if (parent instanceof LeshAssignment) {
                if (roVariables.contains(name) && element.getContainingFile() != LeshBuiltins.getBuiltinsFile(element.getProject())) {
                    annotationHolder.newAnnotation(HighlightSeverity.ERROR, "Zmienna `" + element.getText () + "` jest tylko do odczytu.")
                                    .range(element)
                                    .highlightType (ProblemHighlightType.LIKE_UNKNOWN_SYMBOL)
                                    .create();
                }
                else if (!reserved) {
                    highlight(element, annotationHolder, LeshSyntaxHighlighter.GLOBAL_VARIABLE);
                }
            }
        }
        else if (element instanceof LeshLetName || element instanceof LeshFormalArgument) {
            var reserved = element.getNode().getFirstChildNode().getElementType() == LeshTypes.IDENTIFIER_RESERVED;
            
            if (reserved) {
                annotationHolder.newAnnotation(HighlightSeverity.ERROR, "Nazwa zmiennej `" + element.getText() + "` jest zarezerwowana.")
                                .range(element)
                                .create();
            }
            else {
                highlight(element, annotationHolder, LeshSyntaxHighlighter.LOCAL_VARIABLE);
            }
        }
    }
    
    private void highlight(@NotNull PsiElement element, @NotNull AnnotationHolder annotationHolder, TextAttributesKey attr) {
        annotationHolder
            .newSilentAnnotation(HighlightSeverity.INFORMATION)
            .range(element)
            .textAttributes(attr).create();
    }
}
