//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
// -- na podstawie AddIncludeAction z IDEI

package pl.sokrates.intellisowa.leshlang.annotations;

import com.intellij.codeInsight.FileModificationService;
import com.intellij.codeInsight.daemon.QuickFixBundle;
import com.intellij.codeInsight.hint.QuestionAction;
import com.intellij.ide.util.DefaultPsiElementCellRenderer;
import com.intellij.lang.ASTNode;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.command.CommandProcessor;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.project.DumbService;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.popup.PopupStep;
import com.intellij.openapi.ui.popup.util.BaseListPopupStep;
import com.intellij.psi.PsiDocumentManager;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.TokenType;
import com.intellij.psi.tree.TokenSet;
import com.intellij.ui.popup.list.ListPopupImpl;
import com.intellij.ui.popup.list.PopupListElementRenderer;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import pl.sokrates.intellisowa.leshlang.psi.LeshElementFactory;
import pl.sokrates.intellisowa.leshlang.psi.LeshInclude;
import pl.sokrates.intellisowa.leshlang.psi.LeshSection;
import pl.sokrates.intellisowa.leshlang.psi.LeshTypes;

import javax.swing.*;
import java.awt.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class AddIncludeAction implements QuestionAction
{
    private final Project          myProject;
    private final PsiElement       myElement;
    private final List<? extends PsiElement> myCandidates;
    private final Editor           myEditor;

    public AddIncludeAction (@NotNull Project project,
                             @NotNull PsiElement element,
                             @Nullable Editor editor,
                             @NotNull List<? extends PsiElement> candidates)
    {
        this.myProject = project;
        this.myElement = element;
        this.myCandidates = candidates;
        this.myEditor = editor;
    }

    public boolean execute ()
    {
        PsiDocumentManager.getInstance (this.myProject).commitAllDocuments ();
        if (!this.myElement.isValid ()) {
            return false;
        }
        else {
            if (this.myCandidates.size() == 1) {
                this.addImport(this.myElement, this.myCandidates.get(0));
            }
            else {
                this.chooseFileAndImport();
            }

            return true;
        }
    }

    private void chooseFileAndImport ()
    {
        final BaseListPopupStep step = new BaseListPopupStep<PsiElement> ("Wybierz plik do importu ", this.myCandidates)
        {
            public boolean isAutoSelectionEnabled ()
            {
                return false;
            }

            public boolean isSpeedSearchEnabled ()
            {
                return true;
            }

            @Override
            public PopupStep<?> onChosen (final PsiElement selectedValue, boolean finalChoice)
            {
                if (selectedValue == null) {
                    return FINAL_CHOICE;
                }

                return this.doFinalStep (new Runnable ()
                {
                    @Override
                    public void run ()
                    {
                        PsiDocumentManager.getInstance (AddIncludeAction.this.myProject).commitAllDocuments ();
                        AddIncludeAction.this.addImport (AddIncludeAction.this.myElement, selectedValue);
                    }
                });
            }

            @NotNull
            public String getTextFor (PsiElement value)
            {
                return value.getText();
            }

            public Icon getIconFor (PsiElement aValue)
            {
                return aValue.getIcon (0);
            }
        };
        ListPopupImpl popup = new ListPopupImpl(myProject, step)
        {
            protected ListCellRenderer getListElementRenderer ()
            {
                final PopupListElementRenderer      baseRenderer = (PopupListElementRenderer) super.getListElementRenderer ();
                final DefaultPsiElementCellRenderer psiRenderer  = new DefaultPsiElementCellRenderer ();
                return new ListCellRenderer ()
                {
                    public Component getListCellRendererComponent (JList list,
                                                                   Object value,
                                                                   int index,
                                                                   boolean isSelected,
                                                                   boolean cellHasFocus)
                    {
                        JPanel panel = new JPanel (new BorderLayout ());
                        baseRenderer.getListCellRendererComponent (list, value, index, isSelected, cellHasFocus);
                        panel.add (baseRenderer.getNextStepLabel (), "East");
                        panel.add (psiRenderer.getListCellRendererComponent (list,
                                                                             value,
                                                                             index,
                                                                             isSelected,
                                                                             cellHasFocus));
                        return panel;
                    }
                };
            }
        };
        popup.showInBestPositionFor (this.myEditor);
    }

    private void addImport (final PsiElement element, final PsiElement candidate)
    {
        CommandProcessor.getInstance ().executeCommand (this.myProject, new Runnable ()
        {
            @Override
            public void run ()
            {
                ApplicationManager.getApplication ().runWriteAction (new Runnable ()
                {
                    @Override
                    public void run ()
                    {
                        DumbService.getInstance (AddIncludeAction.this.myProject).withAlternativeResolveEnabled (new Runnable ()
                        {
                            @Override
                            public void run ()
                            {
                                AddIncludeAction.this._addImport (element, candidate);
                            }
                        });
                    }
                });
            }
        }, QuickFixBundle.message ("add.import"), null);
    }

    private static TokenSet skipTokens = TokenSet.create (
        LeshTypes.COMMENT_START,
        LeshTypes.COMMENT,
        LeshTypes.COMMENT_END,
        TokenType.WHITE_SPACE);

    private void _addImport (PsiElement element, PsiElement candidate)
    {
        var candidateFile = candidate.getContainingFile();
        PsiFile file = element.getContainingFile ();
        if (element.isValid () && candidate.isValid () && element.getReference ().resolve () != candidate) {
            if (FileModificationService.getInstance ().preparePsiElementForWrite (file)) {
                PsiElement beforeElement = element;
                PsiElement temp;
                // cofamy się do poziomu bezpośrednio pod plikiem
                while ((temp = beforeElement.getParent ()) != file) beforeElement = temp;
                // cofamy się do pierwszej dyrektywy
                while ((temp = beforeElement.getPrevSibling ()) != null && !(temp instanceof LeshInclude) && !(temp instanceof LeshSection))
                    beforeElement = temp;

                ASTNode before = null;
                if (beforeElement != null) before = beforeElement.getNode ();

                // jeśli wylądowaliśmy na początku pliku, to przesuwamy się poniżej wszelkich komentarzy
                if (temp == null)
                    while (skipTokens.contains (before.getElementType ()))
                        before = before.getTreeNext ();

                Path path1 = Paths.get (file.getVirtualFile ().getParent ().getPath ());
                Path path2 = Paths.get (candidateFile.getVirtualFile ().getPath ());
                Path path  = path1.relativize (path2);

                PsiElement include = LeshElementFactory.createInclude (candidate.getProject (),
                                                                       path.toString ().replace ("/", "\\"));
                PsiElement newline1 = include.getPrevSibling ();
                PsiElement newline2 = include.getNextSibling ();
                file.getNode ().addChild (include.getNode (), before);

                if (before != null && before.getElementType () == TokenType.WHITE_SPACE)
                    file.getNode ().addChild (newline1.getNode (), include.getNode ());
                else {
                    file.getNode ().addChild (newline1.getNode (), before);
                    file.getNode ().addChild (newline2.getNode (), before);
                }
            }
        }
    }

}
