package pl.sokrates.intellisowa.leshlang.annotations;

import com.intellij.codeInsight.hint.QuestionAction;
import com.intellij.codeInsight.intention.IntentionAction;
import com.intellij.codeInspection.ProblemHighlightType;
import com.intellij.lang.annotation.AnnotationHolder;
import com.intellij.lang.annotation.Annotator;
import com.intellij.lang.annotation.HighlightSeverity;
import com.intellij.openapi.command.CommandProcessor;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementResolveResult;
import com.intellij.psi.PsiFile;
import com.intellij.util.IncorrectOperationException;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import pl.sokrates.intellisowa.leshlang.index.IndexUtil;
import pl.sokrates.intellisowa.leshlang.psi.LeshCallName;
import pl.sokrates.intellisowa.leshlang.psi.LeshProcedureCall;
import pl.sokrates.intellisowa.leshlang.psi.LeshSubroutine;
import pl.sokrates.intellisowa.leshlang.psi.LeshTypes;
import pl.sokrates.intellisowa.leshlang.psi.refs.LeshSubroutineRef;
import pl.sokrates.intellisowa.leshlang.syntax.LeshSyntaxHighlighter;

import java.util.Collections;
import java.util.Comparator;

public class LeshSubroutineCallAnnotator implements Annotator
{
    private static class ImportFix implements IntentionAction
    {
        private final LeshCallName myElement;

        public ImportFix (LeshCallName element)
        {
            myElement = element;
        }

        @Nls
        @NotNull
        //@Override
        public String getName ()
        {
            return "Importuj procedurę/funkcję";
        }

        @Nls
        @NotNull
        @Override
        public String getFamilyName ()
        {
            return getName ();
        }

        @Nls
        @NotNull
        @Override
        public String getText ()
        {
            return getName ();
        }

        public boolean startInWriteAction ()
        {
            return false;
        }

        @Override
        public boolean isAvailable (@NotNull Project project, Editor editor, PsiFile psiFile)
        {
            return myElement.isValid ();
        }

        @Override
        public void invoke (@NotNull Project project,
                            Editor editor,
                            PsiFile psiFile) throws IncorrectOperationException
        {
            var wantProcedures = myElement instanceof LeshProcedureCall;
            var candidates = IndexUtil.findSubroutinesInProject(project, myElement.getText(), wantProcedures, !wantProcedures);
            if (candidates != null) {
                Collections.sort (candidates, new Comparator<LeshSubroutine> ()
                {
                    @Override
                    public int compare (LeshSubroutine o1, LeshSubroutine o2)
                    {
                        return o1.getName ().compareTo (o2.getName ());
                    }
                });

                final QuestionAction action = new AddIncludeAction (project,
                                                                    myElement,
                                                                    editor,
                                                                    candidates);
                CommandProcessor.getInstance ().runUndoTransparentAction (new Runnable ()
                {
                    public void run ()
                    {
                        action.execute ();
                    }
                });
            }
        }
    }

    @Override
    public void annotate (@NotNull PsiElement psiElement, @NotNull AnnotationHolder annotationHolder)
    {
        if (!(psiElement instanceof LeshCallName)) return;
        
        LeshSubroutineRef ref = (LeshSubroutineRef) psiElement.getReference ();
        
        var targets = ref.multiResolve(true);
        int numTargets = targets.length;
        if (numTargets == 0) {
            annotationHolder.newAnnotation(HighlightSeverity.ERROR,
                                           "Niezdefiniowana nazwa procedury lub funkcji, albo zła liczba argumentów.")
                            .range(psiElement)
                            .highlightType(ProblemHighlightType.LIKE_UNKNOWN_SYMBOL)
                            .withFix(new ImportFix(ref.getElement ()))
                            .create();
        }
        else if (numTargets > 1) {
            annotationHolder.newAnnotation(HighlightSeverity.ERROR,
                                           "Procedura lub funkcja zadeklarowana więcej niż raz. Użyj Ctrl-B by odnaleźć miejsca.")
                .range(psiElement)
                .create();
        }
        else if (psiElement.getNode().getFirstChildNode().getElementType() == LeshTypes.IDENTIFIER){
            annotationHolder.newSilentAnnotation(HighlightSeverity.INFORMATION)
                .range(psiElement)
                .textAttributes(((PsiElementResolveResult) targets[0]).getElement().getContainingFile().getName().equals("builtins.ll") ? LeshSyntaxHighlighter.BUILTIN : LeshSyntaxHighlighter.PROCEDURE)
                .create();
        }
    }
}
