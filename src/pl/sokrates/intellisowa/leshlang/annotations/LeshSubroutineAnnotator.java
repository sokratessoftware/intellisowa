package pl.sokrates.intellisowa.leshlang.annotations;

import com.intellij.codeInspection.ProblemHighlightType;
import com.intellij.lang.annotation.AnnotationHolder;
import com.intellij.lang.annotation.Annotator;
import com.intellij.lang.annotation.HighlightSeverity;
import com.intellij.psi.PsiElement;
import org.jetbrains.annotations.NotNull;
import pl.sokrates.intellisowa.leshlang.LeshBuiltins;
import pl.sokrates.intellisowa.leshlang.psi.LeshFunction;
import pl.sokrates.intellisowa.leshlang.psi.LeshProcedure;
import pl.sokrates.intellisowa.leshlang.psi.LeshSubroutine;
import pl.sokrates.intellisowa.leshlang.syntax.LeshSyntaxHighlighter;

public class LeshSubroutineAnnotator implements Annotator
{
    @Override
    public void annotate (@NotNull PsiElement element, @NotNull AnnotationHolder holder)
    {
        if (!(element instanceof LeshSubroutine)) return;
        
        var project = element.getProject();
        if (element.getContainingFile() == LeshBuiltins.getBuiltinsFile(project)) return;
        
        var signature = ((LeshSubroutine) element).getSignature();
        
        if ((element instanceof LeshProcedure &&  LeshBuiltins.getBuiltinProcedures(project).containsKey(signature))
                || (element instanceof LeshFunction && LeshBuiltins.getBuiltinFunctions(project).containsKey(signature))) {
            holder.newAnnotation(HighlightSeverity.WARNING,
                                 "Próba nadpisania wbudowanej procedury lub funkcji.")
                  .range(((LeshSubroutine) element).getSubroutineName())
                  .highlightType(ProblemHighlightType.LIKE_UNKNOWN_SYMBOL)
                  .create();
        }
        else {
            holder.newSilentAnnotation(HighlightSeverity.INFORMATION)
                  .range(((LeshSubroutine) element).getSubroutineName())
                  .textAttributes(LeshSyntaxHighlighter.DECLARATION)
                  .create();
        }
    }
}
