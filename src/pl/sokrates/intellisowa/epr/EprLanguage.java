package pl.sokrates.intellisowa.epr;

import com.intellij.lang.Language;
import org.jetbrains.annotations.NotNull;

public class EprLanguage extends Language
{
    public static final EprLanguage INSTANCE = new EprLanguage ();

    private EprLanguage ()
    {
        super ("EprLang");
    }

    @NotNull
    @Override
    public String getDisplayName ()
    {
        return "SOWA: EPR";
    }

    @Override
    public boolean isCaseSensitive ()
    {
        return false;
    }
}
