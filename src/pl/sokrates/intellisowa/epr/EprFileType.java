package pl.sokrates.intellisowa.epr;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import pl.sokrates.intellisowa.SokratesIcons;
import pl.sokrates.intellisowa.common.SowaFileType;

import javax.swing.*;

public class EprFileType extends SowaFileType
{
    public static final EprFileType INSTANCE = new EprFileType ();

    protected EprFileType ()
    {
        super (EprLanguage.INSTANCE);
    }

    @NotNull
    @Override
    public String getName ()
    {
        return "Formularz EPR";
    }

    @NotNull
    @Override
    public String getDescription ()
    {
        return "Formularz EPR";
    }

    @NotNull
    @Override
    public String getDefaultExtension ()
    {
        return "epr";
    }

    @Nullable
    @Override
    public Icon getIcon ()
    {
        return SokratesIcons.EPR;
    }
}

