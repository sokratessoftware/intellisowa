{
  parserClass="pl.sokrates.intellisowa.epr.parser.EprParser"

  extends="com.intellij.extapi.psi.ASTWrapperPsiElement"

  psiClassPrefix="Epr"
  psiImplClassSuffix="Impl"
  psiPackage="pl.sokrates.intellisowa.epr.psi"
  psiImplPackage="pl.sokrates.intellisowa.epr.psi.impl"

  elementTypeHolderClass="pl.sokrates.intellisowa.epr.psi.EprTypes"
  elementTypeClass="pl.sokrates.intellisowa.epr.psi.EprElementType"
  tokenTypeClass="pl.sokrates.intellisowa.epr.psi.EprTokenType"

  psiImplUtilClass="pl.sokrates.intellisowa.epr.psi.impl.EprPsiImplUtil"
}

root ::= (statement | KW_END)* // pozwalamy na dyndające ENDy z uwagi na include'y

strexpr ::= string (OP_PLUS string)*
private next_strexpr ::= OP_COMMA strexpr {pin=1}

string ::= string1 | string2

private string1 ::= STRING_START (PARAMETER_EXPANSION_IN_STRING | MACRO_EXPANSION_IN_STRING | STRING_CONTENT)* STRING_END {pin=1}
private string2 ::= OP_LEFTPAREN strexpr OP_RIGHTPAREN {pin=1}

numexpr ::= OP_MINUS? parnumexpr ((OP_PLUS | OP_MINUS | OP_SLASH) parnumexpr)*
private next_numexpr ::= OP_COMMA numexpr {pin=1}

parnumexpr ::= INT_LITERAL | parnumexpr2

private parnumexpr2 ::= OP_LEFTPAREN numexpr OP_RIGHTPAREN {pin=1}

private on_2 ::= KW_ON numexpr next_numexpr {pin=1}
private on_3 ::= KW_ON numexpr next_numexpr next_numexpr {pin=1}
private on_4 ::= KW_ON numexpr next_numexpr next_numexpr next_numexpr {pin=1}

private statements ::= statement*

private statement ::= directive | label | no | text | panel | frame | scroll | field | dictionary | date | month | number | spin | check | radio | combo | memo | markers | button | speed | menu | on | list | tabs | dynamic | form | block | other

fake str_mask ::= strexpr*
fake num_mask ::= numexpr*
str_mask1   ::= KW_MASK strexpr {pin=1 extends=str_mask}
num_mask1   ::= KW_MASK numexpr {pin=1 extends=num_mask}
num_mask12  ::= KW_MASK numexpr next_numexpr? {pin=1 extends=num_mask}

fake num_limit ::= numexpr*
num_limit1  ::= KW_LIMIT numexpr {pin=1 extends=num_limit}
num_limit2  ::= KW_LIMIT numexpr next_numexpr {pin=1 extends=num_limit} 
num_limit12 ::= KW_LIMIT numexpr next_numexpr? {pin=1 extends=num_limit}

label ::= KW_LABEL strexpr on_2 {pin=1}

no ::= KW_NO strexpr on_2 {pin=1}

text ::= KW_TEXT strexpr KW_FLOAT? on_4 {pin=1}

panel ::= KW_PANEL strexpr on_4 {pin=1}

frame ::= KW_FRAME strexpr on_4 {pin=1}

scroll ::= KW_SCROLL on_4 {pin=1}

field ::= KW_FIELD strexpr str_mask1? num_limit1? on_3 {pin=1}

dictionary ::= KW_DICTIONARY strexpr on_3 {pin=1}

date ::= KW_DATE strexpr on_3 {pin=1}

month ::= KW_MONTH strexpr on_2 {pin=1}

number ::= KW_NUMBER strexpr num_mask12 on_3 {pin=1}

spin ::= KW_SPIN strexpr num_mask1 num_limit12? on_3 {pin=1}

check ::= KW_CHECK strexpr str_mask1 on_3 {pin=1}

radio ::= KW_RADIO strexpr KW_OF numexpr str_mask1 on_4 {pin=1}

combo ::= KW_COMBO strexpr str_mask1 on_3 {pin=1}

memo ::= KW_MEMO strexpr KW_MULTILINE? KW_FLOAT? on_4 {pin=1}

markers ::= KW_MARKERS strexpr str_mask1 KW_FLOAT? on_4 {pin=1}

button ::= KW_BUTTON strexpr str_mask1 KW_DEFAULT? on_4 {pin=1}

speed ::= KW_SPEED strexpr str_mask1 on_4 {pin=1}

private menu_label ::= KW_LABEL strexpr {pin=1}

menu ::= KW_MENU strexpr ( menu_label on_2 | str_mask1? on_4 ) {pin=1}

on ::= KW_ON numexpr OP_COMMA numexpr block {pin=1}

list ::= KW_LIST strexpr KW_OF numexpr on_4 block {pin=1}

tabs ::= KW_TABS on_4 named_block* KW_END {pin=1}

dynamic ::= KW_DYNAMIC strexpr KW_OF strexpr on_4 named_block* KW_END {pin=1}

form ::= KW_FORM strexpr str_mask1 (KW_SPEED strexpr)? on_4 block {pin=1}

named_block ::= strexpr KW_BEGIN statements KW_END {pin=2}

block ::= KW_BEGIN statements KW_END {pin=1}

other ::= INTEGER | MACRO_EXPANSION | PARAMETER_EXPANSION | IDENTIFIER | strexpr

private unused_ ::= COMMENT_START | COMMENT | COMMENT_END | DIRECTIVE_WS;

// z LeshLanga

private directive ::= include | other_directive

other_directive ::= DIRECTIVE_START DIRECTIVE_WS* DIRECTIVE_VALUE (DIRECTIVE_WS | DIRECTIVE_VALUE)* DIRECTIVE_END {pin=3}

include ::= DIRECTIVE_START DIRECTIVE_WS* (DIRECTIVE_PLIK | DIRECTIVE_SKRYPT) (DIRECTIVE_WS include_path)? DIRECTIVE_WS* DIRECTIVE_END {pin=3 implements="pl.sokrates.intellisowa.common.psi.GenericInclude"}

include_path ::= DIRECTIVE_PATH {methods=[handleRename] mixin="pl.sokrates.intellisowa.common.psi.GenericIncludePathImpl" implements="pl.sokrates.intellisowa.common.psi.GenericIncludePath"}
