package pl.sokrates.intellisowa.epr;

import com.intellij.lexer.FlexLexer;
import com.intellij.psi.tree.IElementType;
import pl.sokrates.intellisowa.epr.psi.EprTypes;
import com.intellij.psi.TokenType;

%%

%class EprLexer
%implements FlexLexer
%unicode
%ignorecase
%function advance
%type IElementType
%eof{  return;
%eof}

CRLF= \n|\r|\r\n
WHITE_SPACE=[\ \t\f]
IDENTIFIER=[A-Za-z_][A-za-z0-9_]*
PATH_COMPONENT=[A-Za-z0-9_\.+-]+

%state INCOMMENT
%state INSTRING
%state INEXPANSION
%state INDIRECTIVE
%state ININCLUDE
%state INSTRING

%%

<YYINITIAL> {
    "{"                    { yybegin (INCOMMENT); return EprTypes.COMMENT_START; }
    "'"                    { yybegin (INSTRING); return EprTypes.STRING_START; }
    "["                    { yybegin (INDIRECTIVE); return EprTypes.DIRECTIVE_START; }

    "label"                { return EprTypes.KW_LABEL; }
    "no"                   { return EprTypes.KW_NO; }
    "text"                 { return EprTypes.KW_TEXT; }
    "panel"                { return EprTypes.KW_PANEL; }
    "field"                { return EprTypes.KW_FIELD; }
    "dictionary"           { return EprTypes.KW_DICTIONARY; }
    "date"                 { return EprTypes.KW_DATE; }
    "month"                { return EprTypes.KW_MONTH; }
    "number"               { return EprTypes.KW_NUMBER; }
    "spin"                 { return EprTypes.KW_SPIN; }
    "check"                { return EprTypes.KW_CHECK; }
    "radio"                { return EprTypes.KW_RADIO; }
    "combo"                { return EprTypes.KW_COMBO; }
    "memo"                 { return EprTypes.KW_MEMO; }
    "markers"              { return EprTypes.KW_MARKERS; }
    "button"               { return EprTypes.KW_BUTTON; }
    "speed"                { return EprTypes.KW_SPEED; }
    "menu"                 { return EprTypes.KW_MENU; }
    "list"                 { return EprTypes.KW_LIST; }
    "tabs"                 { return EprTypes.KW_TABS; }
    "dynamic"              { return EprTypes.KW_DYNAMIC; }
    "form"                 { return EprTypes.KW_FORM; }
    "frame"                { return EprTypes.KW_FRAME; }
    "scroll"               { return EprTypes.KW_SCROLL; }

    "on"                   { return EprTypes.KW_ON; }
    "of"                   { return EprTypes.KW_OF; }
    "mask"                 { return EprTypes.KW_MASK; }
    "limit"                { return EprTypes.KW_LIMIT; }
    "multiline"            { return EprTypes.KW_MULTILINE; }
    "default"              { return EprTypes.KW_DEFAULT; }
    "float"                { return EprTypes.KW_FLOAT; }

    "begin"                { return EprTypes.KW_BEGIN; }
    "end"                  { return EprTypes.KW_END; }

    [0-9]+                 { return EprTypes.INT_LITERAL; }
    [A-Za-z_][A-za-z0-9_]* { return EprTypes.IDENTIFIER; }
    \$\&[^$]+\$            { return EprTypes.MACRO_EXPANSION; }
    \$[A-Z0-9_]+\$         { return EprTypes.PARAMETER_EXPANSION; }
    "("                    { return EprTypes.OP_LEFTPAREN; }
    ")"                    { return EprTypes.OP_RIGHTPAREN; }
    ","                    { return EprTypes.OP_COMMA; }
    "/"                    { return EprTypes.OP_SLASH; }
    "+"                    { return EprTypes.OP_PLUS; }
    "-"                    { return EprTypes.OP_MINUS; }

    ({CRLF}|{WHITE_SPACE})+  { return TokenType.WHITE_SPACE; }

    . { return TokenType.BAD_CHARACTER; }
}

<INCOMMENT> {
    "}"             { yybegin (YYINITIAL); return EprTypes.COMMENT_END; }
    ({CRLF}|[^\}])+ { return EprTypes.COMMENT; }
}

<INSTRING> {
    "'"            { yybegin (YYINITIAL); return EprTypes.STRING_END; }
    "$"            { yybegin (INEXPANSION); yypushback(1); }
    [^'\n]+        { return EprTypes.STRING_CONTENT; }
    {CRLF}         { yybegin (YYINITIAL); return TokenType.BAD_CHARACTER; }
}

<INEXPANSION> {
    \$\&[^$ ]+\$   { yybegin (INSTRING); return EprTypes.MACRO_EXPANSION_IN_STRING; }
    \$[A-Z0-9_]+\$ { yybegin (INSTRING); return EprTypes.PARAMETER_EXPANSION_IN_STRING; }
    {CRLF}|.       { yybegin (INSTRING); return EprTypes.STRING_CONTENT; }
}

<INDIRECTIVE> {
    "]"            { yybegin (YYINITIAL); return EprTypes.DIRECTIVE_END; }
    "plik"         { yybegin (ININCLUDE); return EprTypes.DIRECTIVE_PLIK; }
    "skrypt"       { yybegin (ININCLUDE); return EprTypes.DIRECTIVE_SKRYPT; }
    {WHITE_SPACE}+ { return EprTypes.DIRECTIVE_WS; }
    [^\ \t\f\]\n]+ { return EprTypes.DIRECTIVE_VALUE; }
    {CRLF}         { yypushback (1); yybegin (YYINITIAL); }
    .              { return EprTypes.DIRECTIVE_VALUE; }
}

<ININCLUDE> {
    {PATH_COMPONENT} |
    {PATH_COMPONENT}?([/\\]{PATH_COMPONENT}?)+
                   { return EprTypes.DIRECTIVE_PATH; }

    "]"            { yybegin (YYINITIAL); return EprTypes.DIRECTIVE_END; }
    {WHITE_SPACE}+ { return EprTypes.DIRECTIVE_WS; }
    {CRLF}         { yypushback (1); yybegin (YYINITIAL); }
    .              { return TokenType.BAD_CHARACTER; }
}
