package pl.sokrates.intellisowa.epr;

import com.intellij.lang.ASTNode;
import com.intellij.lang.Language;
import com.intellij.lang.ParserDefinition;
import com.intellij.lang.PsiParser;
import com.intellij.lexer.Lexer;
import com.intellij.openapi.project.Project;
import com.intellij.psi.FileViewProvider;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.TokenType;
import com.intellij.psi.tree.IFileElementType;
import com.intellij.psi.tree.TokenSet;
import org.jetbrains.annotations.NotNull;
import pl.sokrates.intellisowa.epr.parser.EprParser;
import pl.sokrates.intellisowa.epr.psi.EprFile;
import pl.sokrates.intellisowa.epr.psi.EprTypes;

public class EprParserDefinition implements ParserDefinition
{
    public static final TokenSet WHITE_SPACES = TokenSet.create (TokenType.WHITE_SPACE);
    public static final TokenSet COMMENTS     = TokenSet.create (EprTypes.COMMENT,
                                                                 EprTypes.COMMENT_START,
                                                                 EprTypes.COMMENT_END);
    public static final TokenSet STRINGS      = TokenSet.create (EprTypes.STRING_START,
                                                                 EprTypes.STRING_CONTENT,
                                                                 EprTypes.STRING_END);

    public static final IFileElementType FILE =
        new IFileElementType (Language.findInstance (EprLanguage.class));

    @NotNull
    @Override
    public Lexer createLexer (Project project)
    {
        return new EprLexerAdapter ();
    }

    @NotNull
    public TokenSet getWhitespaceTokens ()
    {
        return WHITE_SPACES;
    }

    @NotNull
    public TokenSet getCommentTokens ()
    {
        return COMMENTS;
    }

    @NotNull
    public TokenSet getStringLiteralElements ()
    {
        return STRINGS;
    }

    @NotNull
    public PsiParser createParser (final Project project)
    {
        return new EprParser ();
    }

    @Override
    public IFileElementType getFileNodeType ()
    {
        return FILE;
    }

    public PsiFile createFile (FileViewProvider viewProvider)
    {
        return new EprFile (viewProvider);
    }

    public SpaceRequirements spaceExistenceTypeBetweenTokens (ASTNode left, ASTNode right)
    {
        return SpaceRequirements.MAY;
    }

    @NotNull
    public PsiElement createElement (ASTNode node)
    {
        return EprTypes.Factory.createElement (node);
    }
}
