package pl.sokrates.intellisowa.epr.syntax;

import com.intellij.lexer.Lexer;
import com.intellij.openapi.editor.DefaultLanguageHighlighterColors;
import com.intellij.openapi.editor.HighlighterColors;
import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.openapi.fileTypes.SyntaxHighlighterBase;
import com.intellij.psi.TokenType;
import com.intellij.psi.tree.IElementType;
import com.intellij.psi.tree.TokenSet;
import org.jetbrains.annotations.NotNull;
import pl.sokrates.intellisowa.epr.EprLexerAdapter;
import pl.sokrates.intellisowa.epr.psi.EprTypes;
import pl.sokrates.intellisowa.leshlang.syntax.LeshSyntaxHighlighter;

import static com.intellij.openapi.editor.colors.TextAttributesKey.createTextAttributesKey;

public class EprSyntaxHighlighter extends SyntaxHighlighterBase
{
    public static final TextAttributesKey OPERATOR      =
        createTextAttributesKey ("EPR_OPERATOR", LeshSyntaxHighlighter.OPERATOR);
    public static final TextAttributesKey SEPARATOR     =
        createTextAttributesKey ("EPR_SEPARATOR", LeshSyntaxHighlighter.SEPARATOR);
    public static final TextAttributesKey KEYWORD       =
        createTextAttributesKey ("EPR_KEYWORD", LeshSyntaxHighlighter.KEYWORD);
    public static final TextAttributesKey DIRECTIVE     =
        createTextAttributesKey ("EPR_DIRECTIVE", LeshSyntaxHighlighter.DIRECTIVE);
    public static final TextAttributesKey STRING        =
        createTextAttributesKey ("EPR_STRING", LeshSyntaxHighlighter.STRING);
    public static final TextAttributesKey NUMBER        =
        createTextAttributesKey ("EPR_NUMBER", DefaultLanguageHighlighterColors.NUMBER);
    public static final TextAttributesKey COMMENT       =
        createTextAttributesKey ("EPR_COMMENT", LeshSyntaxHighlighter.COMMENT);
    public static final TextAttributesKey BAD_CHARACTER =
        createTextAttributesKey ("EPR_BAD_CHARACTER", HighlighterColors.BAD_CHARACTER);

    public static final TokenSet OPERATOR_SET = TokenSet.create (
        EprTypes.OP_PLUS,
        EprTypes.OP_MINUS,
        EprTypes.OP_SLASH);

    public static final TokenSet SEPARATOR_SET = TokenSet.create (
        EprTypes.OP_LEFTPAREN,
        EprTypes.OP_RIGHTPAREN,
        EprTypes.OP_COMMA);

    public static final TokenSet KEYWORD_SET = TokenSet.create (
        EprTypes.KW_BEGIN,
        EprTypes.KW_END,
        EprTypes.KW_LABEL,
        EprTypes.KW_NO,
        EprTypes.KW_TEXT,
        EprTypes.KW_PANEL,
        EprTypes.KW_FIELD,
        EprTypes.KW_DICTIONARY,
        EprTypes.KW_DATE,
        EprTypes.KW_MONTH,
        EprTypes.KW_NUMBER,
        EprTypes.KW_SPIN,
        EprTypes.KW_CHECK,
        EprTypes.KW_RADIO,
        EprTypes.KW_COMBO,
        EprTypes.KW_MEMO,
        EprTypes.KW_MARKERS,
        EprTypes.KW_BUTTON,
        EprTypes.KW_SPEED,
        EprTypes.KW_MENU,
        EprTypes.KW_LIST,
        EprTypes.KW_TABS,
        EprTypes.KW_DYNAMIC,
        EprTypes.KW_FORM,
        EprTypes.KW_FRAME,
        EprTypes.KW_SCROLL,
        EprTypes.KW_ON,
        EprTypes.KW_OF,
        EprTypes.KW_MASK,
        EprTypes.KW_LIMIT,
        EprTypes.KW_MULTILINE,
        EprTypes.KW_FLOAT,
        EprTypes.KW_DEFAULT);

    public static final TokenSet DIRECTIVE_SET = TokenSet.create (
        EprTypes.DIRECTIVE_START,
        EprTypes.DIRECTIVE_PLIK,
        EprTypes.DIRECTIVE_SKRYPT,
        EprTypes.DIRECTIVE_PATH,
        EprTypes.DIRECTIVE_VALUE,
        EprTypes.DIRECTIVE_END,
        EprTypes.PARAMETER_EXPANSION,
        EprTypes.PARAMETER_EXPANSION_IN_STRING,
        EprTypes.MACRO_EXPANSION,
        EprTypes.MACRO_EXPANSION_IN_STRING);

    public static final TokenSet STRING_SET = TokenSet.create (
        EprTypes.STRING_START,
        EprTypes.STRING_CONTENT,
        EprTypes.STRING_END);

    public static final TokenSet COMMENT_SET = TokenSet.create (
        EprTypes.COMMENT_START,
        EprTypes.COMMENT,
        EprTypes.COMMENT_END);

    private static final TextAttributesKey[] BAD_CHAR_KEYS  = new TextAttributesKey[] { BAD_CHARACTER };
    private static final TextAttributesKey[] OPERATOR_KEYS  = new TextAttributesKey[] { OPERATOR };
    private static final TextAttributesKey[] SEPARATOR_KEYS = new TextAttributesKey[] { SEPARATOR };
    private static final TextAttributesKey[] KEYWORD_KEYS   = new TextAttributesKey[] { KEYWORD };
    private static final TextAttributesKey[] DIRECTIVE_KEYS = new TextAttributesKey[] { DIRECTIVE };
    private static final TextAttributesKey[] STRING_KEYS    = new TextAttributesKey[] { STRING };
    private static final TextAttributesKey[] NUMBER_KEYS    = new TextAttributesKey[] { NUMBER };
    private static final TextAttributesKey[] COMMENT_KEYS   = new TextAttributesKey[] { COMMENT };
    private static final TextAttributesKey[] EMPTY_KEYS     = new TextAttributesKey[0];

    @NotNull
    @Override
    public Lexer getHighlightingLexer ()
    {
        return new EprLexerAdapter ();
    }

    @NotNull
    @Override
    public TextAttributesKey[] getTokenHighlights (IElementType tokenType)
    {
        if (tokenType.equals (EprTypes.INT_LITERAL)) return NUMBER_KEYS;
        if (tokenType.equals (TokenType.BAD_CHARACTER)) return BAD_CHAR_KEYS;
        if (KEYWORD_SET.contains (tokenType)) return KEYWORD_KEYS;
        if (SEPARATOR_SET.contains (tokenType)) return SEPARATOR_KEYS;
        if (OPERATOR_SET.contains (tokenType)) return OPERATOR_KEYS;
        if (DIRECTIVE_SET.contains (tokenType)) return DIRECTIVE_KEYS;
        if (STRING_SET.contains (tokenType)) return STRING_KEYS;
        if (COMMENT_SET.contains (tokenType)) return COMMENT_KEYS;
        return EMPTY_KEYS;
    }
}
