package pl.sokrates.intellisowa.epr.syntax;

import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.openapi.fileTypes.SyntaxHighlighter;
import com.intellij.openapi.options.colors.AttributesDescriptor;
import com.intellij.openapi.options.colors.ColorDescriptor;
import com.intellij.openapi.options.colors.ColorSettingsPage;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import pl.sokrates.intellisowa.SokratesIcons;

import javax.swing.*;
import java.util.Map;

public class EprColorSettingsPage implements ColorSettingsPage
{
    private static final AttributesDescriptor[] DESCRIPTORS = new AttributesDescriptor[] {
        new AttributesDescriptor ("Keyword", EprSyntaxHighlighter.KEYWORD),
        new AttributesDescriptor ("String literal", EprSyntaxHighlighter.STRING),
        new AttributesDescriptor ("Number literal", EprSyntaxHighlighter.NUMBER),
        new AttributesDescriptor ("Comment", EprSyntaxHighlighter.COMMENT),
        new AttributesDescriptor ("Directive", EprSyntaxHighlighter.DIRECTIVE),
        new AttributesDescriptor ("Operator", EprSyntaxHighlighter.OPERATOR),
        new AttributesDescriptor ("Separator", EprSyntaxHighlighter.SEPARATOR),
        };

    @Nullable
    @Override
    public Icon getIcon ()
    {
        return SokratesIcons.EPR;
    }

    @NotNull
    @Override
    public SyntaxHighlighter getHighlighter ()
    {
        return new EprSyntaxHighlighter ();
    }

    @NotNull
    @Override
    public String getDemoText ()
    {
        return "{plik 2005-U-007.EPR - dynamiczny formularz MARC-21/2005 + zasób po nowemu}\n" +
                   "{obsługa 007}\n" +
                   "\n" +
                   "BEGIN\n" +
                   "TABS ON 0,0,630,468\n" +
                   "'opis bibliograficzny'\n" +
                   "BEGIN\n" +
                   "FORM '#.4' MASK 'Dodaj pole' SPEED 'pole.bmp' ON 0,0,0,0\n" +
                   "   BEGIN\n" +
                   "   LABEL 'Etykieta:' ON 2,2\n" +
                   "   FIELD '#.5' MASK '999;0; ' ON 2,20,30\n" +
                   "   BUTTON '#.5' MASK 'Dodaj' ON 50,4,40,40\n" +
                   "   END\n" +
                   "\n" +
                   "MENU\n" +
                   "'|Pojedyncze pole|'\n" +
                   " +'|Pole danych identyfikacyjnych [0XX]|'";
    }

    @Nullable
    @Override
    public Map<String, TextAttributesKey> getAdditionalHighlightingTagToDescriptorMap ()
    {
        return null;
    }

    @NotNull
    @Override
    public AttributesDescriptor[] getAttributeDescriptors ()
    {
        return DESCRIPTORS;
    }

    @NotNull
    @Override
    public ColorDescriptor[] getColorDescriptors ()
    {
        return ColorDescriptor.EMPTY_ARRAY;
    }

    @NotNull
    @Override
    public String getDisplayName ()
    {
        return "SOWA: EPR";
    }
}
