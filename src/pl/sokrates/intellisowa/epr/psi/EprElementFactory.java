package pl.sokrates.intellisowa.epr.psi;

import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiFileFactory;
import pl.sokrates.intellisowa.epr.EprFileType;

public class EprElementFactory
{
    public static EprInclude createInclude (Project project, String name)
    {
        return createFile (project, "\n[PLIK " + name + "]\n").findChildByClass (EprInclude.class);
    }

    public static EprFile createFile (Project project, String text)
    {
        String name = "dummy.ppr";
        return (EprFile) PsiFileFactory.getInstance (project).
                                                                 createFileFromText (name, EprFileType.INSTANCE, text);
    }
}
