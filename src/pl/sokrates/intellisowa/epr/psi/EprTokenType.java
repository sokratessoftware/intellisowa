package pl.sokrates.intellisowa.epr.psi;

import com.intellij.psi.tree.IElementType;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;
import pl.sokrates.intellisowa.epr.EprLanguage;

public class EprTokenType extends IElementType
{
    public EprTokenType (@NotNull @NonNls String debugName)
    {
        super (debugName, EprLanguage.INSTANCE);
    }

    @Override
    public String toString ()
    {
        return "EprTokenType." + super.toString ();
    }
}
