package pl.sokrates.intellisowa.epr.psi.impl;

import com.intellij.psi.PsiElement;
import com.intellij.util.IncorrectOperationException;
import pl.sokrates.intellisowa.epr.psi.EprElementFactory;
import pl.sokrates.intellisowa.epr.psi.EprIncludePath;

import java.io.File;

public class EprPsiImplUtil
{
    public static EprIncludePath handleRename (EprIncludePath element, String newPath)
    {
        newPath = newPath.replace (File.separator, "\\");
        PsiElement newElement = EprElementFactory.createInclude (element.getProject (), newPath);
        if (newElement == null)
            throw new IncorrectOperationException ("Invalid name for file");
        for (PsiElement el : newElement.getChildren ()) {
            if (el instanceof EprIncludePath) {
                element.replace (el);
                return (EprIncludePath) el;
            }
        }
        throw new IncorrectOperationException ("Oops?");
    }
}

