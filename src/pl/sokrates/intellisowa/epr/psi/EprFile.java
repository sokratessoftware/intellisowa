package pl.sokrates.intellisowa.epr.psi;

import com.intellij.extapi.psi.PsiFileBase;
import com.intellij.openapi.fileTypes.FileType;
import com.intellij.psi.FileViewProvider;
import org.jetbrains.annotations.NotNull;
import pl.sokrates.intellisowa.epr.EprFileType;
import pl.sokrates.intellisowa.epr.EprLanguage;

import javax.swing.*;

public class EprFile extends PsiFileBase
{
    public EprFile (@NotNull FileViewProvider viewProvider)
    {
        super (viewProvider, EprLanguage.INSTANCE);
    }

    @NotNull
    @Override
    public FileType getFileType ()
    {
        return EprFileType.INSTANCE;
    }

    @Override
    public String toString ()
    {
        return "EPR(" + getName () + ")";
    }

    @Override
    public Icon getIcon (int flags)
    {
        return super.getIcon (flags);
    }
}