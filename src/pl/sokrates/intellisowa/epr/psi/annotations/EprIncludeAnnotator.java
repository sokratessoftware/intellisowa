package pl.sokrates.intellisowa.epr.psi.annotations;

import com.intellij.lang.annotation.AnnotationHolder;
import com.intellij.lang.annotation.Annotator;
import com.intellij.lang.annotation.HighlightSeverity;
import com.intellij.openapi.fileTypes.FileType;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.util.PsiTreeUtil;
import org.jetbrains.annotations.NotNull;
import pl.sokrates.intellisowa.common.psi.GenericInclude;
import pl.sokrates.intellisowa.common.psi.GenericIncludePath;
import pl.sokrates.intellisowa.common.psi.refs.GenericIncludeRef;
import pl.sokrates.intellisowa.epr.EprFileType;
import pl.sokrates.intellisowa.epr.psi.EprTypes;
import pl.sokrates.intellisowa.leshlang.LeshFileType;

public class EprIncludeAnnotator implements Annotator
{
    @Override
    public void annotate (@NotNull PsiElement psiElement, @NotNull AnnotationHolder annotationHolder)
    {
        if (psiElement instanceof GenericIncludePath) {
            GenericIncludeRef ref = (GenericIncludeRef) psiElement.getReference ();
            if (ref == null) return;

            PsiElement target = ref.resolve ();
            if (target == null) return;

            FileType expectedType = EprFileType.INSTANCE;
            GenericInclude parent = PsiTreeUtil.getParentOfType (psiElement, GenericInclude.class);
            if (parent.getNode ().findChildByType (EprTypes.DIRECTIVE_SKRYPT) != null)
                expectedType = LeshFileType.INSTANCE;
            
            FileType actualType = ((PsiFile) target).getFileType ();
            if (actualType != expectedType)
                annotationHolder
                    .newAnnotation(HighlightSeverity.ERROR, "Wskazany plik to: " + actualType.getName () + ", oczekiwany rodzaj pliku: " + expectedType.getName () + ".")
                    .range(psiElement)
                    .create();
        }
    }
}
