package pl.sokrates.intellisowa.epr.psi;

import com.intellij.psi.tree.IElementType;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;
import pl.sokrates.intellisowa.epr.EprLanguage;

public class EprElementType extends IElementType
{
    public EprElementType (@NotNull @NonNls String debugName)
    {
        super (debugName, EprLanguage.INSTANCE);
    }
}
