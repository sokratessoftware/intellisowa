package pl.sokrates.intellisowa.epr.completion;

import com.intellij.codeInsight.completion.CompletionContributor;
import com.intellij.codeInsight.completion.CompletionType;
import pl.sokrates.intellisowa.common.completion.ErrorCompletionProvider;
import pl.sokrates.intellisowa.common.completion.IncludeCompletionProvider;
import pl.sokrates.intellisowa.epr.EprFileType;
import pl.sokrates.intellisowa.epr.psi.EprTokenType;
import pl.sokrates.intellisowa.epr.psi.EprTypes;
import pl.sokrates.intellisowa.leshlang.LeshFileType;

import static com.intellij.patterns.PlatformPatterns.psiElement;

public class EprCompletionContributor extends CompletionContributor
{
    public EprCompletionContributor ()
    {
        extend (CompletionType.BASIC,
                psiElement (EprTypes.DIRECTIVE_PATH).afterLeafSkipping (psiElement (EprTypes.DIRECTIVE_WS),
                                                                        psiElement (EprTypes.DIRECTIVE_SKRYPT)),
                new IncludeCompletionProvider (LeshFileType.INSTANCE));

        extend (CompletionType.BASIC,
                psiElement (EprTypes.DIRECTIVE_PATH).afterLeafSkipping (psiElement (EprTypes.DIRECTIVE_WS),
                                                                        psiElement (EprTypes.DIRECTIVE_PLIK)),
                new IncludeCompletionProvider (EprFileType.INSTANCE));
    
        extend (CompletionType.BASIC,
                psiElement (),
                new ErrorCompletionProvider (EprTokenType.class.getSimpleName ()));
        
        //extend (CompletionType.BASIC, psiElement (), new PrintCompletionProvider ());
    }
}
