package pl.sokrates.intellisowa.epr;

import com.intellij.lang.BracePair;
import com.intellij.lang.PairedBraceMatcher;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.tree.IElementType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import pl.sokrates.intellisowa.epr.psi.EprTypes;

public class EprBraceMatcher implements PairedBraceMatcher
{
    static BracePair[] PAIRS = new BracePair[] {
        new BracePair (EprTypes.KW_BEGIN, EprTypes.KW_END, true),
        new BracePair (EprTypes.KW_TABS, EprTypes.KW_END, true),
        new BracePair (EprTypes.KW_DYNAMIC, EprTypes.KW_END, true),
        new BracePair (EprTypes.COMMENT_START, EprTypes.COMMENT_END, true),
        new BracePair (EprTypes.DIRECTIVE_START, EprTypes.DIRECTIVE_END, true),
        new BracePair (EprTypes.OP_LEFTPAREN, EprTypes.OP_RIGHTPAREN, false)
    };

    @Override
    public boolean isPairedBracesAllowedBeforeType (@NotNull IElementType iElementType,
                                                    @Nullable IElementType iElementType1)
    {
        return true;
    }

    @Override
    public BracePair[] getPairs ()
    {
        return PAIRS;
    }

    @Override
    public int getCodeConstructStart (PsiFile psiFile, int i)
    {
        PsiElement element = psiFile.findElementAt (i);
        return element.getTextRange ().getStartOffset ();
    }
}
