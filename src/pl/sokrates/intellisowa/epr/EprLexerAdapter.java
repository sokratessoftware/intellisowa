package pl.sokrates.intellisowa.epr;

import com.intellij.lexer.FlexAdapter;

import java.io.Reader;

public class EprLexerAdapter extends FlexAdapter
{
    public EprLexerAdapter ()
    {
        super (new EprLexer (null));
    }
}
