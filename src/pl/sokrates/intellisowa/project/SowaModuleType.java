package pl.sokrates.intellisowa.project;

import com.intellij.ide.util.projectWizard.ModuleWizardStep;
import com.intellij.ide.util.projectWizard.WizardContext;
import com.intellij.openapi.module.ModuleType;
import com.intellij.openapi.module.ModuleTypeManager;
import com.intellij.openapi.roots.ui.configuration.ModulesProvider;
import org.jetbrains.annotations.NotNull;
import pl.sokrates.intellisowa.SokratesIcons;

import javax.swing.*;

public class SowaModuleType extends ModuleType<SowaModuleBuilder>
{
    public static final String ID = "SOWA2_MODULE_TYPE";

    public SowaModuleType ()
    {
        super (ID);
    }

    public static SowaModuleType getInstance ()
    {
        return (SowaModuleType) ModuleTypeManager.getInstance ().findByID (ID);
    }

    @NotNull
    @Override
    public SowaModuleBuilder createModuleBuilder ()
    {
        return new SowaModuleBuilder ();
    }

    @NotNull
    @Override
    public String getName ()
    {
        return "Repozytorium skryptów SOWY (2/SQL)";
    }

    @NotNull
    @Override
    public String getDescription ()
    {
        return "Repozytorium skryptów SOWY (2/SQL)";
    }

    @Override
    public Icon getNodeIcon (@Deprecated boolean b)
    {
        return SokratesIcons.LESH_LANG;
    }

    @NotNull
    @Override
    public ModuleWizardStep[] createWizardSteps (@NotNull WizardContext wizardContext,
                                                 @NotNull SowaModuleBuilder moduleBuilder,
                                                 @NotNull ModulesProvider modulesProvider)
    {
        return super.createWizardSteps (wizardContext, moduleBuilder, modulesProvider);
    }
}
