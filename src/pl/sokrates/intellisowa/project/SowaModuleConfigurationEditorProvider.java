package pl.sokrates.intellisowa.project;

import com.intellij.openapi.module.Module;
import com.intellij.openapi.module.ModuleConfigurationEditor;
import com.intellij.openapi.module.ModuleType;
import com.intellij.openapi.roots.ui.configuration.DefaultModuleConfigurationEditorFactory;
import com.intellij.openapi.roots.ui.configuration.ModuleConfigurationEditorProvider;
import com.intellij.openapi.roots.ui.configuration.ModuleConfigurationState;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

// skopiowane z lua-for-idea
public class SowaModuleConfigurationEditorProvider implements ModuleConfigurationEditorProvider
{
    public ModuleConfigurationEditor[] createEditors (@NotNull final ModuleConfigurationState state)
    {
        final Module module = state.getRootModel ().getModule ();
        if (! ModuleType.get (module).getId ().equals (SowaModuleType.ID)) return ModuleConfigurationEditor.EMPTY;
        final DefaultModuleConfigurationEditorFactory editorFactory = DefaultModuleConfigurationEditorFactory.getInstance ();
        final List<ModuleConfigurationEditor>         editors       = new ArrayList<> ();

        editors.add (editorFactory.createModuleContentRootsEditor (state));

        return editors.toArray (new ModuleConfigurationEditor[editors.size ()]);
    }
}
