package pl.sokrates.intellisowa.project;

import com.intellij.ide.util.projectWizard.ModuleBuilder;
import com.intellij.ide.util.projectWizard.ModuleWizardStep;
import com.intellij.ide.util.projectWizard.SourcePathsBuilder;
import com.intellij.ide.util.projectWizard.WizardContext;
import com.intellij.openapi.Disposable;
import com.intellij.openapi.module.ModuleType;
import com.intellij.openapi.options.ConfigurationException;
import com.intellij.openapi.roots.ContentEntry;
import com.intellij.openapi.roots.ModifiableRootModel;
import com.intellij.openapi.util.Pair;
import com.intellij.openapi.vfs.VirtualFile;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class SowaModuleBuilder extends ModuleBuilder implements SourcePathsBuilder
{
    private List<Pair<String, String>> mySourcePaths;

    @Override
    public void setupRootModel (ModifiableRootModel model) throws ConfigurationException
    {
        // wszystkie katalogi pod modułem są domyślnie źródłowymi
        ContentEntry contentEntry = doAddContentEntry (model);
        if (contentEntry != null) {
            final VirtualFile file = contentEntry.getFile ();
            if (file != null && file.isDirectory ())
                contentEntry.addSourceFolder (file, false);
        }
    }

    @Override
    public ModuleType getModuleType ()
    {
        return SowaModuleType.getInstance ();
    }

    @Nullable
    @Override
    public ModuleWizardStep getCustomOptionsStep (WizardContext context, Disposable parentDisposable)
    {
        return null;
    }

    // przekopiowane z JavaModuleBuilder
    public List<Pair<String, String>> getSourcePaths ()
    {
        if (this.mySourcePaths == null) {
            ArrayList paths = new ArrayList ();
            String    path  = this.getContentEntryPath () + File.separator + "src";
            (new File (path)).mkdirs ();
            paths.add (Pair.create (path, ""));
            return paths;
        }
        else {
            return this.mySourcePaths;
        }
    }

    public void setSourcePaths (List<Pair<String, String>> sourcePaths)
    {
        this.mySourcePaths = sourcePaths != null ? new ArrayList (sourcePaths) : null;
    }

    public void addSourcePath (Pair<String, String> sourcePathInfo)
    {
        if (this.mySourcePaths == null) {
            this.mySourcePaths = new ArrayList ();
        }

        this.mySourcePaths.add (sourcePathInfo);
    }
}
