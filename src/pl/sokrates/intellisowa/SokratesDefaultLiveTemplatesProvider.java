package pl.sokrates.intellisowa;

import com.intellij.codeInsight.template.impl.DefaultLiveTemplatesProvider;
import org.jetbrains.annotations.Nullable;

public class SokratesDefaultLiveTemplatesProvider implements DefaultLiveTemplatesProvider
{
    @Override
    public String[] getDefaultLiveTemplateFiles ()
    {
        return new String[] { "liveTemplates/LeshLang" };
    }
    
    @Nullable
    @Override
    public String[] getHiddenLiveTemplateFiles ()
    {
        return new String[0];
    }
}
