package pl.sokrates.intellisowa.common;

import com.intellij.extapi.psi.PsiFileBase;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.openapi.vfs.VirtualFileWithId;
import com.intellij.psi.PsiFile;
import com.intellij.psi.util.PsiTreeUtil;
import org.jetbrains.annotations.NotNull;
import pl.sokrates.intellisowa.common.psi.GenericInclude;
import pl.sokrates.intellisowa.common.psi.GenericIncludePath;
import pl.sokrates.intellisowa.common.psi.refs.GenericIncludeRef;

import java.util.BitSet;
import java.util.Set;

public class IncludeUtil
{
    public static void visitIncludesById (@NotNull final PsiFileBase file,
                                          @NotNull BitSet visitedFiles)
    {
        if (visitedFiles.isEmpty ())
            visitedFiles.set (((VirtualFileWithId) file.getVirtualFile ()).getId ());
        
        for (GenericInclude include : PsiTreeUtil.collectElementsOfType (file, GenericInclude.class)) {
            GenericIncludePath includePath = include.getIncludePath ();
            if (includePath == null) continue;
        
            PsiFile psiFile = ((GenericIncludeRef) includePath.getReference ()).resolve ();
            if (psiFile != null && psiFile instanceof PsiFileBase) {
                VirtualFileWithId includeFile = (VirtualFileWithId) psiFile.getVirtualFile ();
                int id = includeFile.getId ();
                if (id >= 0 && !visitedFiles.get (id)) {
                    visitedFiles.set (id);
                    visitIncludesById ((PsiFileBase) psiFile, visitedFiles);
                }
            }
        }
    }
    
    public static void visitIncludesByPsi (@NotNull final PsiFileBase file,
                                           @NotNull Set<PsiFileBase> visitedFiles)
    {
        if (visitedFiles.isEmpty ())
            visitedFiles.add (file);
        
        for (GenericInclude include : PsiTreeUtil.collectElementsOfType (file, GenericInclude.class)) {
            GenericIncludePath includePath = include.getIncludePath ();
            if (includePath == null) continue;
        
            PsiFile psiFile = ((GenericIncludeRef) includePath.getReference ()).resolve ();
            if (psiFile != null && psiFile instanceof PsiFileBase) {
                VirtualFile includeFile = psiFile.getVirtualFile ();
                PsiFileBase psiFile2 = (PsiFileBase) psiFile;
                if (!visitedFiles.contains (psiFile2)) {
                    visitedFiles.add (psiFile2);
                    visitIncludesByPsi (psiFile2, visitedFiles);
                }
            }
        }
    }
    
    public static void visitIncludes (@NotNull final PsiFileBase file,
                                      @NotNull Set<VirtualFile> visitedFiles)
    {
        if (visitedFiles.isEmpty ())
            visitedFiles.add (file.getVirtualFile ());
        
        for (GenericInclude include : PsiTreeUtil.collectElementsOfType (file, GenericInclude.class)) {
            GenericIncludePath includePath = include.getIncludePath ();
            if (includePath == null) continue;
            
            PsiFile psiFile = ((GenericIncludeRef) includePath.getReference ()).resolve ();
            if (psiFile != null && psiFile instanceof PsiFileBase) {
                VirtualFile includeFile = psiFile.getVirtualFile ();
                if (!visitedFiles.contains (includeFile)) {
                    visitedFiles.add (includeFile);
                    visitIncludes ((PsiFileBase) psiFile, visitedFiles);
                }
            }
        }
    }
}
