package pl.sokrates.intellisowa.common.codeStyle;

import com.intellij.psi.codeStyle.CodeStyleSettings;
import com.intellij.psi.codeStyle.CustomCodeStyleSettings;

public class SowaCodeStyleSettings extends CustomCodeStyleSettings
{
    public SowaCodeStyleSettings (CodeStyleSettings settings)
    {
        super ("SowaCodeStyleSettings", settings);
    }
}
