package pl.sokrates.intellisowa.common.completion;

import com.intellij.codeInsight.AutoPopupController;
import com.intellij.codeInsight.completion.*;
import com.intellij.codeInsight.lookup.LookupElement;
import com.intellij.codeInsight.lookup.LookupElementPresentation;
import com.intellij.icons.AllIcons;
import com.intellij.openapi.fileTypes.FileType;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.roots.ContentIterator;
import com.intellij.openapi.roots.ProjectRootManager;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.util.ProcessingContext;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import pl.sokrates.intellisowa.SokratesIcons;

import javax.swing.*;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

public class IncludeCompletionProvider extends CompletionProvider<CompletionParameters>
{
    FileType fileType;

    public IncludeCompletionProvider (FileType fileType)
    {
        super ();
        this.fileType = fileType;
    }

    static final String PLACEHOLDER = "IntellijIdeaRulezzz";

    @Override
    protected void addCompletions (@NotNull CompletionParameters completionParameters,
                                   ProcessingContext processingContext,
                                   @NotNull final CompletionResultSet completionResultSet)
    {
        PsiFile file    = completionParameters.getOriginalFile ();
        Project project = file.getProject ();

        VirtualFile vFile = file.getOriginalFile ().getVirtualFile ();
        if (vFile == null) return;

        VirtualFile _dir = vFile;
        while (!_dir.isDirectory ()) _dir = _dir.getParent ();
        final VirtualFile dir = _dir;

        PsiElement element = completionParameters.getPosition ();
        String     text    = element.getParent ().getText ();
        String     prefix  = text.substring (0, text.length () - PLACEHOLDER.length ());

        final char separator, otherSeparator;
        if (prefix.indexOf ('/') >= 0) {
            separator = '/';
            otherSeparator = '\\';
        }
        else {
            separator = '\\';
            otherSeparator = '/';
        }

        if (completionParameters.isExtendedCompletion ()) {   // wyszukiwanie na liście wszystkich plików w projekcie
            ProjectRootManager.getInstance (project).getFileIndex ().iterateContent (new ContentIterator ()
            {
                @Override
                public boolean processFile (VirtualFile virtualFile)
                {
                    if (virtualFile.getFileType ().equals (fileType)) {
                        Path   path  = Paths.get (dir.getPath ()).relativize (Paths.get (virtualFile.getPath ()));
                        String value = path.toString ().replace (otherSeparator, separator);

                        int distance = 0, i = 0, L = value.length ();
                        while (i < L && value.charAt (i) == '.') {
                            i += 3;
                            distance -= 100;
                        }
                        for (; i < L; i++)
                            if (value.charAt (i) == separator)
                                distance -= 1;

                        completionResultSet.consume (PrioritizedLookupElement.withPriority (new PathElement (fileType.getIcon (), value),
                                                                                            distance));
                    }
                    return true;
                }
            });
        }
        else {   // wyszukiwanie w podanym katalogu
            prefix = prefix.replace ('\\', '/');
            String prefixDir  = prefix.replaceFirst ("[^/]+$", "");
            String prefixName = prefix.substring (prefixDir.length (), prefix.length ());

            if (prefixName.equals ("..")) {
                prefixDir = "../";
                prefixName = "";
            }

            VirtualFile searchDir = dir.findFileByRelativePath (prefixDir);
            if (searchDir == null) searchDir = dir.findFileByRelativePath (prefixDir.toLowerCase ());
            if (searchDir == null) searchDir = dir.findFileByRelativePath (prefixDir.toUpperCase ());
            if (searchDir == null) {
                //System.out.println ("ICP: prefix=" + prefix + " prefixDir=" + prefixDir + " prefixName=" + prefixName + " dir=" + dir + " searchDir=NULL");
                return;
            }

            Path   path  = Paths.get (dir.getPath ()).relativize (Paths.get (searchDir.getPath ()));
            String value = path.toString ().replace (otherSeparator, separator);
            if (value.length () > 0 && !value.endsWith ("" + separator))
                value += separator;

            //System.out.println ("ICP: prefix=" + prefix + " prefixDir=" + prefixDir + " prefixName=" + prefixName + " dir=" + dir + " searchDir=" + searchDir + " value=" + value);

            for (VirtualFile virtualFile : searchDir.getChildren ()) {
                if (!virtualFile.getName ().startsWith (prefixName)) continue;

                if (virtualFile.getFileType () == fileType)
                    completionResultSet.consume (new PathElement (fileType.getIcon (), value + virtualFile.getName ()));
                else if (virtualFile.isDirectory ())
                    completionResultSet.consume (new PathElement (AllIcons.Nodes.Folder, value + virtualFile.getName () + separator));
            }
        }
    }

    static class PathElement extends LookupElement
    {
        final String path;
        final Icon icon;

        public PathElement (@Nullable Icon icon, @NotNull String path)
        {
            super ();
            this.path = path;
            this.icon = icon;
        }

        @NotNull
        @Override
        public String getLookupString ()
        {
            return path;
        }

        @Override
        public void renderElement (LookupElementPresentation presentation)
        {
            super.renderElement (presentation);
            presentation.setIcon (icon);
        }

        @Override
        public void handleInsert (InsertionContext context)
        {
            super.handleInsert (context);
            if (path.endsWith ("/") || path.endsWith ("\\")) {
                AutoPopupController.getInstance (context.getProject ()).scheduleAutoPopup (context.getEditor ());
            }
        }
    }
}
