package pl.sokrates.intellisowa.common.completion;

import com.intellij.codeInsight.completion.CompletionParameters;
import com.intellij.codeInsight.completion.CompletionProvider;
import com.intellij.codeInsight.completion.CompletionResultSet;
import com.intellij.codeInsight.lookup.LookupElementBuilder;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiErrorElement;
import com.intellij.util.ProcessingContext;
import org.jetbrains.annotations.NotNull;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ErrorCompletionProvider extends CompletionProvider<CompletionParameters>
{
    final Pattern pattern;
    final Set<String> blacklist;

    public ErrorCompletionProvider (String tokenClassName, String... blacklist)
    {
        super ();
        pattern = Pattern.compile (tokenClassName + "\\.KW_(\\w+)");
        this.blacklist = new HashSet<String> ();
        Collections.addAll (this.blacklist, blacklist);
    }

    @Override
    protected void addCompletions (@NotNull CompletionParameters completionParameters,
                                   ProcessingContext processingContext,
                                   @NotNull CompletionResultSet completionResultSet)
    {
        PsiElement element = completionParameters.getPosition ();
        if (!(element instanceof PsiErrorElement))
            element = element.getParent ();
        if (!(element instanceof PsiErrorElement))
            return;

        String msg = ((PsiErrorElement) element).getErrorDescription ();

        Matcher m = pattern.matcher (msg);
        while (m.find ()) {
            String keyword = m.group (1).toLowerCase ();
            if (! blacklist.contains (keyword))
                completionResultSet.consume (LookupElementBuilder.create (keyword).withBoldness (true));
        }
    }
}
