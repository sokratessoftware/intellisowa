package pl.sokrates.intellisowa.common.completion;

import com.intellij.codeInsight.completion.AddSpaceInsertHandler;
import com.intellij.codeInsight.completion.CompletionParameters;
import com.intellij.codeInsight.completion.CompletionProvider;
import com.intellij.codeInsight.completion.CompletionResultSet;
import com.intellij.codeInsight.lookup.LookupElement;
import com.intellij.codeInsight.lookup.LookupElementBuilder;
import com.intellij.util.ProcessingContext;
import org.jetbrains.annotations.NotNull;

public class KeywordCompletionProvider extends CompletionProvider<CompletionParameters>
{
    final LookupElement[] lookups;

    public KeywordCompletionProvider (boolean addSpace, String... keywords)
    {
        super ();

        lookups = new LookupElement[keywords.length];

        for (int i = 0; i < keywords.length; i++) {
            LookupElementBuilder x = LookupElementBuilder.create (keywords[i]).withBoldness (true);
            if (addSpace)
                x = x.withInsertHandler (AddSpaceInsertHandler.INSTANCE);
            lookups[i] = x;

        }
    }

    @Override
    protected void addCompletions (@NotNull CompletionParameters completionParameters,
                                   ProcessingContext processingContext,
                                   @NotNull CompletionResultSet completionResultSet)
    {
        for (LookupElement lookup : lookups)
            completionResultSet.consume (lookup);
    }
}
