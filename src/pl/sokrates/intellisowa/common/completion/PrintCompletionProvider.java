package pl.sokrates.intellisowa.common.completion;

import com.intellij.codeInsight.completion.CompletionParameters;
import com.intellij.codeInsight.completion.CompletionProvider;
import com.intellij.codeInsight.completion.CompletionResultSet;
import com.intellij.psi.PsiElement;
import com.intellij.util.ProcessingContext;
import org.jetbrains.annotations.NotNull;

public class PrintCompletionProvider extends CompletionProvider<CompletionParameters>
{
    public PrintCompletionProvider ()
    {
        super ();
    }

    @Override
    protected void addCompletions (@NotNull CompletionParameters completionParameters,
                                   ProcessingContext processingContext,
                                   @NotNull CompletionResultSet completionResultSet)
    {
        PsiElement element = completionParameters.getPosition ().getOriginalElement ();
        String     pfx     = "";
        while (element != null) {
            System.out.print (pfx);
            System.out.print (element);
            String text = element.getText ();
            if (text.indexOf ('\n') < 0) {
                System.out.print (" \"");
                System.out.print (text);
                System.out.print ("\"");
            }
            pfx += "  ";
            element = element.getParent ();
            System.out.println ("");
        }
    }
}
