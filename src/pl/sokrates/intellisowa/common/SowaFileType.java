package pl.sokrates.intellisowa.common;

import com.intellij.lang.Language;
import com.intellij.openapi.fileTypes.LanguageFileType;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.openapi.vfs.encoding.EncodingManager;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.nio.charset.Charset;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class SowaFileType extends LanguageFileType
{
    protected SowaFileType (@NotNull Language language) { super (language); }
    
    public static final String  GLORIOUS_ENCODING = "windows-1250";
    public static final Charset GLORIOUS_CHARSET  = Charset.forName(GLORIOUS_ENCODING);
    public static final Pattern ENCODING_PATTERN  = Pattern.compile ("^\\s*(?:\\{[^{]*?\\}\\s*)*?\\{\\s*(?:en)?coding\\s*[:=]\\s*([^\n]+?)\\s*\\}",
                                                                     Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
    public static final Charset LATIN1            = Charset.forName ("ISO-8859-1");

    @Override
    public String getCharset (@NotNull VirtualFile file, @NotNull byte[] content)
    {
        VirtualFile iter    = file;
        Charset     charset = null;
        while (iter != null) {
            charset = EncodingManager.getInstance ().getEncoding (iter, false);
            if (charset != null)
                return charset.name ();
            
            iter = iter.getParent ();
        }
        
        charset = extractCharsetFromFileContent (null, file, (CharSequence) new String (content, 0, Math.min (content.length, 4096), LATIN1));
        if (charset != null) {
            return charset.name ();
        }
        
        return null;
    }

    @Override
    public Charset extractCharsetFromFileContent (@Nullable Project project,
                                                  @Nullable VirtualFile file,
                                                  @NotNull  CharSequence content)
    {
        Matcher m = ENCODING_PATTERN.matcher (content).region (0, Math.min (content.length (), 4096));
        if (m.find (0))
            try {
                return Charset.forName (m.group (1));
            }
            catch (IllegalArgumentException ex) {}
        
        return null;
    }
}
