package pl.sokrates.intellisowa.common.psi.refs;

import com.intellij.codeInsight.lookup.LookupElement;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.*;
import com.intellij.psi.impl.source.resolve.reference.impl.providers.PsiFileReference;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import pl.sokrates.intellisowa.common.psi.GenericIncludePath;

public class GenericIncludeRef extends BaseRef<GenericIncludePath> implements PsiFileReference
{
    public GenericIncludeRef (@NotNull GenericIncludePath element)
    {
        super (element);
    }

    @NotNull
    @Override
    public ResolveResult[] multiResolve (boolean incompleteCode)
    {
        PsiElement var = resolve ();
        if (var != null)
            return new ResolveResult[] { new PsiElementResolveResult (var) };
        else
            return ResolveResult.EMPTY_ARRAY;
    }

    @Nullable
    @Override
    public PsiFile resolve ()
    {
        if (!myElement.isValid ()) return null;

        VirtualFile refFile = myElement.getContainingFile ().getVirtualFile ();
        if (refFile == null) return null;

        String      path       = myElement.getPath ();
        VirtualFile targetFile = refFile.findFileByRelativePath ("../" + path); // .. potrzebne żeby cofnąć się o nazwę refFile
        if (targetFile == null) targetFile = refFile.findFileByRelativePath ("../" + path.toLowerCase ());
        if (targetFile == null) targetFile = refFile.findFileByRelativePath ("../" + path.toUpperCase ());
        if (targetFile == null) return null;

        return PsiManager.getInstance (myElement.getProject ()).findFile (targetFile);
    }

    @NotNull
    @Override
    public Object[] getVariants ()
    {
        return LookupElement.EMPTY_ARRAY;
    }

    @Override
    public PsiElement handleElementRename (String newPath)
    {
        return myElement.handleRename (newPath);
    }
}
