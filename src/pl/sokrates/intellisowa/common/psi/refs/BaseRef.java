package pl.sokrates.intellisowa.common.psi.refs;

import com.intellij.openapi.util.TextRange;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiReference;
import com.intellij.psi.PsiReferenceBase;
import org.jetbrains.annotations.NotNull;

public abstract class BaseRef<T extends PsiElement> extends PsiReferenceBase<T>
{
    protected PsiReference[] cachedArray;

    public BaseRef (@NotNull T element)
    {
        super (element, new TextRange (0, element.getTextLength ()));
    }

    public PsiReference[] getArray ()
    {
        if (cachedArray == null)
            cachedArray = new PsiReference[] { this };
        return cachedArray;
    }
}
