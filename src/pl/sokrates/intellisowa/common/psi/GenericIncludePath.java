package pl.sokrates.intellisowa.common.psi;

public interface GenericIncludePath extends PsiReferenceHolder
{
    String getPath ();

    GenericIncludePath handleRename (String newPath);
}
