package pl.sokrates.intellisowa.common.psi;

import com.intellij.psi.PsiElement;
import pl.sokrates.intellisowa.common.psi.refs.BaseRef;

public interface PsiReferenceHolder extends PsiElement
{
    BaseRef getNewReference ();
}
