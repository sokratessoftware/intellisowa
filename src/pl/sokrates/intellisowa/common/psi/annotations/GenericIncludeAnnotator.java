package pl.sokrates.intellisowa.common.psi.annotations;

import com.intellij.codeInspection.ProblemHighlightType;
import com.intellij.lang.annotation.AnnotationHolder;
import com.intellij.lang.annotation.Annotator;
import com.intellij.lang.annotation.HighlightSeverity;
import com.intellij.psi.PsiElement;
import com.intellij.psi.ResolveResult;
import org.jetbrains.annotations.NotNull;
import pl.sokrates.intellisowa.common.psi.GenericInclude;
import pl.sokrates.intellisowa.common.psi.GenericIncludePath;
import pl.sokrates.intellisowa.common.psi.refs.GenericIncludeRef;

public class GenericIncludeAnnotator implements Annotator
{
    @Override
    public void annotate (@NotNull PsiElement psiElement, @NotNull AnnotationHolder annotationHolder)
    {
        if (psiElement instanceof GenericInclude) {
            if (((GenericInclude) psiElement).getIncludePath () == null)
                annotationHolder.newAnnotation(HighlightSeverity.ERROR, "Brak ścieżki pliku w inkludzie.")
                    .range(psiElement).create();

            return;
        }

        if (psiElement instanceof GenericIncludePath) {
            GenericIncludeRef ref = (GenericIncludeRef) psiElement.getReference ();
            ResolveResult[]   results;

            if (ref == null || (results = ref.multiResolve (true)).length == 0) {
                annotationHolder.newAnnotation(HighlightSeverity.ERROR, "Plik nie odnaleziony.")
                    .range(psiElement)
                    .highlightType(ProblemHighlightType.LIKE_UNKNOWN_SYMBOL)
                    .create();
            }
            else {
                PsiElement target = results[0].getElement ();
                if (target.getManager ().areElementsEquivalent (target, psiElement.getContainingFile ()))
                    annotationHolder.newAnnotation(HighlightSeverity.ERROR, "Cykliczna referencja.")
                        .range(psiElement)
                        .create();
            }
        }
    }
}
