package pl.sokrates.intellisowa.common.psi;

import com.intellij.lang.ASTNode;
import org.jetbrains.annotations.NotNull;
import pl.sokrates.intellisowa.common.psi.refs.GenericIncludeRef;

import java.io.File;

public abstract class GenericIncludePathImpl extends PsiReferenceHolderImpl implements GenericIncludePath
{
    public GenericIncludePathImpl (@NotNull ASTNode node)
    {
        super (node);
    }

    @Override
    @NotNull
    public String getPath ()
    {
        return getText ().replace ("\\", File.separator);
    }

    @Override
    public GenericIncludeRef getNewReference ()
    {
        return new GenericIncludeRef (this);
    }
}
