package pl.sokrates.intellisowa.common.psi;

import com.intellij.psi.PsiElement;

public interface GenericInclude extends PsiElement
{
    GenericIncludePath getIncludePath ();
}
