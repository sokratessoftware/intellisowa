package pl.sokrates.intellisowa.common.psi;

import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiReference;
import org.jetbrains.annotations.NotNull;

/**
 * Próbowaliśmy tutaj cache'ować referencje, ale nie wolno,
 * IDEA cache'uje wynik resolve() póki obiekt referencji żyje i wszystko się sypie
 */
public abstract class PsiReferenceHolderImpl extends ASTWrapperPsiElement implements PsiReferenceHolder
{
    public PsiReferenceHolderImpl (@NotNull ASTNode node)
    {
        super (node);
    }

    @Override
    public PsiReference getReference ()
    {
        return getNewReference ();
    }

    @NotNull
    @Override
    public PsiReference[] getReferences ()
    {
        PsiReference ref = getNewReference ();
        if (ref != null) return new PsiReference[] { ref };
        else return PsiReference.EMPTY_ARRAY;
    }

    //    protected WeakReference<BaseRef> cachedReference;
//
//    @Override
//    public PsiReference getReference () {
//        PsiReference[] result = getReferences ();
//        if (result.length == 0) return null;
//        return result[0];
//    }
//
//    @Override
//    public PsiReference[] getReferences () {
//        BaseRef ref = null;
//        if (cachedReference != null && (ref = cachedReference.get ()) != null)
//            return ref.getArray ();
//
//        ref = getNewReference ();
//        if (ref == null) return PsiReference.EMPTY_ARRAY;
//        cachedReference = new WeakReference<BaseRef> (ref);
//        return ref.getArray ();
//    }
}
