package pl.sokrates.intellisowa;

import com.intellij.openapi.util.IconLoader;

import javax.swing.*;

public class SokratesIcons
{
    public static final Icon LESH_LANG = IconLoader.getIcon ("/pl/sokrates/intellisowa/icons/sowa.png");
    public static final Icon EPR = IconLoader.getIcon ("/pl/sokrates/intellisowa/icons/sowa2.png");
}
