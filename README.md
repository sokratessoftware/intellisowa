# IntelliSOWA #

Plugin dla IntelliJ IDEA (również wersji darmowej) zapewniający wsparcie dla:

  * skryptów LeshLanga
  * formularzy EPR
  * projektu grupującego takie pliki

## Budowanie ##

Niestety generacja leksera i parsera w pluginach IDEI nie jest zautomatyzowana.
Zanim będzie dało się skompilować projekt, należy:

  * odnaleźć katalog `src/pl/sokrates/intellisowa/leshlang`:
    * z menu kontekstowego pliku `LeshLang.bnf` wybrać *Generate Parser Code*
    * z menu kontekstowego pliku `LeshLang.flex` wybrać *Run JFlex Generator*
  * odnaleźć katalog `src/pl/sokrates/intellisowa/epr`:
    * z menu kontekstowego pliku `EprLang.bnf` wybrać *Generate Parser Code*
    * z menu kontekstowego pliku `EprLang.flex` wybrać *Run JFlex Generator*

Jeśli wygenerowany lexer się nie kompiluje, to możemy spróbować inną wersję:

  * pobrać jara z: https://github.com/JetBrains/intellij-deps-jflex/releases 
  * odpalić: `java -Dfile.encoding=UTF-8 -jar jflex-1.9.1.jar -skel /ścieżka/do/idea-flex.skeleton -d src/intellisowa/gen/pl/sokrates/intellisowa/epr src/intellisowa/src/pl/sokrates/intellisowa/epr/EprLang.flex`
  * `idea-flex.skeleton` jest w katalogu, do którego kazaliśmy pluginowi `Grammar-Kit` pobrać JFlexa.

Po aktualizacji repozytorium i zmianach w w/w plikach należy:

  * usunąć katalog `gen` - bo generator parserów sam nie czyści starych klas

## Testowanie ##

W *Run* >> *Edit Configurations...* tworzymy sobie konfigurację typu "Plugin"
i wybieramy IntelliSOWA w polu *Use classpath of module*.

Uruchomienie tej konfiguracji opali nam osobną kopię IDEI z zainstalowanym pluginem.

## Paczkowanie ##

*Build* >> *Prepare Plugin Module 'IntelliSOWA' For Deployment*

## Poziom wsparcia LeshLanga ##

  * podświetlanie składni
  * podświetlanie błędów
    * wywołanie nieznanej procedury
    * wywołanie procedury zdefiniowanej wiele razy
    * wywołanie nieznanej funkcji
    * wywołanie funkcji ze złą liczbą argumentów
    * odczyt zmiennej, która nie jest przypisana nigdzie w pliku lub importach
    * import nieistniejącego pliku
    * import pliku o złym typie
  * naprawa błędów
    * propozycja importu nieznanej procedury (`ctrl-enter`)
  * wyświetlanie dokumentacji
    * dla funkcji (`ctrl-q`, `ctrl-p`)
    * dla procedur (`ctrl-q`)
  * dodawanie/usuwanie komentarza (`ctrl-/`)
  * automatyczne wcięcia / formatowanie kodu (tylko wcięcia)
    * z możliwością konfiguracji osobno od innych języków
  * referencje do procedur:
    * przechodzenie do deklaracji (`ctrl-b`)
    * wyszukiwanie deklaracji (`ctrl-alt-shift-n`)
    * odnajdywanie wywołań respektując importy (*Find Usages*)
  * referencje do zmiennych:
    * odnajdywanie użyć, globalnie (*Find Usages*)
  * lista procedur w danym pliku w panelu *Structure*
  * dopełnianie
    * nazw zmiennych i procedur z danego pliku i importów
    * nazw funkcji z automatycznymi nawiasami
    * słów kluczowych gdzie mają sens
    * szablonów (*Live Templates*)
      * dla deklaracji procedury oraz begin/end
      * dla if/endif, if/else/endif i while/enddo
    * ścieżek plików w dyrektywie `PLIK`
    * kontekst dopełniania dla szablonów wyrażeń - żeby można było dodać szablony dla często używanych funkcji z domyślnymi argumentami

## Poziom wsparcia EPR ##

  * podświetlanie składni
  * podświetlanie błędów
    * import nieistniejącego pliku
    * import pliku o złym typie
  * dopełnianie
    * słów kluczowych
    * ścieżek plików w dyrektywach `PLIK` i `SKRYPT`

## Niezamplementowane ##

  * *Stub tree* - zredukowane drzewo AST do cache'owania, bo to jeszcze bardziej komplikuje klasy AST
  * *Inspections* - sprawdzanie błędów przetwarzane wsadowo
