#!/bin/bash

LOGIN="sokrates"

PLUGIN_XML="../resources/META-INF/plugin.xml"
PLUGIN_JAR="../IntelliSOWA.jar"

set -e

cd $(dirname $(realpath "$BASH_SOURCE"))

[ -f "$PLUGIN_XML" ]     || { echo Cannot find: $PLUGIN_XML; exit 1; }
[ -f "$PLUGIN_JAR" ]     || { echo Cannot find: $PLUGIN_JAR; exit 1; }

which davix-put >/dev/null || { echo WebDAV client davix not found; exit 2; }
which xmllint >/dev/null || { echo libxml2 tool xmllint not found; exit 2; }

echo -n "Password? "
read -s PASS

ID=$(xmllint --xpath "string(/idea-plugin/id)" $PLUGIN_XML)
VERSION=$(xmllint --xpath "string(/idea-plugin/version)" $PLUGIN_XML)

XML_NAME=$(xmllint --xpath "/idea-plugin/name" $PLUGIN_XML)
XML_DESC=$(xmllint --xpath "/idea-plugin/description" $PLUGIN_XML)
XML_CLOG=$(xmllint --xpath "/idea-plugin/change-notes" $PLUGIN_XML)
XML_IDEA=$(xmllint --xpath "/idea-plugin/idea-version" $PLUGIN_XML)

cat > updatePlugins.xml <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<plugins>
  <plugin id="${ID}" version="${VERSION}" url="https://dav.sokrates.pl/intellisowa/intellisowa-${VERSION}.jar">
    ${XML_IDEA}
    ${XML_NAME}
    ${XML_DESC}
    ${XML_CLOG}
  </plugin>
</plugins>
EOF

davix-mkdir --userlogin "$LOGIN" --userpass "$PASS" davs://dav.sokrates.pl/intellisowa 2>/dev/null || true
davix-put   --userlogin "$LOGIN" --userpass "$PASS" "$PLUGIN_JAR" davs://dav.sokrates.pl/intellisowa/intellisowa-${VERSION}.jar
davix-rm    --userlogin "$LOGIN" --userpass "$PASS" davs://dav.sokrates.pl/intellisowa/updatePlugins.xml 2>/dev/null || true
davix-put   --userlogin "$LOGIN" --userpass "$PASS" "updatePlugins.xml" davs://dav.sokrates.pl/intellisowa/updatePlugins.xml

echo DONE.

rm updatePlugins.xml
